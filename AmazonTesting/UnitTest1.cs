﻿using System;
using FSI.ASINOptimization.Services.Amazon;
using FSI.ASINOptimization.UI.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Threading;
using System.Collections.Generic;

namespace AmazonTesting
{
    [TestClass]
    public class AmazonProductServiceTest
    {
        //[TestMethod]
        //public void ReportsListRequestTest()
        //{
        //    var itemList = AmazonReportService.Instance.ReportsListRequest();
        //    Assert.IsNotNull(itemList);
        //}
        //[TestMethod]
        //public void GetReportRequestTest()
        //{
        //    var itemList = AmazonReportService.Instance.GetReportRequest("5054159887017294", (int)ReportTypes._GET_MERCHANT_LISTINGS_DATA_);
        //    Assert.IsNotNull(itemList);
        //}
        //[TestMethod]
        //public void GetChildMatchingProductRequestTest()
        //{
        //    var item = AmazonProductService.Instance.GetChildMatchingProductRequest("B01G2N1WKU");
        //    Assert.IsNotNull(item);
        //}
        public void dotest(Stopwatch timer, string asin, int testNum)
        {
            timer.Start();
            var test1 = AmazonProductService.Instance.GetLowestPricedOffersForASIN(asin, "New");
            timer.Stop();
            Debug.WriteLine(String.Format("Test{0}: B00X4WHP5E - time:{1}", testNum, timer.ElapsedMilliseconds));
        }


        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            // code goes here
        }

        [TestMethod]
        public void GetLowestPricedOffersForASINTest()
        {
            // Settings 
            int testCount = 50;
            List<string> asins = new List<string>();
            asins.Add("B00X4WHP5E");
            asins.Add("B01DFKC2SO");

            List<Stopwatch> timers = new List<Stopwatch>();


            for (int i = 0; i < testCount; i++)
            {
                var sw = new Stopwatch();
                timers.Add(sw);
                dotest(sw, "B01DFKC2SO", i);
            }
            long sum = 0;
            for (int i = 0; i < testCount; i++)
            {
                sum += timers[i].ElapsedMilliseconds;
            }
            Debug.WriteLine(String.Format("Test ALL TIME ==================: - time:{0}", sum));
        }

        [TestMethod]
        public void GetLowestPricedOffersForASINTestASYNC()
        {
            List<Thread> tasks = new List<Thread>();
            for (int i = 0; i < 30; i++)
            {
                Thread thread = new Thread(() => {

                    var test5 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B01E9AHU8Q", "New");
                });
                tasks.Add(thread);
            }
            
            asin1();
            System.Diagnostics.Stopwatch sw1 = new System.Diagnostics.Stopwatch();
            sw1.Start();
            foreach (var task in tasks)
            {
                task.Start();

            }

            foreach (var task in tasks)
            {
                task.Join();
            }


            sw1.Stop();
            Debug.WriteLine(String.Format("All TESTS FINISH ============ - time:{0}", sw1.ElapsedMilliseconds));
        }
        public void asin5()
        {
            var test5 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B01E9AHU8Q", "New");
        }
        public void asin4()
        {
            var test4 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B01J24C0TI", "New");
        }
        public void asin3()
        {
            var test3 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B00JESQSY6", "New");
        }

        public void asin2()
        {
            var test2 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B01DFKC2SO", "New");
        }
        public void asin1()
        {
            var test1 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B00X4WHP5E", "New");
        }

        public void asin6()
        {
            var test5 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B01E9AHU8Q", "New");
        }
        public void asin7()
        {
            var test4 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B01J24C0TI", "New");
        }
        public void asin8()
        {
            var test3 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B00JESQSY6", "New");
        }

        public void asin9()
        {
            var test2 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B01DFKC2SO", "New");
        }
        public void asin10()
        {
            var test1 = AmazonProductService.Instance.GetLowestPricedOffersForASIN("B00X4WHP5E", "New");
        }
        //[TestMethod]
        //public void requestReportTest()
        //{
        //    AmazonReportService.Instance.ReportRequestRequest((int)ReportTypes._GET_MERCHANT_LISTINGS_ALL_DATA_);
        //}

        //[TestMethod]
        //public void requestBuyBoxTest()
        //{
        //    var data = BuyBoxProcessorProcessor.Instance.GetBuyBox();
        //}

    }
}
