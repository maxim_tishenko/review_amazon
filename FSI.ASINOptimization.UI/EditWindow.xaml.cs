﻿using System;
using System.Windows;
using System.Windows.Controls;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.EditControls;
using FSI.ASINOptimization.UI.ViewModel;

namespace FSI.ASINOptimization.UI
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        private readonly IEditControl _editControl;

        public EditWindow(object model, ASINOptimizationContext context)
        {
            InitializeComponent();
            _editControl = GetCorrespondingControl(model.GetType());
            _editControl.Model = model;
            _editControl.Context = context;
            controlGrid.Children.Add((UserControl)_editControl);
        }



        private void CancelAction(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void AcceptAction(object sender, RoutedEventArgs e)
        {
            if (_editControl.Commit())
            {
                DialogResult = true;
                Close();
            }
        }

        private static IEditControl GetCorrespondingControl(Type modelType)
        {

            if (modelType == typeof(BrandLookupColorEntityViewModel))
            {
                return new BrandColorEditControl();
            }
            if (modelType == typeof(BrandLookupSizeEntityViewModel))
            {
                return new BrandSizeEditControl();
            }
            if (modelType == typeof (BrandLookupBrandEntityViewModel))
            {
                return new BrandEditControl();
            }
            if (modelType == typeof (FeedNameViewModel))
            {
                return new FeedNameEditControl();
            }
            

            throw new NotImplementedException("There is no edit control for current model.");
        }
    }
}
