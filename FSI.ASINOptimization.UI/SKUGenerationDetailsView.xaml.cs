﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;

namespace FSI.ASINOptimization.UI
{
    /// <summary>
    /// Interaction logic for SKUGenerationDetailsView.xaml
    /// </summary>
    public partial class SKUGenerationDetailsView : UserControl, IASINOptimizationView
    {
        private SearchFeedViewModel _parentModel;
        private int _selectedPosition;

        private ObservableCollection<BrandLookupBrand> _brands;
        private ObservableCollection<BrandLookupColor> _colors;
        private ObservableCollection<BrandLookupSize> _sizes; 

        public SKUGenerationDetailsView(ASINOptimizationContext context, SearchFeedViewModel parentModel, int selectedPosition)
        {
            InitializeComponent();
            Context = context;
            _parentModel = parentModel;
           _brands = new ObservableCollection<BrandLookupBrand>(context.StorageContext.Brands);
            _colors = new ObservableCollection<BrandLookupColor>(context.StorageContext.BrandColors);
            _sizes = new ObservableCollection<BrandLookupSize>(context.StorageContext.BrandSizes);

            cbBrand.DataContext = _brands;
            cbColor.DataContext = _colors;
            cbSize.DataContext = _sizes;
            productGrid.DataContext = parentModel.Items[selectedPosition];
            _selectedPosition = selectedPosition;
            CurrentAsin.Text = parentModel.Items[selectedPosition].ASIN;
            ParentASIN.Text = parentModel.Items[selectedPosition].ParentASIN;
        }

        public ASINOptimizationContext Context { get; set; }
        public void SaveCurrentState()
        {
            var currentProduct = (MarketplaceProductViewModel)productGrid.DataContext;
            currentProduct.SaveChanges();
            _parentModel.SaveChanges();
        }

        public void StopBackgroundTasks()
        {

        }
        public event NavigateEventHandler Navigated;

        private void CbiNewBrand_OnSelected(object sender, RoutedEventArgs e)
        {
            var model = new BrandLookupBrandEntityViewModel(new BrandLookupBrand());
            var editWindow = new EditWindow(model, Context);
            if (editWindow.ShowDialog() == true)
            {
                model.SaveChanges();
                Context.StorageContext.Brands.Add(model.OriginalModel);
                Context.StorageContext.SaveChanges();
                _brands.Add(model.OriginalModel);
                cbBrand.SelectedItem = model.OriginalModel;
            }
            else
            {
                cbBrand.SelectedItem = null;
            }
        }


        private void CbiNewColor_OnSelected(object sender, RoutedEventArgs e)
        {
            var model = new BrandLookupColorEntityViewModel(new BrandLookupColor());
            var editWindow = new EditWindow(model, Context);
            if (editWindow.ShowDialog() == true)
            {
                model.SaveChanges();
                Context.StorageContext.BrandColors.Add(model.OriginalModel);
                Context.StorageContext.SaveChanges();
                _colors.Add(model.OriginalModel);
                cbColor.SelectedItem = model.OriginalModel;
            }
            else
            {
                cbColor.SelectedItem = null;
            }
        }

        private void CbiNewSize_OnSelected(object sender, RoutedEventArgs e)
        {
            var model = new BrandLookupSizeEntityViewModel(new BrandLookupSize());
            var editWindow = new EditWindow(model, Context);
            if (editWindow.ShowDialog() == true)
            {
                model.SaveChanges();
                Context.StorageContext.BrandSizes.Add(model.OriginalModel);
                Context.StorageContext.SaveChanges();
                _sizes.Add(model.OriginalModel);
                cbSize.SelectedItem = model.OriginalModel;
            }
            else
            {
                cbSize.SelectedItem = null;
            }
        }

        private void OnDeleteASIN(object sender, RoutedEventArgs e)
        {
            var currentProduct = (MarketplaceProductViewModel) productGrid.DataContext;
            _parentModel.RemoveItem(currentProduct);
            _parentModel.SaveChanges();
            _selectedPosition--;
            ShowNextASIN();
        }

        private void OnGoBack(object sender, RoutedEventArgs e)
        {
            SaveCurrentState();
            if (Navigated != null)
            {
                Navigated(this, new NavigateEventArgs { IsBackNavigation = true, NavigationParameters = new object[] { _parentModel } });
            }
        }

        private void OnGenerateSKU(object sender, RoutedEventArgs e)
        {
            ErrorMsg.Content = "";
             var currentProduct = (MarketplaceProductViewModel)productGrid.DataContext;
            if (!currentProduct.CalculateSKU())
                ErrorMsg.Content = "↑ Please Map all of the property!";
                
            SaveCurrentState();
        }

        private void OnNextASIN(object sender, RoutedEventArgs e)
        {
            SaveCurrentState();
            ShowNextASIN();
        }

        private void ShowNextASIN()
        {
            if (_selectedPosition == _parentModel.Items.Count - 1)
            {
                OnGoBack(null, null);
            }
            else
            {
                _selectedPosition++;
                productGrid.DataContext = _parentModel.Items[_selectedPosition];
                CurrentAsin.Text = _parentModel.Items[_selectedPosition].ASIN;
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var currentProduct = (MarketplaceProductViewModel)productGrid.DataContext;
            System.Diagnostics.Process.Start(String.Format("http://www.amazon.com/dp/{0}", currentProduct.ASIN));
        }
    }
}
