﻿namespace FSI.ASINOptimization.UI.Core
{
    public interface IEditControl
    {
        ASINOptimizationContext Context { get; set; }

        object Model { get; set; }

        bool Commit();
    }
}
