﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using FSI.ASINOptimization.UI.ViewModel;

namespace FSI.ASINOptimization.UI.Core
{
    public class NavigationContext
    {
        private Stack<UIModules> _backStack;

        private Dictionary<UIModules, UserControl> _views;
        private static Dictionary<UIModules, NavigationViewModel> _navigationMap = new Dictionary
            <UIModules, NavigationViewModel>
        {
            {
                UIModules.FeedProcessor,
                new NavigationViewModel {UIModule = UIModules.FeedProcessor, Title = "Feed Processor", Active = true}
            },
            {
                UIModules.SKUGeneratorList,
                new NavigationViewModel {UIModule = UIModules.SKUGeneratorList, Title = "SKU Generation List"}
            },
            {
                UIModules.Reports,
                new NavigationViewModel {UIModule = UIModules.Reports, Title = "Reports and Remapping"}
            },
            {
                UIModules.ASINRemapping,
                new NavigationViewModel {UIModule = UIModules.ASINRemapping, Title = "ASIN Remapping"}
            },
            {
                UIModules.SKUGeneratorDetails,
                new NavigationViewModel
                {
                    UIModule = UIModules.SKUGeneratorDetails,
                    Title = "SKU Generation Details Overwrite"
                }
            },
            {
                UIModules.BrandLookupPopulation,
                new NavigationViewModel {UIModule = UIModules.BrandLookupPopulation, Title = "Brand Lookup Population"}
            },
            //{UIModules.Report, new NavigationViewModel {UIModule = UIModules.Report, Title = "Manage Reports"}}

        };

        public NavigationViewModel ActiveModule
        {
            get { return _navigationMap.Values.First(m => m.Active); }
        }

        public NavigationContext()
        {
            _backStack = new Stack<UIModules>();
            _views = new Dictionary<UIModules, UserControl>();
            _views.Add(UIModules.ASINRemapping, null);
            _views.Add(UIModules.BrandLookupPopulation, null);
            _views.Add(UIModules.FeedProcessor, null);
            _views.Add(UIModules.Reports, null);
            _views.Add(UIModules.SKUGeneratorDetails, null);
            _views.Add(UIModules.SKUGeneratorList, null);


        }

        public IEnumerable<NavigationViewModel> GetMenuNavigation(IEnumerable<UIModules> modulesToShow)
        {
            return modulesToShow.Select(module => _navigationMap[module]);
        }

        public void ReactivatePreviousModule()
        {
            if (_backStack.Count == 0)
            {
                throw new InvalidOperationException("Cannot navigate back!");
            }

            SetActiveModule(_backStack.Pop());
        }

        public void SetActiveModule(UIModules module, bool keepBackNav = false)
        {
            if (keepBackNav)
            {
                _backStack.Push(ActiveModule.UIModule);
            }

            foreach (NavigationViewModel navigationViewModel in _navigationMap.Values)
            {
                navigationViewModel.Active = false;
            }

            _navigationMap[module].Active = true;
        }

        public Control GetViewForActiveModule(ASINOptimizationContext context, object[] parameters)
        {
            switch (ActiveModule.UIModule)
            {
                case UIModules.FeedProcessor:
                    return new FeedProcessorView(context);
                case UIModules.BrandLookupPopulation:
                    return new BrandLookupPopulationView(context);
                case UIModules.SKUGeneratorList:
                    return new SKUGenerationListView(context, (SearchFeedViewModel)parameters[0]);
                case UIModules.SKUGeneratorDetails:
                    return new SKUGenerationDetailsView(context, (SearchFeedViewModel) parameters[0],
                        (int) parameters[1]);
                case UIModules.Reports:
                    if(_views[ActiveModule.UIModule] == null)
                        _views[ActiveModule.UIModule] = new LoadingReports(context);
                    return _views[ActiveModule.UIModule];
                case UIModules.ASINRemapping:
                    if (_views[ActiveModule.UIModule] == null)
                    {
                        _views[ActiveModule.UIModule] = new ASINRemapping(context);
                        (_views[ActiveModule.UIModule] as ASINRemapping).CompilationMark = (int)parameters[0];
                        return _views[ActiveModule.UIModule];
                    }
                    else
                    {
                        if ((_views[ActiveModule.UIModule] as ASINRemapping).CompilationMark == (_views[UIModules.Reports] as LoadingReports).CompilationMark)
                        {
                            return _views[ActiveModule.UIModule];
                        }
                        else
                        {
                            return new ASINRemapping(context,(int)parameters[0]);
                        }
                    }
                    break;
                default:
                    return null;
            }
        }
    }
}
