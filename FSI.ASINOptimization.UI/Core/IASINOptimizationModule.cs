﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FSI.ASINOptimization.UI.ViewModel;

namespace FSI.ASINOptimization.UI.Core
{
    public delegate void NavigateEventHandler(IASINOptimizationView source, NavigateEventArgs e);

    public class NavigateEventArgs : EventArgs
    {
        public UIModules Target { get; set; }

        public object[] NavigationParameters { get; set; }

        public bool IsBackNavigation { get; set; }

        public bool NeedToKeepBackNavigation { get; set; }
    }

    public interface IASINOptimizationView
    {

        ASINOptimizationContext Context { get; set; }

        void SaveCurrentState();

        event NavigateEventHandler Navigated;

        void StopBackgroundTasks();
    }
}
