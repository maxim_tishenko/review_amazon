﻿using System;
using System.Collections.Generic;
using System.Linq;
using FBAInventoryServiceMWS.Model;
using FSI.ASINOptimization.Services.Amazon;
using FSI.ASINOptimization.Services.Local;
using FSI.ASINOptimization.Storage;
using FSI.ASINOptimization.Storage.Model;
using MarketplaceWebService.Model;
using System.Globalization;
using FSI.ASINOptimization.UI.ViewModel;
using System.Windows.Controls;
using FSI.ASINOptimization.UI.Helpers;
using FSI.ASINOptimization.Services.Amazon.AmazonProductServiceModels.BuyBoxPricesModels;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;
using System.Windows;

namespace FSI.ASINOptimization.UI.Core
{
    public class ASINOptimizationContext : IDisposable
    {
        System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer(DispatcherPriority.Background, Application.Current.Dispatcher);
        public StorageContext StorageContext { get; set; }
        public HashSet<string> AsinHistoryHashSet { get; set; }
        public event EventHandler<string> _lostConnectionEvent;
        public ASINOptimizationContext()
        {
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
            StorageContext = new StorageContext();
            AsinHistoryHashSet = new HashSet<string>();
            InternetConnectionService.Instance._updateStatusEvent += Instance__updateStatusEvent;
            LoadLocalData();
        }

        private void Instance__updateStatusEvent(object sender, string e)
        {
            if (_lostConnectionEvent != null)
                _lostConnectionEvent(this, e);
        }

        public void LoadLocalData()
        {
            StorageContext.SearchQueries.ToList();
            StorageContext.BrandColors.ToList();
            StorageContext.BrandSizes.ToList();
            StorageContext.Brands.ToList();
            StorageContext.MarketplaceProducts.ToList();
            StorageContext.ASINs.ToList();
            StorageContext.AsinHistory.ToList();
            foreach (var asinM in StorageContext.AsinHistory.ToList())
            {
                AsinHistoryHashSet.Add(asinM.Asin);
            }
            StorageContext.SearchFeeds.ToList();
            StorageContext.SubmittedFeeds.ToList();
        }
        public void UpdateAsinHistory()
        {
            foreach (var asinM in StorageContext.AsinHistory.ToList())
            {
                AsinHistoryHashSet.Add(asinM.Asin);
            }
        }

        public IEnumerable<FeedInfo> GetRemoteFeeds()
        {
            var feedList = AmazonFeedService.Instance.GetFeedSubmissionListRequest();
            foreach (FeedSubmissionInfo feedSubmissionInfo in feedList)
            {
                var feed =
                    StorageContext.SubmittedFeeds.FirstOrDefault(
                        f => f.FeedSubmissionId == feedSubmissionInfo.FeedSubmissionId);
                //if (feed == null)
                //{
                //    feed=new FeedInfo
                //    {
                //        FeedSubmissionId = feedSubmissionInfo.FeedSubmissionId,
                //        Status = feedSubmissionInfo.FeedProcessingStatus,
                //        SubmittedDate = DateTime.Parse(feedSubmissionInfo.SubmittedDate),
                //        Name = feedSubmissionInfo.FeedSubmissionId,

                //    };
                //}
                if (feed != null)
                {
                    yield return feed;
                }
            }
        }

        public IEnumerable<MarketplaceProduct> GetMatchingProducts(string query)
        {
            List<string> parentAsins = AmazonRawProductService.QueryProductsParentASINs(query);
            return AmazonProductService.Instance.ListMatchingProductsRequest(query).Select(matchingProduct => new MarketplaceProduct
            {
                ASIN = matchingProduct.ASIN,
                Title = matchingProduct.Title,
                Color = matchingProduct.Color,
                Size = matchingProduct.Size,
                Brand = matchingProduct.Brand,
                ImageSrc = matchingProduct.ImageURl
            });
        }

        public bool IsSearchPossible(string query)
        {
            var data = AmazonProductService.Instance.ListMatchingProductsRequest(query);
            if (data == null)
                return false;
            bool isCan = data.Count > 0;
            return isCan;
        }
        public IEnumerable<MarketplaceProduct> GetProductChilds(string query)
        {
            try
            {
                SimpleLog.WriteLog("partially foreach...");
                return AmazonProductService.Instance.ListMatchingProductsDistinctChildsRequest(query).Select(matchingProduct => new MarketplaceProduct
                {
                    ASIN = matchingProduct.ASIN,
                    ParentASIN = matchingProduct.ParentASIN,
                    Title = matchingProduct.Title,
                    Color = matchingProduct.Color,
                    Size = matchingProduct.Size,
                    Brand = matchingProduct.Brand,
                    ImageSrc = matchingProduct.ImageURl
                });
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        private void Browser_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SubmittedASIN> GetInventoryASINs()
        {
            return
                AmazonInventoryService.Instance.ListInventorySupplyRequest()
                    .Select(supply => new SubmittedASIN { SKU = supply.SellerSKU, Value = supply.ASIN });
        }

        public void LoadInventoryReport(string reportId, ObservableCollection<string> skuFilters)
        {
            var fileName = AmazonReportService.Instance.GetReportRequest(reportId);
            //InventoryReportProcessor.Instance.ProcessReportFile("_report_5_29_2017_9539171.csv");
            InventoryReportProcessor.Instance.ProcessReportFile(fileName, skuFilters);
        }

        public IEnumerable<MerchantListingModel> GetInventoryReportData()
        {
            return InventoryReportProcessor.Instance.Data;
        }

        public void LoadBusinessReport(string filename, ObservableCollection<string> brandFilters)
        {
            BusinessReportProcessor.Instance.ProcessReportFile(filename, brandFilters);
        }

        public IEnumerable<BusinessReportListingModel> GetBusinessReportData()
        {
            return BusinessReportProcessor.Instance.Data;
        }

        public void CombineReportsData()
        {
            DataCombineProcessor.Instance.CombineReportsData(
                GetInventoryReportData(),
                GetBusinessReportData(),
                GetLandedPricesData());
        }




        

        public void LoadLandedPriceForASIN(string asin)
        {
            requestsPerSecond++;
            workingThreads++;
            var data = AmazonProductService.Instance.GetLowestPricedOffersForASIN(asin, "New");
            //var data = new LandedPrice() { Amount = new Random(Environment.TickCount).Next(30).ToString(), CurrencyCode = "$" };
            LandedPriceProcessor.LimitReached = AmazonProductService.quotaReachedMessage;
            LandedPriceProcessor.Instance.AddLandedPriceItem(asin, (data != null) ? data.Amount : "null");
            workingThreads--;
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            requestsPerSecond = 0;
        }
        int requestsPerSecond = 0;
        int workingThreads =0;
        public void LoadLandedPrices()
        {
            LandedPriceProcessor.ResetProgress();
            SimpleLog.WriteLog("LandedPriceProcessor.SetProgressItemsCount(GetBusinessReportData().Count()); ,exit exception...");
            SimpleLog.WriteLog("Load business report data...");
            LandedPriceProcessor.SetProgressItemsCount(GetInventoryReportData().Count());
            foreach (var item in GetInventoryReportData())
            {
                while (true)
                {
                    if (requestsPerSecond < 10 &&  workingThreads<10)
                    {
                        Thread thread = new Thread(() => {
                            LoadLandedPriceForASIN(item.ASIN);
                        });
                        thread.Start();
                        SimpleLog.WriteLog("GetLowestPricedOffersForASIN...");

                        break;
                    }
                    else {
                        Thread.Sleep(500);
                    }
                }
               
                //var data = new LandedPrice() { Amount = new Random(Environment.TickCount).Next(30).ToString(), CurrencyCode = "$" };
                //LandedPriceProcessor.Instance.AddLandedPriceItem(item.ASIN, (data != null) ? data.Amount : "null");
            }
            while (workingThreads != 0)
                Thread.Sleep(100);
            Thread.Sleep(500);
            SimpleLog.WriteLog("GetLowestPricedOffersForASIN complete...");
            LandedPriceProcessor.Instance.Complete();
        }

        public List<LandedPriceItemModel> GetLandedPricesData()
        {
            return LandedPriceProcessor.Instance.GetItems();
        }

        public List<RemappingDataModel> GetCombinedReportsData()
        {
            return DataCombineProcessor.Instance.Data;
        }

        public string CreateNewReportRequest(int reportType)
        {
            var data = AmazonReportService.Instance.ReportRequestRequest(reportType);
            if (data.ReportProcessingStatus == "_SUBMITTED_")
            {
                return data.ReportRequestId;
            }
            else
                return null;
        }

        public string GetReportIdByReportRequestId(string reportRequestId)
        {
            var data = AmazonReportService.Instance.ReportsListRequest(reportRequestId);
            if (data != null && data.Count >= 1)
                return data[0].ReportId;
            else
            {
                return null;
            }
        }

        public void Dispose()
        {
            StorageContext.Dispose();
        }
    }
}
