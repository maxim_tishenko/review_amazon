﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;
using FSI.ASINOptimization.Services.Amazon;
using Microsoft.Win32;
using FSI.ASINOptimization.Services.Local;
using System.IO;
using System.Globalization;

namespace FSI.ASINOptimization.UI
{
    /// <summary>
    /// Interaction logic for FeedProcessorView.xaml
    /// </summary>
    public partial class LoadingReports : UserControl, IASINOptimizationView
    {
        private ReportsViewModel _model;
        private bool IsRunAll = false;
        private int compilationMark = 0;

        public static class StatusImageFactory
        {
            public static BitmapImage GreenArrow
            {
                get
                {
                    return new BitmapImage(new Uri("pack://application:,,,/ASIN Optimization Tool;component/Resources/green_arrow.png"));
                }
            }
            public static BitmapImage GrayArrow
            {
                get
                {
                    return new BitmapImage(new Uri("pack://application:,,,/ASIN Optimization Tool;component/Resources/gray_arrow.png"));
                }
            }
            public static BitmapImage RedArrow
            {
                get
                {
                    return new BitmapImage(new Uri("pack://application:,,,/ASIN Optimization Tool;component/Resources/red_arrow.png"));
                }
            }
        }
        public LoadingReports(ASINOptimizationContext context = null)
        {
            if (Context != null)
                return;
            InitializeComponent();
            Context = context;

            LoadAllDataButton.Background = Brushes.DarkGreen;
            LoadAllDataButton.Content = "Run All";
            Services.Local.LandedPriceProcessor.itemLoaded += LandedPriceProcessor_itemLoaded;
            Services.Local.LandedPriceProcessor.complete += _model__LandedPriceLoaded;

            _model = new ReportsViewModel(context);
            _model._bisunessReportLoaded += _model__bisunessReportLoaded;
            _model._bisunessReportError += _model__bisunessReportError;
            _model._inventoryReportLoaded += _model__inventoryReportLoaded;
            _model._inventoryReportError += _model__inventoryReportError;
            _model._reportsDataCombined += _model__reportsDataCombined;
            _model._reportsDataCombineError += _model__reportsDataCombineError;
            _model._bisunessReportLoaded += DataCombiningAction;
            _model._inventoryReportLoaded += DataCombiningAction;
            _model._lendedPriceLoaded += DataCombiningAction;
            _model._lendedPriceLoaded += _model__lendedPriceLoaded;
            _model._inventoryReportLoadingStep += _model__inventoryReportLoadingStep;
            FiltersLayout.DataContext = _model.Filters;
        }

        private void _model__lendedPriceLoaded(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                LandedReportStatusImage.Source = StatusImageFactory.GreenArrow;
            }));
        }

        public void StopBackgroundTasks()
        {

        }

        private void LandedPriceProcessor_itemLoaded(object sender, Services.Local.LoadedLandedPricePercentEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                ProgressPercentageLandedPriceLabel.Content = string.Format("Processing Landed Price : {0}%", Math.Round((double)e.CurrentItem * 100 / e.ItemsCount));
                ProgressBoxLendedPrice.Maximum = e.ItemsCount;
                ProgressBoxLendedPrice.Value = e.CurrentItem;
            }));
        }

        #region Data Combine

        private void DataCombiningAction(object sender,EventArgs e)
        {
            if (!_model.IsDataCombineCanRun())
                return;
            Dispatcher.BeginInvoke(new Action(() => {
                CombineButton.IsEnabled = false;
                ProgressDataCombining.Visibility = Visibility.Visible;
                _model.CombineReportsData();
            }));
        }
        private void CombineButton_Click(object sender, RoutedEventArgs e)
        {
            if (_model.Filters.SKUFilters.Count == 0)
            {
                MessageBox.Show("Please add at least one SKU number!");
                return;
            }
            if (_model.Filters.BrandFilters.Count == 0)
            {
                MessageBox.Show("Please add at least one Brand line!");
                return;
            }
            DataCombiningAction(this,new EventArgs());
        }
        private void _model__reportsDataCombined(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() => {
                ProgressDataCombining.Visibility = Visibility.Hidden;
                if(!IsRunAll)
                {
                    CombineButton.IsEnabled = false;
                }
                LoadAllDataButton.IsEnabled = true;
                LoadAllDataButton.Background = Brushes.DarkGreen;
                LoadAllDataButton.Content = "Run All";

                DataCombineStatusImage.Source = StatusImageFactory.GreenArrow;
                IsRunAll = false;
                compilationMark = new Random(Environment.TickCount).Next(999999999);
                TotalProgressBar.Visibility = Visibility.Hidden;
                RemappingPage.IsEnabled = true;
                ExportToExcel.IsEnabled = true;
                FiltersSettingLayout.IsEnabled = true;

            }));
        }
        private void _model__reportsDataCombineError(object sender, EventArgs e)
        {
            MessageBox.Show("Error while combining reports data! please reload reports and try again later.", "Error!");
            Dispatcher.BeginInvoke(new Action(() => {
                ProgressDataCombining.Visibility = Visibility.Hidden;

            }));
        }

        #endregion

        #region Loading Business Report

        private void LoadBusinessReportAction()
        {
            FiltersSettingLayout.IsEnabled = false;
            LoadBusinessReportButton.IsEnabled = false;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "CSV files (*.csv) | *.csv";
            if ((bool)ofd.ShowDialog())
            {
                ProgressBusinessReportInfo.Visibility = Visibility.Visible;
                _model.LoadBusinessReport(ofd.FileName);
                
            }
            else
            {
                LoadLendedPriceButton.IsEnabled = true;
                FiltersSettingLayout.IsEnabled = true;
                LoadBusinessReportButton.IsEnabled = true;
                LoadAllDataButton.IsEnabled = true;
                LoadAllDataButton.Background = Brushes.DarkGreen;
                LoadAllDataButton.Content = "Run All";
                IsRunAll = false;
                TotalProgressBar.Visibility = Visibility.Hidden;
            }

           
        }
        private void LoadBusinessReportButton_Click(object sender, RoutedEventArgs e)
        {
            if (_model.Filters.SKUFilters.Count == 0)
            {
                MessageBox.Show("Please add at least one SKU number!");
                return;
            }
            IsRunAll = false;
            _model.ResetLoading();
            LoadInventoryReportButton.IsEnabled = true;
            LoadLendedPriceButton.IsEnabled = false;
            BusinessReportStatusImage.Source = StatusImageFactory.GrayArrow;
            InventoryReportStatusImage.Source = StatusImageFactory.GrayArrow;
            LandedReportStatusImage.Source = StatusImageFactory.GrayArrow;
            DataCombineStatusImage.Source = StatusImageFactory.GrayArrow;
            LoadBusinessReportAction();
        }
        private void _model__bisunessReportLoaded(object sender, EventArgs e)
        {
            if (!IsRunAll)
            {
                LoadBusinessReportButton.IsEnabled = true;
            }
            
            Dispatcher.BeginInvoke(new Action(() => {
                ProgressBusinessReportInfo.Visibility = Visibility.Hidden;
                BusinessReportStatusImage.Source = StatusImageFactory.GreenArrow;

                if (IsRunAll)
                {
                    while (!_model.IsBusinessReportLoaded())
                        continue;
                    LoadInventoryReportAction();
                }
                    
            }));
        }
        private void _model__bisunessReportError(object sender, EventArgs e)
        {
            MessageBox.Show("Error while processing business report file! Check file./// "+ ((ReportEventArgs)e).Message, "Error!");
            Dispatcher.BeginInvoke(new Action(() => {
                ProgressBusinessReportInfo.Visibility = Visibility.Hidden;
                BusinessReportStatusImage.Source = StatusImageFactory.RedArrow;
                LoadBusinessReportButton.IsEnabled = true;


            }));
        }

        #endregion

        #region Loading Inventory Report
        private void LoadInventoryReportAction()
        {
            LoadInventoryReportButton.IsEnabled = false;

            ProgressInventoryReportInfo.Visibility = Visibility.Visible;
            SimpleLog.WriteLog("LoadInventoryReport...");
            _model.LoadInventoryReport();
        }

        private void LoadInventoryReportButton_Click(object sender, RoutedEventArgs e)
        {
            if (_model.Filters.SKUFilters.Count == 0)
            {
                MessageBox.Show("Please add at least one SKU number!");
                return;
            }
            IsRunAll = false;
            LoadInventoryReportAction();
        }

        private void _model__inventoryReportLoadingStep(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() => {
                InventoryReportProgressLabel.Content = _model.InventoryReportLoadingStepDescription;
            }));
        }
        private void _model__inventoryReportLoaded(object sender, EventArgs e)
        {
          

            Dispatcher.BeginInvoke(new Action(() => {
                ProgressBusinessReportInfo.Visibility = Visibility.Hidden;
                BusinessReportStatusImage.Source = StatusImageFactory.GreenArrow;
                if (IsRunAll)
                {
                    while (!_model.IsBusinessReportLoaded())
                        continue;
                }
                if (!IsRunAll)
                {
                    LoadLendedPriceButton.IsEnabled = true;
                    LoadInventoryReportButton.IsEnabled = true;
                }

            }));



            Dispatcher.BeginInvoke(new Action(() => {
                ProgressInventoryReportInfo.Visibility = Visibility.Hidden;
                if (!IsRunAll)
                {
                    LoadBusinessReportButton.IsEnabled = true;
                    InventoryReportStatusImage.Source = StatusImageFactory.GreenArrow;
                }
                InventoryReportStatusImage.Source = StatusImageFactory.GreenArrow;
                LoadLandedPriceAction();
            }));
        }
        private void _model__inventoryReportError(object sender, EventArgs e)
        {
            MessageBox.Show("Error while processing inventory report! Check internet connection, and try later.", "Error!");
            Dispatcher.BeginInvoke(new Action(() =>
            {
                ProgressInventoryReportInfo.Visibility = Visibility.Hidden;
                InventoryReportStatusImage.Source = StatusImageFactory.RedArrow;
                FiltersSettingLayout.IsEnabled = true;
                LoadInventoryReportButton.IsEnabled = true;
            }));
        }

        #endregion

        public ASINOptimizationContext Context { get; set; }

        public int CompilationMark
        {
            get
            {
                return compilationMark;
            }

            set
            {
                compilationMark = value;
            }
        }

        public void SaveCurrentState()
        {
            
        }

        public event NavigateEventHandler Navigated;

        private void LoadLandedPriceAction()
        {
            if (IsRunAll)
            {
                if (_model.IsBusinessReportLoaded())
                {
                    LoadLendedPriceButton.IsEnabled = false;
                    ProgressLandedPriceInfo.Visibility = Visibility.Visible;

                    _model.LoadLandedPrice();
                }
            }
            else
            {
                if (_model.IsBusinessReportLoaded())
                {
                    LoadLendedPriceButton.IsEnabled = false;
                    ProgressLandedPriceInfo.Visibility = Visibility.Visible;

                    _model.LoadLandedPrice();
                }
                else
                {
                    MessageBox.Show("Please upload Business Report first!");
                }
            
            }
            
        }

        private void LoadLendedPriceButton_Click(object sender, RoutedEventArgs e)
        {
            if (_model.Filters.SKUFilters.Count == 0)
            {
                MessageBox.Show("Please add at least one SKU number!");
                return;
            }
            IsRunAll = false;
            
                LoadLandedPriceAction();
        }

        private void LoadAllDataButton_Click(object sender, RoutedEventArgs e)
        {
            if(_model.Filters.SKUFilters.Count ==0)
            {
                MessageBox.Show("Please add at least one SKU number!");
                return;
            }
            if (IsRunAll)
            {
                ProgressBusinessReportInfo.Visibility = Visibility.Hidden;
                ProgressInventoryReportInfo.Visibility = Visibility.Hidden;
                ProgressLandedPriceInfo.Visibility = Visibility.Hidden;
                ProgressDataCombining.Visibility = Visibility.Hidden;
                TotalProgressBar.Visibility = Visibility.Hidden;

                _model.ResetLoading();
                IsRunAll = false;
                return;
            }
            LoadLendedPriceButton.IsEnabled = false;
            RemappingPage.IsEnabled = false;
            ExportToExcel.IsEnabled = false;
            LoadAllDataButton.IsEnabled = false;
            _model.ResetLoading();
            LoadAllDataButton.Background = Brushes.DarkOrange;
            LoadAllDataButton.Content = "Cancel All";
            IsRunAll = true;
            TotalProgressBar.Visibility = Visibility.Visible;
            BusinessReportStatusImage.Source = StatusImageFactory.GrayArrow;
            InventoryReportStatusImage.Source = StatusImageFactory.GrayArrow;
            LandedReportStatusImage.Source = StatusImageFactory.GrayArrow;
            DataCombineStatusImage.Source = StatusImageFactory.GrayArrow;
            LoadBusinessReportAction();
            
        }

        private void _model__LandedPriceLoaded(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() => {
                ProgressLandedPriceInfo.Visibility = Visibility.Hidden;
                if (LandedPriceProcessor.LimitReached)
                {
                    MessageBox.Show("You exceeded your quota of 200.0 requests per 1 hour for operation");
                }

            }));
        }

        private void _model__LendedPriceError(object sender, EventArgs e)
        {
            MessageBox.Show("Error while processing Buy Box Price! Check internet connection, and try later.", "Error!");
            Dispatcher.BeginInvoke(new Action(() =>
            {
                ProgressLandedPriceInfo.Visibility = Visibility.Hidden;
                LoadLendedPriceButton.IsEnabled = true;
            }));
        }

        private void RemappingPage_Click(object sender, RoutedEventArgs e)
        {
            SaveCurrentState();
            if (Navigated != null)
            {
                Navigated(this,
                    new NavigateEventArgs
                    {
                        NeedToKeepBackNavigation = true,
                        Target = UIModules.ASINRemapping,
                         NavigationParameters = new object[] { compilationMark }
                    });
            }
        }

        private void SKUFilterAddToListButton_Click(object sender, RoutedEventArgs e)
        {
            var text = SKUFilterTextBox.Text;
            if (SKUFilterTextBox.Text.Trim().Length > 0 && !_model.Filters.SKUFilters.Contains(SKUFilterTextBox.Text.Replace("\n", "")))
                _model.Filters.SKUFilters.Add(SKUFilterTextBox.Text.Replace("\n", ""));
            SKUFilterTextBox.Text = "";
        }

        private void SKUFilterRemoveFromListButton_Click(object sender, RoutedEventArgs e)
        {
            if (SKUFiltersListBox.Items.Count > 0 && SKUFiltersListBox.SelectedItem != null)
                _model.Filters.SKUFilters.Remove((string)SKUFiltersListBox.SelectedItem);
        }

        private void BrandFilterAddToListButton_Click(object sender, RoutedEventArgs e)
        {
            if (BrandFilterTextBox.Text.Trim().Length > 0 && !_model.Filters.BrandFilters.Contains(BrandFilterTextBox.Text.Replace("\n", "")))
                _model.Filters.BrandFilters.Add(BrandFilterTextBox.Text.Replace("\n",""));
            BrandFilterTextBox.Text = "";
        }

        private void BrandFilterRemoveFromListButton_Click(object sender, RoutedEventArgs e)
        {
            if (BrandFiltersListBox.Items.Count > 0 && BrandFiltersListBox.SelectedItem != null)
                _model.Filters.BrandFilters.Remove((string)BrandFiltersListBox.SelectedItem);
        }

        private void ExportToExcel_Click(object sender, RoutedEventArgs e)
        {
            _model.ExportRawDataToEXEL();
        }

        
    }
}