﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;
using FSI.ASINOptimization.Services.Amazon;
using Microsoft.Win32;
using System.ComponentModel;
using System.Threading;

namespace FSI.ASINOptimization.UI
{
    /// <summary>
    /// Interaction logic for FeedProcessorView.xaml
    /// </summary>
    /// 
    public partial class ASINRemapping : UserControl, IASINOptimizationView
    {
        private ASINRemappingViewModel _model;
        private int compilationMark;
        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor)e.PropertyDescriptor;
            e.Column.Header = propertyDescriptor.DisplayName;
            if (propertyDescriptor.DisplayName == "IsInDesignMode")
            {
                e.Cancel = true;
            }
        }
        
        public ASINRemapping(ASINOptimizationContext context = null, int compilation_Mark = 0)
        {
            InitializeComponent();
            Context = context;
            compilationMark = compilation_Mark;
            if(_model==null || _model.IsModelLoaded == false)
            {
                _model = new ASINRemappingViewModel(context);
                var task = new Task(() =>
                {
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        spinner.loadBox.Text = "Loading Reports Data...";
                        Thread.Sleep(1500);
                        RemappingDataViewModel.itemLoaded += RemappingDataViewModel_itemLoaded;
                    }));
                    _model.LoadData();
                    Dispatcher.BeginInvoke(new Action(() =>
                    {

                        BrandsBox.ItemsSource = _model.MapData.OrderBy(x=>x.KeyBrand);
                        spinner.Visibility = Visibility.Hidden;
                    }));
                
                });
                task.Start();
            }
        }
        public void StopBackgroundTasks()
        {

        }
        private void RemappingDataViewModel_itemLoaded(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                spinner.loadBox.Text = string.Format("Grouping and processing Data... {0}%",Math.Round((double)RemappingDataViewModel.LoadedItemCount*100/ RemappingDataViewModel.ItemsToLoadCount));
            }));
        }
        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            //get parent item
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);

            //we've reached the end of the tree
            if (parentObject == null) return null;

            //check if the parent matches the type we're looking for
            T parent = parentObject as T;
            if (parent != null)
                return parent;
            else
                return FindParent<T>(parentObject);
        }

        private void ComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //_model.Data.First(x=>x.)
            var comboBox = sender as ComboBox;
            if (comboBox.SelectedItem == null)
                return;
            var selectedItem = (RemappingDataViewModel)comboBox.SelectedItem;
            var selectedIndex = comboBox.SelectedIndex;
            var currentRow = (RemappingDataViewModel)comboBox.DataContext;
            //var changedRow = ((IList)(RemappingDataViewModel).Items.IndexOf).fir
            var changedRow = FindParent<DataGrid>(comboBox).Items.SourceCollection.Cast<RemappingDataViewModel>().FirstOrDefault(x => x.ASIN == selectedItem.ASIN &&
                                                                                                        x.SKU == selectedItem.SKU &&
                                                                                                        x.Sessions == selectedItem.Sessions &&
                                                                                                        x.Quantity == selectedItem.Quantity);
            //_model.MapData.First(x=>x.Data.FirstOrDefault().Brand == selectedItem.Brand)
            if (changedRow == null)
                return;

            changedRow.ChangedTo = currentRow.ASIN;
        }

        public ASINOptimizationContext Context { get; set; }

        public int CompilationMark
        {
            get
            {
                return compilationMark;
            }

            set
            {
                compilationMark = value;
            }
        }

        public void SaveCurrentState()
        {
            
        }

        public event NavigateEventHandler Navigated;

        private void GoBack_Click(object sender, RoutedEventArgs e)
        {
            SaveCurrentState();
            if (Navigated != null)
            {
                Navigated(this, new NavigateEventArgs { IsBackNavigation = true, Target = UIModules.Reports, NeedToKeepBackNavigation = true });
            }
        }

        private void CommitEndExportButton_Click(object sender, RoutedEventArgs e)
        {
            _model.ExportDataToEXEL();
        }
    }
}
