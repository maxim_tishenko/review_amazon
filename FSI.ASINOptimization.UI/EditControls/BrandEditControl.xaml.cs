﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;

namespace FSI.ASINOptimization.UI.EditControls
{
    /// <summary>
    /// Interaction logic for BrandEditControl.xaml
    /// </summary>
    public partial class BrandEditControl : UserControl, IEditControl
    {
        public BrandEditControl()
        {
            InitializeComponent();
        }

        private BrandLookupBrandEntityViewModel _model;

        public ASINOptimizationContext Context { get; set; }

        public object Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value as BrandLookupBrandEntityViewModel;
                if (_model != null)
                {
                    tbBrand.Text = _model.Brand;
                    tbCode.Text = _model.Code;
                }
            }
        }

        public bool Commit()
        {
            if (Context == null || Context.StorageContext == null)
            {
                throw new InvalidOperationException("Cannot commit to the empty context");
            }

            bool isValid = true;
            if (string.IsNullOrWhiteSpace(tbBrand.Text))
            {
                brandValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                brandValidation.Visibility = Visibility.Collapsed;
                if (_model.Id == 0 &&
                    Context.StorageContext.Brands.Any(bs => bs.Brand == tbBrand.Text.Trim()))
                {
                    duplicateValidation.Visibility = Visibility.Visible;
                    isValid = false;
                }
                else if (_model.Id != 0 && Context.StorageContext.Brands.Any(bs => bs.Id != _model.Id && bs.Brand == tbBrand.Text.Trim()))
                {
                    duplicateValidation.Visibility = Visibility.Visible;
                    isValid = false;
                }
                else
                {
                    duplicateValidation.Visibility = Visibility.Hidden;
                }
            }

            if (string.IsNullOrWhiteSpace(tbCode.Text))
            {
                codeValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                codeValidation.Visibility = Visibility.Hidden;
            }

            if (tbBrand.Text.Length >= 255)
            {
                brandLengthValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else if (tbCode.Text.Length >= 255)
            {
                brandLengthValidation.Visibility = Visibility.Collapsed;
                CodeLengthValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                CodeLengthValidation.Visibility = Visibility.Collapsed;
                brandLengthValidation.Visibility = Visibility.Collapsed;
            }

            //if (Context.StorageContext.Brands.FirstOrDefault(x => x.Brand == tbColor.Text.Trim() && x.Style == tbStyle.Text.Trim()) != null)
            //{
            //    isValid = false;
            //    colorStyleIndividual.Visibility = Visibility.Visible;

            //}
            //else
            //{
            //    colorStyleIndividual.Visibility = Visibility.Collapsed;
            //}

            if (isValid)
            {
                _model.Brand = tbBrand.Text.Trim();
                _model.Code = tbCode.Text.Trim();

            }

            return isValid;
        }
    }
}
