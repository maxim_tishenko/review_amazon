﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;

namespace FSI.ASINOptimization.UI.EditControls
{
    /// <summary>
    /// Interaction logic for BrandSizeEditControl.xaml
    /// </summary>
    public partial class BrandSizeEditControl : UserControl, IEditControl
    {
        public BrandSizeEditControl()
        {
            InitializeComponent();
        }

        private BrandLookupSizeEntityViewModel _model;

        public ASINOptimizationContext Context { get; set; }

        public object Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value as BrandLookupSizeEntityViewModel;
                if (_model != null)
                {
                    tbSize.Text = _model.Size;
                    tbCode.Text = _model.Code;
                }
            }
        }

        public bool Commit()
        {
            if (Context == null || Context.StorageContext == null)
            {
                throw new InvalidOperationException("Cannot commit to the empty context");
            }

            bool isValid = true;
            if (string.IsNullOrWhiteSpace(tbSize.Text))
            {
                sizeValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                sizeValidation.Visibility = Visibility.Collapsed;
                if (_model.Id == 0 &&
                    Context.StorageContext.BrandSizes.Any(bs => bs.Size == tbSize.Text.Trim()))
                {
                    duplicateValidation.Visibility = Visibility.Visible;
                    isValid = false;
                }
                else if (_model.Id != 0 && Context.StorageContext.BrandSizes.Any(bs => bs.Id != _model.Id && bs.Size == tbSize.Text.Trim()))
                {
                    duplicateValidation.Visibility = Visibility.Visible;
                    isValid = false;
                }
                else
                {
                    duplicateValidation.Visibility = Visibility.Hidden;
                }
            }

            if (string.IsNullOrWhiteSpace(tbCode.Text))
            {
                codeValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                codeValidation.Visibility = Visibility.Hidden;
            }
            if (tbSize.Text.Length >= 255)
            {
                sizeLengthValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else if (tbCode.Text.Length >= 255)
            {
                sizeLengthValidation.Visibility = Visibility.Collapsed;
                codeLengthValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                codeLengthValidation.Visibility = Visibility.Collapsed;
                sizeLengthValidation.Visibility = Visibility.Collapsed;
            }

            if (isValid)
            {
                _model.Size = tbSize.Text.Trim();
                _model.Code = tbCode.Text.Trim();

            }

            return isValid;
        }
    }
}
