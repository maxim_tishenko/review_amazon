﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;

namespace FSI.ASINOptimization.UI.EditControls
{
    /// <summary>
    /// Interaction logic for BrandColorEditControl.xaml
    /// </summary>
    public partial class BrandColorEditControl : UserControl, IEditControl
    {
        public BrandColorEditControl()
        {
            InitializeComponent();
        }

        private BrandLookupColorEntityViewModel _model;

        public ASINOptimizationContext Context { get; set; }

        public object Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value as BrandLookupColorEntityViewModel;
                if (_model != null)
                {
                    tbStyle.Text = _model.Style;
                    tbColor.Text = _model.Color;
                    tbCode.Text = _model.Code;
                }
            }
        }

        public bool Commit()
        {
            if (Context == null || Context.StorageContext == null)
            {
                throw new InvalidOperationException("Cannot commit to the empty context");
            }

            bool isValid = true;
            if (string.IsNullOrWhiteSpace(tbColor.Text))
            {
                colorValidation.Visibility = Visibility.Visible;
                isValid = false;
            }else if (string.IsNullOrWhiteSpace(tbStyle.Text))
            {
                styleValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                colorValidation.Visibility = Visibility.Collapsed;
                styleValidation.Visibility = Visibility.Collapsed;
                if (_model.Id == 0 &&
                    Context.StorageContext.BrandColors.Any(bc => bc.Color == tbColor.Text.Trim() && bc.Style == tbStyle.Text.Trim()))
                {
                    duplicateStyleValidation.Visibility = Visibility.Visible;
                    duplicateValidation.Visibility = Visibility.Visible;
                    isValid = false;
                }
                else if(_model.Id != 0 && Context.StorageContext.BrandColors.Any(bc => bc.Id !=_model.Id && bc.Color == tbColor.Text.Trim() && bc.Style == tbStyle.Text.Trim())){
                    duplicateStyleValidation.Visibility = Visibility.Visible;
                    duplicateValidation.Visibility = Visibility.Visible;
                    isValid = false;
                }
                else
                {
                    duplicateValidation.Visibility = Visibility.Hidden;
                    duplicateStyleValidation.Visibility = Visibility.Hidden;
                }
            }

            
            if (string.IsNullOrWhiteSpace(tbCode.Text))
            {
                codeValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                codeValidation.Visibility = Visibility.Hidden;
            }

            if (tbColor.Text.Length >= 255)
            {
                ColorlengthValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else if(tbStyle.Text.Length >= 255)
            {
                ColorlengthValidation.Visibility = Visibility.Collapsed;
                StylelengthValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else if(tbCode.Text.Length >= 255)
            {
                StylelengthValidation.Visibility = Visibility.Collapsed;
                CodelengthValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                ColorlengthValidation.Visibility = Visibility.Collapsed;
                StylelengthValidation.Visibility = Visibility.Collapsed;
                CodelengthValidation.Visibility = Visibility.Collapsed;
            }

            //if (Context.StorageContext.BrandColors.FirstOrDefault(x => x.Color == tbColor.Text.Trim() && x.Style == tbStyle.Text.Trim()) != null)
            //{
            //    isValid = false;
            //    colorStyleIndividual.Visibility = Visibility.Visible;

            //}
            //else
            //{
            //    colorStyleIndividual.Visibility = Visibility.Collapsed;
            //}

            if (isValid)
            {
                _model.Style = tbStyle.Text.Trim();
                _model.Color = tbColor.Text.Trim();
                _model.Code = tbCode.Text.Trim();
            }

            
            return isValid;
        }
    }
}
