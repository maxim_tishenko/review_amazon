﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;

namespace FSI.ASINOptimization.UI.EditControls
{
    /// <summary>
    /// Interaction logic for FeedNameEditControl.xaml
    /// </summary>
    public partial class FeedNameEditControl : UserControl, IEditControl
    {
        public FeedNameEditControl()
        {
            InitializeComponent();
        }

        private FeedNameViewModel _model;

        public ASINOptimizationContext Context { get; set; }

        public object Model
        {
            get
            {
                return _model;
            }
            set
            {
                _model = value as FeedNameViewModel;
                if (_model != null)
                {
                    tbName.Text = _model.Name;
                }
            }
        }

        public bool Commit()
        {
            if (Context == null || Context.StorageContext == null)
            {
                throw new InvalidOperationException("Cannot commit to the empty context");
            }

            bool isValid = true;
            if (string.IsNullOrWhiteSpace(tbName.Text))
            {
                nameValidation.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                nameValidation.Visibility = Visibility.Collapsed;
                if (Context.StorageContext.SubmittedFeeds.Any(bs => bs.Name == tbName.Text.Trim())
                    || Context.StorageContext.SearchFeeds.Any(f => f.Name == tbName.Text.Trim()))
                {
                    duplicateValidation.Visibility = Visibility.Visible;
                    isValid = false;
                }
                else
                {
                    duplicateValidation.Visibility = Visibility.Hidden;
                }
            }

            if (isValid)
            {
                _model.Name = tbName.Text.Trim();
            }

            return isValid;
        }
    }
}
