﻿using System;
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;

namespace FSI.ASINOptimization.UI
{
    /// <summary>
    /// Interaction logic for BrandLookupPopulationView.xaml
    /// </summary>
    public partial class BrandLookupPopulationView : UserControl, IASINOptimizationView
    {
        private BrandLookupViewModel _model;

        public BrandLookupPopulationView(ASINOptimizationContext context = null, bool canGoBack = false)
        {
            InitializeComponent();
            Context = context;
            backButton.Visibility = canGoBack ? Visibility.Visible : Visibility.Hidden;
        }

        private ASINOptimizationContext _context;

        public ASINOptimizationContext Context
        {
            get
            {
                return _context;
            }
            set
            {
                _context = value;
                if (_context != null)
                {
                    _model = new BrandLookupViewModel(_context.StorageContext);
                    colorList.DataContext = _model.BrandColors;
                    sizeList.DataContext = _model.BrandSizes;
                    brandList.DataContext = _model.Brands;
                }
            }
        }

        public void SaveCurrentState()
        {
            _model.SaveChanges();
        }

        public event NavigateEventHandler Navigated;

        public void StopBackgroundTasks()
        {

        }
        private void OnBackNavigated(object sender, EventArgs e)
        {
            if (Navigated != null)
            {
                Navigated(this, new NavigateEventArgs {IsBackNavigation = true});
            }
        }

        private void OnEdit(object sender, RoutedEventArgs e)
        {
            Button source = (Button) sender;
            var editWindow = new EditWindow(source.DataContext, Context);
            if (editWindow.ShowDialog() == true)
            {
                if (source.DataContext is BrandLookupColorEntityViewModel)
                {
                    _model.UpdateColor((BrandLookupColorEntityViewModel) source.DataContext);
                }
                else if (source.DataContext is BrandLookupSizeEntityViewModel)
                {
                    _model.UpdateSize((BrandLookupSizeEntityViewModel) source.DataContext);
                }
                else if (source.DataContext is BrandLookupBrandEntityViewModel)
                {
                    _model.UpdateBrand((BrandLookupBrandEntityViewModel) source.DataContext);
                }
            }
        }

        private void OnDelete(object sender, RoutedEventArgs e)
        {
            Button source = (Button) sender;
            if (
                MessageBox.Show("Do you want to delete current item?", "Are you sure?", MessageBoxButton.YesNo,
                    MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
               
                if (source.DataContext is BrandLookupBrandEntityViewModel)
                {
                    _model.Brands.Remove((BrandLookupBrandEntityViewModel)source.DataContext);
                }
                else if (source.DataContext is BrandLookupColorEntityViewModel)
                {
                    _model.BrandColors.Remove((BrandLookupColorEntityViewModel)source.DataContext);
                }
                else if (source.DataContext is BrandLookupSizeEntityViewModel)
                {
                    _model.BrandSizes.Remove((BrandLookupSizeEntityViewModel)source.DataContext);
                }
            }
        }

        private void OnAddColor(object sender, RoutedEventArgs e)
        {
            var model = new BrandLookupColorEntityViewModel(new BrandLookupColor());
            try
            {
            var editWindow = new EditWindow(model, Context);
            if (editWindow.ShowDialog() == true)
            {
                    //if(_model.BrandColors.FirstOrDefault(x=>x.Color == model.Color&& x.Code == model.Code) == null)
                        _model.BrandColors.Add(model);
            }
        }catch(Exception ex)
            {
                _model.BrandColors.Remove(model);
                MessageBox.Show("Please input correct data.", "Incorrect format!");
            }
}

        private void OnAddSize(object sender, RoutedEventArgs e)
        {
            var model = new BrandLookupSizeEntityViewModel(new BrandLookupSize());
            try
            {
               
            var editWindow = new EditWindow(model, Context);
            if (editWindow.ShowDialog() == true)
            {
                    //if (_model.BrandSizes.FirstOrDefault(x => x.Size == model.Size && x.Code == model.Code) == null)
                        _model.BrandSizes.Add(model);
            }
}catch(Exception ex)
            {
                _model.BrandSizes.Remove(model);
                MessageBox.Show("Please input correct data.", "Incorrect format!");
            }
        }

        private void OnAddBrand(object sender, RoutedEventArgs e)
        {

            var model = new BrandLookupBrandEntityViewModel(new BrandLookupBrand());
            try
            {
                
            var editWindow = new EditWindow(model, Context);
            if (editWindow.ShowDialog() == true)
            {
                    //if (_model.Brands.FirstOrDefault(x => x.Brand == model.Brand && x.Code == model.Code) == null)
                        _model.Brands.Add(model);
            }
}catch(Exception)
            {
                _model.Brands.Remove(model);
                MessageBox.Show("Please input correct data.", "Incorrect format!");
            }
        }
    }
}
