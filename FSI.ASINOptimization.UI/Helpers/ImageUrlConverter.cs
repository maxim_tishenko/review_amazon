﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace FSI.ASINOptimization.UI.Helpers
{
    [ValueConversion(typeof(string), typeof(ImageSource))]
    public class ImageUrlConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof (ImageSource))
            {
                throw new InvalidOperationException("Type must be a string");
            }

            string url = (string) value;
            ImageSource result = null;
            if (!string.IsNullOrWhiteSpace(url))
            {
                var src = new BitmapImage();
                src.BeginInit();
                src.UriSource = new Uri(url, UriKind.Absolute);
                src.EndInit();
                result = src;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
