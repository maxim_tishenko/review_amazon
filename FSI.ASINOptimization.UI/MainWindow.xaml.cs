﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using FSI.ASINOptimization.Storage;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;
using System.Net;
using System.Globalization;
using FSI.ASINOptimization.Services.Amazon;

namespace FSI.ASINOptimization.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly NavigationContext _navigationContext = new NavigationContext();
        private ASINOptimizationContext _asinContext;

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("https://www.amazon.com/"))
                    {
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            var task = new Task(InitASINOptimizationApplication);
            task.Start();
        }

        private void InitASINOptimizationApplication()
        {
            _asinContext = new ASINOptimizationContext();
            Dispatcher.BeginInvoke(new Action(() =>
            {

                if (!CheckForInternetConnection())
                {
                    MessageBox.Show("Check your Internet Connection", "Oops!");
                    Close();
                }
                menu.DataContext =
                _navigationContext.GetMenuNavigation(new[]
                {UIModules.FeedProcessor, UIModules.BrandLookupPopulation,UIModules.Reports});
                currentContent.Content = _navigationContext.GetViewForActiveModule(_asinContext, new object[0]);
                ((IASINOptimizationView)currentContent.Content).Navigated += OnNavigateTo;
                title.Content = _navigationContext.ActiveModule.Title;
                spinner.Visibility = Visibility.Collapsed;
            }));
        }

        private void GoToPage_OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void GoToPage_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var target = (UIModules)e.Parameter;
            _navigationContext.SetActiveModule(target);
            var currentControl = (IASINOptimizationView)currentContent.Content;
            currentControl.Navigated -= OnNavigateTo;
            currentControl.StopBackgroundTasks();
            currentControl.SaveCurrentState();
            currentContent.Content = _navigationContext.GetViewForActiveModule(_asinContext, new object[0]);
            ((IASINOptimizationView)currentContent.Content).Navigated += OnNavigateTo;
            title.Content = _navigationContext.ActiveModule.Title;
        }

        private void OnNavigateTo(IASINOptimizationView view, NavigateEventArgs e)
        {
            var currentControl = (IASINOptimizationView)currentContent.Content;
            currentControl.Navigated -= OnNavigateTo;
            currentControl.StopBackgroundTasks();
            currentControl.SaveCurrentState();
            if (e.IsBackNavigation)
            {
                _navigationContext.ReactivatePreviousModule();
            }
            else
            {
                _navigationContext.SetActiveModule(e.Target, e.NeedToKeepBackNavigation);
            }
            currentContent.Content = _navigationContext.GetViewForActiveModule(_asinContext, e.NavigationParameters);
            ((IASINOptimizationView)currentContent.Content).Navigated += OnNavigateTo;
            title.Content = _navigationContext.ActiveModule.Title;
        }
    }
}
