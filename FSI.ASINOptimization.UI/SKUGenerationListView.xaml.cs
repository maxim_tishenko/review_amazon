﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;
using System.IO;

namespace FSI.ASINOptimization.UI
{
    /// <summary>
    /// Interaction logic for SKUGenerationListView.xaml
    /// </summary>
    public partial class SKUGenerationListView : UserControl, IASINOptimizationView
    {
        public SearchFeedViewModel Model { get; private set; }
        private Dictionary<int, Button> _pages;
        Task loadingPagesTask;
        bool loadBreaker = false;

        public SKUGenerationListView(ASINOptimizationContext context, SearchFeedViewModel model)
        {
            InitializeComponent();



            _pages = new Dictionary<int, Button>
            {
                {1, firstPage},
                {2, secondPage},
                {3, thirdPage},
                {4, forthPage},
                {5, fifthPage},
                {6, sixthPage},
                {7, seventhPage},
                {8, eighthPage},
                {9, ninethPage}
            };
            Context = context;
            Model = model;
            lock (Model.SyncRoot)
            {
                Model.LoadBreaker = false;
            }
            Model.pageLoaded += Model_pageLoaded;
            mainGrid.DataContext = Model;
            Model.PropertyChanged += (sender, e) =>
            {
                switch (e.PropertyName)
                {
                    case "CurrentPage":
                        SetPageNumber();
                        break;
                    case "PageCount":
                        RefreshPages();
                        break;
                }
            };
            RefreshPages();
            if (Model.ModelLoaded)
            {
                Model.ChangePage(Model.CurrentPage);
            }
            else
            {
                Model.ChangePage(1);

                spinner.Visibility = Visibility.Visible;
                HideRect.Visibility = Visibility.Visible;
                spinner.ellipse.Width = 18;
                spinner.ellipse.Height = 18;
                spinner.ellipse.StrokeThickness = 4;
                spinner.loadBox.Text = "Loading pages... ";
                spinner.loadBox.FontSize = 12;

                BackButton.IsEnabled = false;
                DeleteFeedButton.IsEnabled = false;
                ExportButton.IsEnabled = false;
                loadingPagesTask = new Task(() =>
                {
                    Dispatcher.BeginInvoke(new Action(SwitchButtonsState));
                    Model.FillProductsFields();
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        SwitchButtonsState();
                        spinner.Visibility = Visibility.Collapsed;
                        HideRect.Visibility = Visibility.Hidden;
                        BackButton.IsEnabled = true;
                        DeleteFeedButton.IsEnabled = true;
                        ExportButton.IsEnabled = true;
                    }));
                });
                loadingPagesTask.Start();

            }

        }

        private void Model_pageLoaded(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                spinner.loadBox.Text = string.Format("Loading pages... {0}%", (Math.Round((double)Model.PageLoadCounter * 100 / Model.Items.Count)));
            }));
        }

        public void StopBackgroundTasks()
        {
            if (Model == null) return;

            lock (Model.SyncRoot)
            {
                Model.LoadBreaker = true;
            }
            if (loadingPagesTask != null)
                loadingPagesTask.Wait();
        }

        private void SwitchButtonsState()
        {
            foreach (var btn in _pages.Values)
            {
                btn.IsEnabled = !btn.IsEnabled;
            }
        }
        private void SetPageNumber()
        {
            int ninthPageNum = Convert.ToInt32(_pages[9].Content);
            int secondPageNum = Convert.ToInt32(_pages[2].Content);
            if (Model.PageCount > 10)
            {
                if (Model.CurrentPage >= ninthPageNum && ninthPageNum < Model.PageCount - 1)
                {
                    for (int i = 2; i <= 9; i++)
                    {
                        _pages[i].Content = Model.CurrentPage == Model.PageCount
                            ? Model.PageCount - 10 + i
                            : Convert.ToInt32(_pages[i].Content) + 1;
                    }
                    firstPageSeparator.Visibility = Visibility.Visible;
                    if (Convert.ToInt32(_pages[9].Content) == Model.PageCount - 1)
                    {
                        pageSeparator.Visibility = Visibility.Collapsed;
                    }
                }
                else if (Model.CurrentPage <= secondPageNum && secondPageNum > 2)
                {
                    for (int i = 2; i <= 9; i++)
                    {
                        _pages[i].Content = Model.CurrentPage == 1 ? i : Convert.ToInt32(_pages[i].Content) - 1;
                    }

                    if (Convert.ToInt32(_pages[9].Content) < Model.PageCount)
                    {
                        lastPage.Content = Model.PageCount;
                        lastPage.Visibility = Visibility.Visible;
                        pageSeparator.Visibility = Visibility.Visible;
                    }
                    if (Convert.ToInt32(_pages[2].Content) == 2)
                    {
                        firstPageSeparator.Visibility = Visibility.Collapsed;
                    }
                }
            }
            for (int i = 1; i <= 9; i++)
            {
                _pages[i].IsEnabled = Convert.ToInt32(_pages[i].Content) != Model.CurrentPage;
            }

            lastPage.IsEnabled = Convert.ToInt32(lastPage.Content) != Model.CurrentPage;
        }

        private void RefreshPages()
        {
            for (int i = 2; i <= 9; i++)
            {
                _pages[i].Visibility = Visibility.Collapsed;
            }

            lastPage.Visibility = Visibility.Collapsed;
            pageSeparator.Visibility = Visibility.Collapsed;
            firstPageSeparator.Visibility = Visibility.Collapsed;

            for (int i = 2; i <= 9 && i <= Model.PageCount - 1; i++)
            {
                _pages[i].Visibility = Visibility.Visible;
            }

            if (Convert.ToInt32(lastPage.Content) < Model.PageCount)
            {
                SetPageNumber();
                lastPage.Content = Model.PageCount;
                lastPage.Visibility = Visibility.Visible;
            }

            if (Convert.ToInt32(_pages[2].Content) > 2)
            {
                firstPageSeparator.Visibility = Visibility.Visible;
            }

            if (Convert.ToInt32(_pages[9].Content) < Model.PageCount)
            {
                lastPage.Content = Model.PageCount;
                lastPage.Visibility = Visibility.Visible;
                if (Convert.ToInt32(_pages[9].Content) < Model.PageCount - 1)
                {
                    pageSeparator.Visibility = Visibility.Visible;
                }
            }
        }

        private void ChangePage(object sender, EventArgs e)
        {
            var pageButton = sender as Button;
            if (pageButton != null)
            {
                Model.ChangePage(Convert.ToInt32(pageButton.Content));
            }
        }

        private ASINOptimizationContext _context;

        public ASINOptimizationContext Context
        {
            get
            {
                return _context;
            }
            set
            {
                _context = value;
            }
        }

        public void SaveCurrentState()
        {
            if (Model != null)
            {
                Model.SaveChanges();
            }
        }

        public event NavigateEventHandler Navigated;

        private void NextPage(object sender, EventArgs e)
        {
            if (Model.NextPageExist)
            {
                Model.ChangePage(Model.CurrentPage + 1);
            }
        }

        private void PrevPage(object sender, EventArgs e)
        {
            if (Model.PrevPageExist)
            {
                Model.ChangePage(Model.CurrentPage - 1);
            }
        }

        public void OnEdit(object sender, EventArgs e)
        {
            SaveCurrentState();
            var currentProduct = (MarketplaceProductViewModel)((FrameworkElement)sender).DataContext;
            if (Navigated != null)
            {
                Navigated(this,
                    new NavigateEventArgs
                    {
                        NeedToKeepBackNavigation = true,
                        Target = UIModules.SKUGeneratorDetails,
                        NavigationParameters = new object[] { Model, Model.Items.IndexOf(currentProduct) }
                    });
            }
        }

        public void OnDelete(object sender, EventArgs e)
        {
            var productToDelete = ((FrameworkElement)sender).DataContext as MarketplaceProductViewModel;
            if (productToDelete != null)
            {
                Model.RemoveItem(productToDelete);
                _context.StorageContext.MarketplaceProducts.Remove(productToDelete.OriginalModel);
                Model.SaveChanges();
            }
        }

        public void OnBackToFeedProcessor(object sender, RoutedEventArgs e)
        {
            SaveCurrentState();
            if (Navigated != null)
            {
                Navigated(this, new NavigateEventArgs { Target = UIModules.FeedProcessor });
            }
        }

        private void OnSeeOnAmazon(object sender, EventArgs e)
        {
            var currentProduct = (MarketplaceProductViewModel)((FrameworkElement)sender).DataContext;
            System.Diagnostics.Process.Start(String.Format("http://www.amazon.com/dp/{0}", currentProduct.ASIN));
        }

        public void OnDeleteFeed(object sender, RoutedEventArgs e)
        {
            if (Model.Id > 0)
            {
                StopBackgroundTasks();
                foreach (MarketplaceProduct marketplaceProduct in Model.OriginalModel.Items.ToList())
                {
                    Context.StorageContext.MarketplaceProducts.Remove(marketplaceProduct);
                }

                Context.StorageContext.SearchFeeds.Remove(Model.OriginalModel);
                Context.StorageContext.SaveChanges();
            }

            Model = null;
            if (Navigated != null)
            {
                Navigated(this, new NavigateEventArgs { Target = UIModules.FeedProcessor });
            }
        }

        public void OnSubmitFeed(object sender, RoutedEventArgs e)
        {

        }

        private void OnExport(object sender, RoutedEventArgs e)
        {
            Model.ExportDataToEXEL();
        }

        private void skuGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {

            if (sender is ListView)
            {
                SaveCurrentState();
                var listView = (ListView)sender;

                if (listView.SelectedItem is MarketplaceProductViewModel)
                {
                    MarketplaceProductViewModel currentProduct = (MarketplaceProductViewModel)listView.SelectedItem;
                    if (Navigated != null)
                    {
                        Navigated(this,
                            new NavigateEventArgs
                            {
                                NeedToKeepBackNavigation = true,
                                Target = UIModules.SKUGeneratorDetails,
                                NavigationParameters = new object[] { Model, Model.Items.IndexOf(currentProduct) }
                            });
                    }
                }
            }
        }
    }
}
