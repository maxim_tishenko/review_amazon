﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using System.IO;
using System.Globalization;
using Microsoft.Win32;
using System.Windows;
using System.Diagnostics;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public class SearchFeedViewModel : EntityViewModelBase<SearchFeed>
    {
        public object SyncRoot;
        private ASINOptimizationContext _context;
        public bool LoadBreaker { get; set; }

        public SearchFeedViewModel(SearchFeed model, ASINOptimizationContext context, int pageSize = 20) : base(model)
        {
            SyncRoot = new object();
            LoadBreaker = false;
            _context = context;
            _searchTerms = new ObservableCollection<SearchQuery>(model.SearchTerms);
            _searchTerms.CollectionChanged += HandleSearchTerms;
            _items =
                new ObservableCollection<MarketplaceProductViewModel>(
                    model.Items.Select(i => new MarketplaceProductViewModel(i)));
            _items.CollectionChanged += HandleItems;
            PagedItems = new ObservableCollection<MarketplaceProductViewModel>();
            PageSize = pageSize;
            foreach (MarketplaceProductViewModel productViewModel in Items.Take(PageSize))
            {
                PagedItems.Add(productViewModel);
            }
        }

        public int Id { get { return OriginalModel.Id; } }

        public DateTime Date
        {
            get { return OriginalModel.Date; }
            set { OriginalModel.Date = value;
                OnPropertyChanged("Date");
            }
        }
        public string Name
        {
            get { return OriginalModel.Name; }
            set
            {
                OriginalModel.Name = value;
                OnPropertyChanged("Name");
            }
        }

        private ObservableCollection<SearchQuery> _searchTerms;

        public ObservableCollection<SearchQuery> SearchTerms
        {
            get { return _searchTerms; }
        }

        private ObservableCollection<MarketplaceProductViewModel> _items;
        public ObservableCollection<MarketplaceProductViewModel> Items { get { return _items; } }

        public ObservableCollection<MarketplaceProductViewModel> PagedItems { get; }

        public void RemoveItem(MarketplaceProductViewModel item)
        {
            PagedItems.Remove(item);
            Items.Remove(item);
            _context.StorageContext.MarketplaceProducts.Remove(item.OriginalModel);
            if (CurrentPage < PageCount)
            {
                ChangePage(CurrentPage);
            }
            else if (PagedItems.Count == 0 && CurrentPage > 1)
            {
                ChangePage(CurrentPage - 1);
            }
        }

        private int _pageSize;

        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                if (value < 1) throw new InvalidOperationException("Cannot set negative page size");
                _pageSize = value;
                ChangePage(1);
                OnPropertyChanged("PageSize");
                OnPropertyChanged("PageCount");
            }
        }

        public int PageCount { get
            {
                return Items.Count / PageSize + (Items.Count % PageSize > 0 || Items.Count < PageSize ? 1 : 0);
            } }

        private int _currentPage = 1;

        public int CurrentPage
        {
            get { return _currentPage; }
            private set
            {
                _currentPage = value;
                OnPropertyChanged("CurrentPage");
                OnPropertyChanged("NextPageExist");
                OnPropertyChanged("PrevPageExist");
            }
        }

        public bool NextPageExist { get { return CurrentPage < PageCount; } }

        public bool PrevPageExist { get { return CurrentPage > 1; } }

        public int TotalCount { get { return Items.Count; } }

        public void ChangePage(int page)
        {
            if (page < 1 || page > PageCount) throw new ArgumentOutOfRangeException("page");

            CurrentPage = page;
            PagedItems.Clear();
            foreach (MarketplaceProductViewModel productViewModel in Items.Skip((CurrentPage - 1) * PageSize).Take(PageSize))
            {
                PagedItems.Add(productViewModel);
            }
        }

        private void HandleSearchTerms(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (SearchQuery searchQuery in e.NewItems)
                {
                    OriginalModel.SearchTerms.Add(searchQuery);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (SearchQuery searchQuery in e.OldItems)
                {
                    OriginalModel.SearchTerms.Remove(searchQuery);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                OriginalModel.SearchTerms.Clear();
            }
        }

        private void HandleItems(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (MarketplaceProductViewModel item in e.NewItems)
                {
                    OriginalModel.Items.Add(item.OriginalModel);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (MarketplaceProductViewModel item in e.OldItems)
                {
                    OriginalModel.Items.Remove(item.OriginalModel);

                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                OriginalModel.Items.Clear();
            }

            OnPropertyChanged("TotalCount");
            OnPropertyChanged("PageCount");
        }
        public string Status
        {
            get { return OriginalModel.Status; }
            set
            {
                OriginalModel.Status = value;
                OnPropertyChanged("Status");
            }
        }
        public bool ModelLoaded { get; set; }

        public event EventHandler pageLoaded;
        public int PageLoadCounter { get; set; }
        public void FillProductsFields()
        {
            PageLoadCounter = 0;
            ModelLoaded = false;
            foreach (MarketplaceProductViewModel marketplaceProduct in Items)
            {
                lock (SyncRoot)
                {
                    if (LoadBreaker)
                        break;
                }
                    
                //FillStyle(marketplaceProduct);
                FillBrand(marketplaceProduct);
                FillColor(marketplaceProduct);
                FillSize(marketplaceProduct);
                if (marketplaceProduct.ManualChanged == null || marketplaceProduct.ManualChanged == "No")
                marketplaceProduct.CalculateSKU();
                marketplaceProduct.SaveChanges();
                PageLoadCounter++;
                if (pageLoaded != null)
                    pageLoaded(this, new EventArgs());

            }
            ModelLoaded = true;
        }

        //private void FillStyle(MarketplaceProductViewModel marketplaceProduct)
        //{
        //    if (string.IsNullOrWhiteSpace(marketplaceProduct.Style))
        //    {
        //        foreach (BrandLookupStyle brandLookupStyle in _context.StorageContext.BrandStyles)
        //        {
        //            if (
        //                marketplaceProduct.Title.IndexOf(brandLookupStyle.Style,
        //                    StringComparison.CurrentCultureIgnoreCase) >= 0)
        //            {
        //                marketplaceProduct.StyleMapped = brandLookupStyle;
        //            }
        //        }
        //    }
        //    else if (marketplaceProduct.StyleMapped == null)
        //    {
        //        marketplaceProduct.StyleMapped =
        //            _context.StorageContext.BrandStyles.FirstOrDefault(s => marketplaceProduct.Style.Contains(s.Style));
        //        //_context.StorageContext.BrandStyles.FirstOrDefault(s => s.Style == marketplaceProduct.Style);
        //    }
        //}

        private void FillColor(MarketplaceProductViewModel marketplaceProduct)
        {
            if (!string.IsNullOrWhiteSpace(marketplaceProduct.Color) && marketplaceProduct.ColorMapped == null)
            {
                marketplaceProduct.ColorMapped =
                    _context.StorageContext.BrandColors.FirstOrDefault(c => 
                        marketplaceProduct.Color.ToLower() == c.Color.ToLower() &&
                        marketplaceProduct.Title.ToLower().Contains(c.Style.ToLower()));
                //_context.StorageContext.BrandColors.FirstOrDefault(c => c.Color == marketplaceProduct.Color);
            }
        }

        private void FillBrand(MarketplaceProductViewModel marketplaceProduct)
        {
            if (!string.IsNullOrWhiteSpace(marketplaceProduct.Brand) && marketplaceProduct.BrandMapped == null)
            {
                marketplaceProduct.BrandMapped =
                    _context.StorageContext.Brands.FirstOrDefault(b =>marketplaceProduct.Brand.ToLower().Contains(b.Brand.ToLower()));
                //_context.StorageContext.Brands.FirstOrDefault(b => b.Brand == marketplaceProduct.Brand);
            }
        }

        private void FillSize(MarketplaceProductViewModel marketplaceProduct)
        {
            if (!string.IsNullOrWhiteSpace(marketplaceProduct.Size) && marketplaceProduct.SizeMapped == null)
            {
                foreach (var data in _context.StorageContext.BrandSizes
                    .OrderByDescending(size => size.Size.Length))
                { Debug.WriteLine(data); }

                var match =_context.StorageContext.BrandSizes.FirstOrDefault(s =>marketplaceProduct.Size.ToLower() == s.Size.ToLower());
                var contains = _context.StorageContext.BrandSizes
                    .OrderByDescending(size=>size.Size.Length)
                    .FirstOrDefault(s => marketplaceProduct.Size.ToLower().Contains(s.Size.ToLower()));

                marketplaceProduct.SizeMapped = (match != null) ? match : contains;
            }
        }

        public override void SaveChanges()
        {
            if (Id == 0)
            {
                _context.StorageContext.SearchFeeds.Add(OriginalModel);
            }

            foreach (var product in OriginalModel.Items.Where(i => i.Id == 0))
            {
                _context.StorageContext.MarketplaceProducts.Add(product);
            }
            _context.StorageContext.SaveChanges();
        }

        public void ExportDataToEXEL()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel(.csv) | *.csv ";
            if (sfd.ShowDialog() == true)
            {
                if (File.Exists(sfd.FileName))
                    File.Delete(sfd.FileName);
                using (StreamWriter sw = new StreamWriter(File.Open(sfd.FileName, FileMode.OpenOrCreate)))
                {
                    string s = Convert.ToString(CultureInfo.CurrentCulture.TextInfo.ListSeparator);
                    sw.WriteLine(MarketplaceProduct.Headers(s));
                    foreach (var item in OriginalModel.Items.OrderBy(x=>x.Id))
                    {
                        sw.WriteLine(item.ToExcelString(s));
                    }
                }
                MessageBox.Show("Sucessful Exported!","Success");
            }

            

        }
    }
}
