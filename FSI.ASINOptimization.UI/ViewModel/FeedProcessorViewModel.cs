﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.Services.Amazon;
using System.Threading;
using FSI.ASINOptimization.Services.Local;
using System.Windows;

namespace FSI.ASINOptimization.UI.ViewModel
{

    public delegate void SearchResultEventHandler(IEnumerable<MarketplaceProduct> result);
    public class FeedProcessorViewModel : UINotifyPropertyChangedBase
    {

        private bool isCanceledSearch = true;
        public object SyncRoot = new object();
        public bool isCompleatedSearch { get; set; }
        private Task searchTask;
        public event SearchResultEventHandler SearchResulted;
        public event EventHandler searchCanceledEvent;
        private ASINOptimizationContext _context;
        public SearchFeedViewModel CurrentFeed { get; private set; }
        public ObservableCollection<FeedInfo> Feeds { get; }
        public ObservableCollection<FeedInfo> FoundFeeds { get; } 
        public ObservableCollection<SearchQuery> SavedQueries { get; }

        private string _currentQuery;

        public string CurrentQuery
        {
            get { return _currentQuery; }
            set
            {
                _currentQuery = value;
                OnPropertyChanged("CurrentQuery");
            }
        }

        private Dictionary<string,SubmittedASIN> _inventoryAsins;
        private Dictionary<string, List<FeedInfo>> _asinToFeedMapping; 

        public void AddAsinToHistory(string asin)
        {
            _context.StorageContext.AsinHistory.Add(new AsinModel() { Asin = asin });
        }

        public HashSet<string> GetAsinHistory()
        {
            return _context.AsinHistoryHashSet;
        }
        public void SaveAsinHistory()
        {
            _context.StorageContext.SaveChanges();
            _context.UpdateAsinHistory();
        }
        public void CancelSearch()
        {
            
            if (ts == null || searchTask == null) return;
            ts.Cancel();
            InternetConnectionService.Instance.Cancel();
            try
            {
                searchTask.Wait(ts.Token);
            }
            catch (OperationCanceledException ex)
            {
                //ignored
            }
        }

        private void OnSearchResult(IEnumerable<MarketplaceProduct> products)
        {
            if (SearchResulted != null)
                SearchResulted(products);
        }

        private void PerformSearch(string query)
        {
            SimpleLog.WriteLog("PerformSearch started...");
            lock (SyncRoot)
            {
                isCompleatedSearch = false;
            }
            int counter = 0;
            InternetConnectionService.Instance.Reset();
            SimpleLog.WriteLog("InternetConnectionService.Instance.Reset()...");
            List<MarketplaceProduct> searchResult = new List<MarketplaceProduct>();

            if (!_context.IsSearchPossible(query))
            {
                MessageBox.Show("Search cannot be done. Check search query.");
                lock (SyncRoot)
                {
                    isCompleatedSearch = true;
                }
                OnSearchResult(searchResult);
                return;
            }
            SimpleLog.WriteLog("Going into main foreach...");
            foreach (var part in _context.GetProductChilds(query))
            {
                
                if (ct.IsCancellationRequested)
                {
                    isCanceledSearch = true;
                    SearchCanceledEvent();
                    MessageBox.Show("Search was canceled.");
                    ct.ThrowIfCancellationRequested();
                    
                }
                if (part == null)
                    continue;
                AmazonProductService.ProgressDataHelper.CurrentChildren++;
                if(!GetAsinHistory().Contains(part.ASIN))
                searchResult.Add(part);
                //counter++;
                //if (counter > 4)
                //{
                //    counter = 0;
                    OnSearchResult(searchResult);
                    searchResult = new List<MarketplaceProduct>();
                //}
            }

            lock (SyncRoot)
            {
                isCompleatedSearch = true;
            }
            OnSearchResult(searchResult);
            
        }
        public void SearchCanceledEvent()
        {
            if (searchCanceledEvent != null)
                searchCanceledEvent(this, new EventArgs());
        }
        public void SetCancelValue(bool val)
        {
            isCanceledSearch = val;
        }
        public bool CanStartSearch()
        {
            return isCanceledSearch;
        }
        public FeedProcessorViewModel(ASINOptimizationContext context)
        {
            isCompleatedSearch = true;
            _context = context;
            CurrentFeed = new SearchFeedViewModel(new SearchFeed(), context);
            Feeds = new ObservableCollection<FeedInfo>();

            if (SavedQueries == null)
            {
                SavedQueries = new ObservableCollection<SearchQuery>(context.StorageContext.SearchQueries.OrderByDescending(x=>x.Date).ToList());
            }
            _inventoryAsins = new Dictionary<string, SubmittedASIN>(StringComparer.InvariantCultureIgnoreCase);
            _asinToFeedMapping = new Dictionary<string, List<FeedInfo>>(StringComparer.CurrentCultureIgnoreCase);
            foreach (var asin in context.GetInventoryASINs())
            {
                if (!_inventoryAsins.ContainsKey(asin.Value))
                {
                    _inventoryAsins.Add(asin.Value, asin);
                }
            }
            foreach (
                SearchFeed searchFeed in
                    _context.StorageContext.SearchFeeds)
            {
                //FeedInfo feedInfo =
                //    context.StorageContext.SubmittedFeeds.FirstOrDefault(f => f.LocalFeed.Id == searchFeed.Id);
                //if (feedInfo == null)
                //{
                    var feedInfo = new FeedInfo
                    {
                        LocalFeed = searchFeed,
                        Status = searchFeed.Status,
                        Date = searchFeed.Date,
                        Name = searchFeed.Name,
                        ASINs =
                            searchFeed.Items.Select(it => new SubmittedASIN {SKU = it.SKU, Value = it.ASIN}).ToList()
                    };
                //}
                foreach (SubmittedASIN asin in feedInfo.ASINs)
                {
                    if (!_asinToFeedMapping.ContainsKey(asin.Value))
                    {
                        _asinToFeedMapping.Add(asin.Value, new List<FeedInfo> {feedInfo});
                    }
                    else
                    {
                        List<FeedInfo> feeds = _asinToFeedMapping[asin.Value];
                        if (!feeds.Contains(feedInfo))
                        {
                            feeds.Add(feedInfo);
                        }
                    }
                }
                Feeds.Add(feedInfo);
                
            }

            //foreach (FeedInfo remoteFeed in _context.GetRemoteFeeds())
            //{
            //    Feeds.Add(remoteFeed);
            //}
            FoundFeeds = new ObservableCollection<FeedInfo>(Feeds);
        }


        CancellationTokenSource ts;
        CancellationToken ct;

        public void ProcessSearchQuery(SearchQuery query)
        {

            if (query.Id == 0)
            {
                if (SavedQueries.FirstOrDefault(x => x.QueryString == query.QueryString) == null)
                {
                    SavedQueries.Add(query);
                    _context.StorageContext.SearchQueries.Add(query);
                    _context.StorageContext.SaveChangesAsync();
                }
            }
            FoundFeeds.Clear();
            //var asinsData = AmazonProductService.Instance.ListMatchingProductsRequest(query.QueryString);
            string searchQuery = query.QueryString;

            ts = new CancellationTokenSource();
            ct = ts.Token;

            CurrentFeed.Items.Clear();
            isCanceledSearch = false;
            searchTask = Task.Factory.StartNew(() => { PerformSearch(searchQuery); }, ts.Token);
 
        }

        public void ProcessSearchResult(IEnumerable<MarketplaceProduct> products)
        {
            foreach (MarketplaceProduct marketplaceProduct in products)
            {
                if (_asinToFeedMapping.ContainsKey(marketplaceProduct.ASIN))
                {
                    foreach (FeedInfo feedInfo in _asinToFeedMapping[marketplaceProduct.ASIN])
                    {
                        if (!FoundFeeds.Contains(feedInfo))
                        {
                            FoundFeeds.Add(feedInfo);
                        }
                    }
                }
                else if (!_inventoryAsins.ContainsKey(marketplaceProduct.ASIN))
                {
                    marketplaceProduct.Feed = CurrentFeed.OriginalModel;
                    CurrentFeed.Items.Add(new MarketplaceProductViewModel(marketplaceProduct));
                }
            }
        }
    }
}
