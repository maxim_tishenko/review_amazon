﻿using System.ComponentModel;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public abstract class UINotifyPropertyChangedBase : INotifyPropertyChanged
    {
        protected void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
