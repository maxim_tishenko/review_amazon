﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public class NavigationViewModel : UINotifyPropertyChangedBase
    {
        public UIModules UIModule { get; set; }

        public string Title { get; set; }

        private bool _active;

        public bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
                OnPropertyChanged("Active");
            }
        }
    }

    public enum UIModules
    {
        FeedProcessor,
        SKUGeneratorList,
        SKUGeneratorDetails,
        BrandLookupPopulation,
        Reports,
        ASINRemapping
        //Report
    }
}
