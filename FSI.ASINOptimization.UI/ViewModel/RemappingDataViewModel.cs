﻿using FSI.ASINOptimization.Services.Local;
using FSI.ASINOptimization.UI.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.UI.ViewModel
{

    public class MapASIN
    {
        public MapASIN()
        {
            Key = "";
            Value = "";
        }
        public MapASIN(string key,string value)
        {
            Key = key;
            Value = value;
        }
        public string Key { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
    public class RemappingDataViewModel:UINotifyPropertyChangedBase
    {
        private string itemName;
        private string brand;
        private string sKU;
        private string aSIN;
        private string quantity;
        private string sessions;
        private string pageViews;
        private string buyBoxPercentage;
        private string changedTo;
        private int index;
        private ObservableCollection<RemappingDataViewModel> aSINSource;
        public string SKU
        {
            get
            {
                return sKU;
            }

            set
            {
                sKU = value;
                OnPropertyChanged("SKU");
            }
        }

        public string ASIN
        {
            get
            {
                return aSIN;
            }

            set
            {
                aSIN = value;
                OnPropertyChanged("ASIN");
            }
        }

        public string Sessions
        {
            get
            {
                return sessions;
            }

            set
            {
                sessions = value;
                OnPropertyChanged("Sessions");
            }
        }

        public string PageViews
        {
            get
            {
                return pageViews;
            }

            set
            {
                pageViews = value;
                OnPropertyChanged("PageViews");
            }
        }

        public string LandedPrice
        {
            get
            {
                return buyBoxPercentage;
            }

            set
            {
                buyBoxPercentage = value;
                OnPropertyChanged("BuyBoxPercentage");
            }
        }

        public string Quantity
        {
            get
            {
                return quantity;
            }

            set
            {
                quantity = value;
                OnPropertyChanged("Quantity");
            }
        }

        public string ChangedTo
        {
            get
            {
                return changedTo;
            }

            set
            {
                changedTo = value;
                OnPropertyChanged("ChangedTo");
            }
        }

        public string Info
        {
            get
            {
                return this.ToString();
            }
        }

        public override string ToString()
        {
            return String.Format("ASIN:{0} Sessions:{1} PageViews:{2} LandedPrice:{3}",ASIN, Sessions, PageViews, LandedPrice);
        }

        public string ToExcelString(string separator)
        {
            return String.Format("{0}{8}{1}{8}{2}{8}{3}{8}{4}{8}{5}{8}{6}{8}{7}{8}",
                (ItemName != null) ? ItemName.Replace(separator," "): "null",
                (ASIN != null) ? ASIN : "null",
                (ChangedTo != null) ? ChangedTo : "null",
                (SKU != null) ? SKU : "null",
                (Sessions != null) ? Sessions.Replace(separator, " ") : "null",
                (PageViews != null) ? PageViews.Replace(separator, " ") : "null",
                (LandedPrice != null) ? LandedPrice : "null",
                (Quantity != null) ? Quantity : "null",
                separator);
        }

        public static String Headers(string separator)
        {
            return String.Format("ItemName{0} ASIN{0} ChangedTo{0} SKU{0} Sessions{0} PageViews{0} BuyBoxPercentage{0} Quantity{0}", separator);
        }
        public ObservableCollection<RemappingDataViewModel> ASINSource
        {
            get
            {
                return aSINSource;
            }

            set
            {
                aSINSource = value;
                OnPropertyChanged("ASINSource");
            }
        }

        public int Index
        {
            get
            {
                return index;
            }

            set
            {
                index = value;
            }
        }

        public static int LoadedItemCount { get; set; }
        public static int ItemsToLoadCount { get; set; }

        public string Brand
        {
            get
            {
                return brand;
            }

            set
            {
                brand = value;
            }
        }

        public string ItemName
        {
            get
            {
                return itemName;
            }

            set
            {
                itemName = value;
            }
        }

        public static event EventHandler itemLoaded;
        public static List<DataBindModel> LoadFromRemappingDataModels(List<RemappingDataModel> data)
        {
            ItemsToLoadCount = data.Count;
            LoadedItemCount = 0;
            List<RemappingDataViewModel> models = new List<RemappingDataViewModel>();
            foreach (var value in data)
            {
                var model = new RemappingDataViewModel()
                {
                    ItemName = value.ItemName,
                    Brand = value.Brand,
                    ASIN = value.ASIN,
                    LandedPrice = value.LandedPrice,
                    PageViews = value.PageViews,
                    Quantity = value.Quantity,
                    Sessions = value.Sessions,
                    SKU = value.SKU,
                    ChangedTo = "",
                    Index = value.Index
                };
                model.ASINSource = new ObservableCollection<RemappingDataViewModel>();
                
                foreach (var map in data)
                {
                    if (map.SKU.Split('@')[0] == model.SKU.Split('@')[0])
                    {
                        model.ASINSource.Add(new RemappingDataViewModel()
                        {
                            ItemName = map.ItemName,
                            Brand = map.Brand,
                            ASIN = map.ASIN,
                            LandedPrice = map.LandedPrice,
                            PageViews = map.PageViews,
                            Quantity = map.Quantity,
                            Sessions = map.Sessions,
                            SKU = map.SKU,
                            ChangedTo = "",
                            Index = map.Index
                            
                        });
                    }


                }
                    models.Add(model);
                LoadedItemCount++;
                if (itemLoaded != null)
                    itemLoaded(Guid.NewGuid(), new EventArgs());
            }
            //var result = models.GroupBy(x => x.Brand.Split(' ')[0]).Select(x => new DataBindModel() { Data = x.ToList(), KeyBrand = x.Key}).ToList();
            var result = models.GroupBy(x => x.Brand.Split(' ')[0].ToLower()).Select(x => new DataBindModel() { Data = x.ToList(), KeyBrand = x.Key.ToLower() }).ToList();
            //DataBindModel model1 = new DataBindModel();
            //model1.Data = models;
            return result;
        }

        private static ObservableCollection<string> GetASINVarianceCollectionForASIN(List<RemappingDataModel> data)
        {
            return new ObservableCollection<string>();
        }

    }
}
