﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Linq;
using FSI.ASINOptimization.Storage;
using FSI.ASINOptimization.Storage.Model;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public class BrandLookupViewModel
    {
        private readonly StorageContext _context;
        
        public ObservableCollection<BrandLookupColorEntityViewModel> BrandColors { get; set; }
        
        public ObservableCollection<BrandLookupSizeEntityViewModel> BrandSizes { get; set; }

        public ObservableCollection<BrandLookupBrandEntityViewModel> Brands { get; set; } 

        public BrandLookupViewModel(StorageContext context)
        {
            _context = context;
            
            BrandColors =
                new ObservableCollection<BrandLookupColorEntityViewModel>(
                    context.BrandColors.ToList().Select(bc => new BrandLookupColorEntityViewModel(bc)));
            BrandSizes =
                new ObservableCollection<BrandLookupSizeEntityViewModel>(
                    context.BrandSizes.ToList().Select(bs => new BrandLookupSizeEntityViewModel(bs)));
            Brands =
                new ObservableCollection<BrandLookupBrandEntityViewModel>(
                    context.Brands.ToList().Select(b => new BrandLookupBrandEntityViewModel(b)));

            BrandColors.CollectionChanged += (sender, e) =>
            {
                HandleAction(_context.BrandColors, e);
            };

            BrandSizes.CollectionChanged += (sender, e) =>
            {
                HandleAction(_context.BrandSizes, e);
            };

            Brands.CollectionChanged += (sender, e) =>
            {
                HandleAction(_context.Brands, e);
            };

        }

        private void HandleAction<TModel>(DbSet<TModel> set, NotifyCollectionChangedEventArgs e) where TModel : class, ICloneable
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (object newItem in e.NewItems)
                {
                    var model = (EntityViewModelBase<TModel>) newItem;
                    model.SaveChanges();
                    set.Add(model.OriginalModel);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (object oldItem in e.OldItems)
                {
                    set.Remove(((EntityViewModelBase<TModel>)oldItem).OriginalModel);
                }
            }

            _context.SaveChanges();
        }


        public void UpdateColor(BrandLookupColorEntityViewModel color)
        {
            color.SaveChanges();
            _context.SaveChanges();
        }

        public void UpdateSize(BrandLookupSizeEntityViewModel size)
        {
            size.SaveChanges();
            _context.SaveChanges();
        }

        public void UpdateBrand(BrandLookupBrandEntityViewModel brand)
        {
            brand.SaveChanges();
            _context.SaveChanges();
        }

        public void SaveChanges()
        {

            foreach (BrandLookupColorEntityViewModel brandColor in BrandColors)
            {
                brandColor.SaveChanges();
            }

            foreach (BrandLookupSizeEntityViewModel brandSize in BrandSizes)
            {
                brandSize.SaveChanges();
            }

            foreach (BrandLookupBrandEntityViewModel brand in Brands)
            {
                brand.SaveChanges();
            }

            _context.SaveChanges();
        }
    }

    public class BrandLookupBrandEntityViewModel : EntityViewModelBase<BrandLookupBrand>
    {
        public BrandLookupBrandEntityViewModel(BrandLookupBrand model) : base(model)
        {
        }

        public int Id { get { return OriginalModel.Id; } }

        public bool Enabled
        {
            get { return TempModel.Enabled; }
            set
            {
                TempModel.Enabled = value;
                OnPropertyChanged("Enabled");
            }
        }

        public string Code
        {
            get { return TempModel.Code; }
            set
            {
                TempModel.Code = value;
                OnPropertyChanged("Code");
            }
        }

        public string Brand
        {
            get { return TempModel.Brand; }
            set
            {
                TempModel.Brand = value;
                OnPropertyChanged("Brand");
            }
        }

        public override void SaveChanges()
        {
            OriginalModel.Brand = Brand;
            OriginalModel.Code = Code;
            OriginalModel.Enabled = Enabled;
        }
    }

    public class BrandLookupColorEntityViewModel : EntityViewModelBase<BrandLookupColor>
    {

        public bool Enabled
        {
            get { return TempModel.Enabled; }
            set
            {
                TempModel.Enabled = value;
                OnPropertyChanged("Enabled");
            }
        }

        public BrandLookupColorEntityViewModel(BrandLookupColor model) : base(model)
        {
        }

        public int Id { get { return OriginalModel.Id; } }

        public string Style
        {
            get { return TempModel.Style; }
            set
            {
                TempModel.Style = value;
                OnPropertyChanged("Style");
            }
        }

        public string Color
        {
            get { return TempModel.Color; }
            set
            {
                TempModel.Color = value;
                OnPropertyChanged("Color");
            }
        }

        public string Code
        {
            get { return TempModel.Code; }
            set
            {
                TempModel.Code = value;
                OnPropertyChanged("Code");
            }
        }

        public override void SaveChanges()
        {
            OriginalModel.Style = Style;
            OriginalModel.Color = Color;
            OriginalModel.Enabled = Enabled;
            OriginalModel.Code = Code;
        }
    }

    public class BrandLookupSizeEntityViewModel : EntityViewModelBase<BrandLookupSize>
    {

        public bool Enabled
        {
            get { return TempModel.Enabled; }
            set
            {
                TempModel.Enabled = value;
                OnPropertyChanged("Enabled");
            }
        }

        public BrandLookupSizeEntityViewModel(BrandLookupSize model) : base(model)
        {
        }

        public int Id { get { return OriginalModel.Id; } }

        public string Size
        {
            get { return TempModel.Size; }
            set
            {
                TempModel.Size = value;
                OnPropertyChanged("Size");
            }
        }

        public string Code
        {
            get { return TempModel.Code; }
            set
            {
                TempModel.Code = value;
                OnPropertyChanged("Code");
            }
        }

        public override void SaveChanges()
        {
            OriginalModel.Size = Size;
            OriginalModel.Enabled = Enabled;
            OriginalModel.Code = Code;
        }
    }
}
