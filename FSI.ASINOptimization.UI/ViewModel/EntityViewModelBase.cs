﻿using System;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public abstract class EntityViewModelBase<T> : UINotifyPropertyChangedBase where T : ICloneable
    {
        public T OriginalModel { get; }

        public T TempModel { get; }

        protected EntityViewModelBase(T model)
        {
            if (model == null) throw new ArgumentNullException("model");

            OriginalModel = model;

            TempModel = (T) model.Clone();
        }

        public abstract void SaveChanges();
    }
}
