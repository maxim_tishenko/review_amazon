﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public class SKUGenerationViewModel
    {
        private ASINOptimizationContext _context;

        public SearchFeedViewModel Feed { get; set; }


        public SKUGenerationViewModel(ASINOptimizationContext context, SearchFeed feed = null)
        {
            if (context == null) throw new ArgumentNullException("context");

            //Feed = feed == null ? new SearchFeedViewModel(new SearchFeed()) : new SearchFeedViewModel(feed); 
            _context = context;
        }


    }
}
