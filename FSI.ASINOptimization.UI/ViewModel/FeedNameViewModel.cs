﻿using System;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public class FeedNameViewModel : EntityViewModelBase<String>
    {
        public FeedNameViewModel(string model) : base(model)
        {
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public override void SaveChanges()
        {
            
        }
    }
}
