﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.Services.Amazon;
using System.Threading;
using FSI.ASINOptimization.Services.Local;
using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Globalization;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public class DataBindModel
    {
        public string KeyBrand { get; set; }
        public List<RemappingDataViewModel> Data { get; set; }
    }
    class ASINRemappingViewModel : UINotifyPropertyChangedBase
    {
        public bool IsModelLoaded { get; set; }
        private List<DataBindModel> data;
        public ASINRemappingViewModel(ASINOptimizationContext context)
        {
            _context = context;
            MapData = new List<DataBindModel>();
            IsModelLoaded = false;


        }
        private ASINOptimizationContext _context;

        public void LoadData()
        {
            MapData=RemappingDataViewModel.LoadFromRemappingDataModels(_context.GetCombinedReportsData());
            IsModelLoaded = true;
        }
        public List<DataBindModel> MapData
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

        

        public void ExportDataToEXEL()
        {
            var dataToExport = MapData.Select(x => new DataBindModel() { KeyBrand = x.KeyBrand, Data = x.Data.Where(d => d.ChangedTo != "").ToList() });
            if (dataToExport == null || dataToExport.Count() == 0)
            {
                MessageBox.Show("Nothing to Export!", "Warning");
                return;
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel(.csv) | *.csv ";
            if (sfd.ShowDialog() == true)
            {
                if (File.Exists(sfd.FileName))
                    File.Delete(sfd.FileName);
                using (StreamWriter sw = new StreamWriter(File.Open(sfd.FileName, FileMode.OpenOrCreate)))
                {
                    string s = Convert.ToString(CultureInfo.CurrentCulture.TextInfo.ListSeparator);
                    sw.WriteLine(RemappingDataViewModel.Headers(s));
                    foreach(var model in dataToExport)
                    {
                        foreach (var item in model.Data)
                        {
                            sw.WriteLine(item.ToExcelString(s));
                        }
                    }
                }

                // generate Letter

                var lettterpath = sfd.FileName.Substring(0, sfd.FileName.LastIndexOf('.')) + "txt";
                if (File.Exists(sfd.FileName))
                    File.Delete(sfd.FileName);
                using (StreamWriter sw = new StreamWriter(File.Open(sfd.FileName, FileMode.OpenOrCreate)))
                {
                    string s = Convert.ToString(CultureInfo.CurrentCulture.TextInfo.ListSeparator);
                    sw.WriteLine(RemappingDataViewModel.Headers(s));
                    foreach (var model in dataToExport)
                    {
                        foreach (var item in model.Data)
                        {
                            sw.WriteLine(item.ToExcelString(s));
                        }
                    }
                }
                MessageBox.Show("Sucessful Exported!", "Success");
            }
        }
    }
}
