﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.Services.Amazon;
using System.Threading;
using FSI.ASINOptimization.Services.Local;
using System.Windows;
using Microsoft.Win32;
using System.IO;
using System.Globalization;

namespace FSI.ASINOptimization.UI.ViewModel
{

    public class ReportEventArgs: EventArgs
    {
        public string Message { get; set; }
    }


    public class ReportsViewModel : UINotifyPropertyChangedBase
    {
        private ASINOptimizationContext _context;

        private RemappingFiltersModel _filters;
        CancellationTokenSource Combinets;
        CancellationToken Combinect;
        CancellationTokenSource Businessts;
        CancellationToken Businessct;
        CancellationTokenSource Inventoryts;
        CancellationToken Inventoryct;
        CancellationTokenSource Landedts;
        CancellationToken Landedct;
        private Task CombineDataTask;
        private Task BusinessAsyncTask;
        private Task InventoryAsyncTask;
        private Task LandedAsyncTask;

        public event EventHandler _reportsDataCombined;
        public event EventHandler _reportsDataCombineError;
        public event EventHandler _reportsDataCombineCanceled;

        public event EventHandler _bisunessReportLoaded;
        public event EventHandler _bisunessReportError;
        public event EventHandler _bisunessReportCanceled;

        public event EventHandler _inventoryReportLoaded;
        public event EventHandler _inventoryReportError;
        public event EventHandler _inventoryReportCanceled;

        public event EventHandler _inventoryReportLoadingStep;
        public event EventHandler _lendedPriceLoaded;
        public event EventHandler _lendedPriceError;
        public event EventHandler _lendedPriceCanceled;

        public string InventoryReportLoadingStepDescription;

        private bool IsReportsDataCombined;
        private bool IsBisunessReportLoaded;
        private bool IsInventoryReportLoaded;
        private bool IsLandedPriceLoaded;

        public RemappingFiltersModel Filters
        {
            get
            {
                return _filters;
            }

            set
            {
                _filters = value;
            }
        }

        public bool IsBusinessReportLoaded()
        {
            return IsBisunessReportLoaded;
        }
        public void ResetLoading()
        {
            IsReportsDataCombined = false;
            IsBisunessReportLoaded = false;
            IsInventoryReportLoaded = false;
            IsLandedPriceLoaded = false;
        }

        public bool IsRportsLoaded()
        {
            return IsBisunessReportLoaded && IsInventoryReportLoaded;
        }

        public bool IsDataCombineCanRun()
        {
            return IsBisunessReportLoaded && IsInventoryReportLoaded && IsLandedPriceLoaded;
        }

        public bool IsAllLoaded()
        {
            return IsBisunessReportLoaded && IsInventoryReportLoaded && IsLandedPriceLoaded && IsReportsDataCombined;
        }
        public void CombineReportsData() {
            Combinets = new CancellationTokenSource();
            Combinect = Combinets.Token;

            CombineDataTask = Task.Factory.StartNew(() => { CombineReportsDataProcess(); });
        }
        private void CombineReportsDataProcess()
        {
            try
            {
                _context.CombineReportsData();
                IsReportsDataCombined = true;
                if (_reportsDataCombined != null)
                {
                    _reportsDataCombined(this, new EventArgs());
                }
            }
            catch (Exception ex)
            {
                if (_reportsDataCombineError != null)
                {
                    _reportsDataCombineError(this, new ReportEventArgs() { Message = ex.Message });
                }
            }
           

            
        }
        public void LoadBusinessReport(string fileName)
        {
            Businessts = new CancellationTokenSource();
            Businessct = Businessts.Token;

            //BusinessAsyncTask = Task.Factory.StartNew(() => { LoadBusinessReportProcess(fileName); }, Businessts.Token);
            LoadBusinessReportProcess(fileName);
        }
        private void LoadBusinessReportProcess(string fileName)
        {
            try
            {
                _context.LoadBusinessReport(fileName,Filters.BrandFilters);
                IsBisunessReportLoaded = true;
                if (_bisunessReportLoaded != null)
                {
                    _bisunessReportLoaded(this, new EventArgs());
                }
            }
            catch (Exception ex)
            {
                if (_bisunessReportError != null)
                {
                    _bisunessReportError(this, new ReportEventArgs() { Message = ex.Message });
                }
            }
            
        }


        public void LoadInventoryReport()
        {
            Inventoryts = new CancellationTokenSource();
            Inventoryct = Inventoryts.Token;
            SimpleLog.WriteLog("LoadInventoryReport start...");
            InventoryAsyncTask = Task.Factory.StartNew(() => { LoadInventoryReportProcess(); }, Inventoryts.Token);
        }
        private void LoadInventoryReportProcess()
        {
            try
            {
                SimpleLog.WriteLog("Create report start..."); 
                InventoryReportLoadingStepDescription = "Creating Report Request ...";
                if (_inventoryReportLoadingStep != null)
                    _inventoryReportLoadingStep(this, new EventArgs());
                SimpleLog.WriteLog("Create report request start...");
                string reportRequestId = _context.CreateNewReportRequest((int)ReportTypes._GET_MERCHANT_LISTINGS_ALL_DATA_);
                string reportId = null;
                SimpleLog.WriteLog("wait...");
                InventoryReportLoadingStepDescription = "Waiting for request result ...";
                if (_inventoryReportLoadingStep != null)
                    _inventoryReportLoadingStep(this, new EventArgs());

                SimpleLog.WriteLog("try to get reportId...");
                while (reportId == null)
                {
                    try
                    {
                        SimpleLog.WriteLog("try to get reportId...");
                        reportId = _context.GetReportIdByReportRequestId(reportRequestId);
                        Thread.Sleep(10000);
                    }
                    catch(Exception ex)
                    {
                        SimpleLog.WriteLog("Exception thrown..." + ex.Message);
                        if (ex.InnerException != null)
                        {
                            SimpleLog.WriteLog("Exception thrown..." + ex.InnerException.Message);
                        }
                    }
                   
                }
                SimpleLog.WriteLog("successful loaded");
                InventoryReportLoadingStepDescription = "Processing report result ...";
                if (_inventoryReportLoadingStep != null)
                    _inventoryReportLoadingStep(this, new EventArgs());
                _context.LoadInventoryReport(reportId,Filters.SKUFilters);
                IsInventoryReportLoaded = true;
                if (_inventoryReportLoaded != null)
                    _inventoryReportLoaded(this, new EventArgs());
            }
            catch (Exception ex)
            {
                SimpleLog.WriteLog("error throw exception " + ex.Message);
                if (ex.InnerException != null)
                {
                    SimpleLog.WriteLog("error throw exception " + ex.InnerException.Message);
                }
                if (_inventoryReportError != null)
                {
                    _inventoryReportError(this, new ReportEventArgs() { Message = ex.Message });
                }
            }
            
        }


        public void LoadLandedPrice()
        {
            Landedts = new CancellationTokenSource();
            Landedct = Landedts.Token;
            //LoadLandedPriceProcess();
            LandedAsyncTask = Task.Factory.StartNew(() => { LoadLandedPriceProcess(); }, Landedts.Token);
        }
        private void LoadLandedPriceProcess()
        {
            try
            {
                _context.LoadLandedPrices();
                IsLandedPriceLoaded = true;
                if (_lendedPriceLoaded != null)
                    _lendedPriceLoaded(this, new EventArgs());
            }
            catch (Exception ex)
            {
                if (_lendedPriceError != null)
                {
                    _lendedPriceError(this, new ReportEventArgs() { Message = ex.Message });
                }
            }

        }

        public ReportsViewModel(ASINOptimizationContext context)
        {
            _context = context;
            IsBisunessReportLoaded = false;
            IsInventoryReportLoaded = false;
            IsLandedPriceLoaded = false;
            IsReportsDataCombined = false;
            Filters = new RemappingFiltersModel();
        }

        public void ExportRawDataToEXEL()
        {
            var dataToExport = _context.GetCombinedReportsData();
            if (dataToExport == null || dataToExport.Count() == 0)
            {
                MessageBox.Show("Nothing to Export!", "Warning");
                return;
            }

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel(.csv) | *.csv ";
            if (sfd.ShowDialog() == true)
            {
                if (File.Exists(sfd.FileName))
                    File.Delete(sfd.FileName);
                using (StreamWriter sw = new StreamWriter(File.Open(sfd.FileName, FileMode.OpenOrCreate)))
                {
                    string s = Convert.ToString(CultureInfo.CurrentCulture.TextInfo.ListSeparator);
                    sw.WriteLine(RemappingDataModel.Headers(s));
                    foreach (var model in dataToExport)
                    {
                        sw.WriteLine(model.ToExcelString(s));
                    }
                }

                MessageBox.Show("Sucessful Exported!", "Success");
            }
        }

    }
}
