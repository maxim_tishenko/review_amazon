﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public class RemappingFiltersModel: UINotifyPropertyChangedBase
    {
        public RemappingFiltersModel()
        {
            sKUFilters = new ObservableCollection<string>();
            brandFilters = new ObservableCollection<string>();
        }
        private ObservableCollection<string> sKUFilters;
        private ObservableCollection<string> brandFilters;


        public ObservableCollection<string> BrandFilters
        {
            get
            {
                return brandFilters;
            }

            set
            {
                brandFilters = value;
            }
        }

        public ObservableCollection<string> SKUFilters
        {
            get
            {
                return sKUFilters;
            }

            set
            {
                sKUFilters = value;
            }
        }
    }
}
