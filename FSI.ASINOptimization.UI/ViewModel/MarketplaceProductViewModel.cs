﻿using System;
using FSI.ASINOptimization.Storage.Model;

namespace FSI.ASINOptimization.UI.ViewModel
{
    public class MarketplaceProductViewModel : EntityViewModelBase<MarketplaceProduct>
    {
        public MarketplaceProductViewModel(MarketplaceProduct model) : base(model)
        {
        }

        public int Id { get { return OriginalModel.Id; } }

        private BrandLookupStyle _styleMapped;

        //public BrandLookupStyle StyleMapped
        //{
        //    get { return _styleMapped; }
        //    set
        //    {
        //        _styleMapped = value;
        //        //if (_styleMapped != null)
        //        //{
        //        //    TempModel.Style = _styleMapped.Style;
        //        //    OnPropertyChanged("Style");
        //        //}
        //        OnPropertyChanged("StyleMapped");
        //    }
        //}

        //public string Style
        //{
        //    get { return TempModel.Style; }
        //    set
        //    {
        //        TempModel.Style = value;
        //        StyleMapped = null;
        //        OnPropertyChanged("Style");
        //    }
        //}

        private BrandLookupColor _colorMapped;

        public BrandLookupColor ColorMapped
        {
            get { return _colorMapped; }
            set
            {
                _colorMapped = value;
                //if (_colorMapped != null)
                //{
                //    TempModel.Color = _colorMapped.Color;
                //    OnPropertyChanged("Color");
                //}
                OnPropertyChanged("ColorMapped");
            }
        }

        public string Color
        {
            get { return TempModel.Color; }
            set
            {
                TempModel.Color = value;
                ColorMapped = null;
                OnPropertyChanged("Color");
            }
        }

        private BrandLookupSize _sizeMapped;

        public BrandLookupSize SizeMapped
        {
            get { return _sizeMapped; }
            set
            {
                _sizeMapped = value;
                //if (_sizeMapped != null)
                //{
                //    TempModel.Size = _sizeMapped.Size;
                //    OnPropertyChanged("Size");
                //}
                OnPropertyChanged("SizeMapped");
            }
        }

        private BrandLookupBrand _brandMapped;

        public BrandLookupBrand BrandMapped
        {
            get { return _brandMapped; }
            set
            {
                _brandMapped = value;
                //if (_brandMapped != null)
                //{
                //    TempModel.Brand = _brandMapped.Brand;
                //    OnPropertyChanged("Brand");
                //}

                OnPropertyChanged("BrandMapped");
            }
        }

        public string Brand
        {
            get { return TempModel.Brand; }
            set
            {
                TempModel.Brand = value;
                OnPropertyChanged("Brand");
            }
        }

        public string Size
        {
            get { return TempModel.Size; }
            set
            {
                TempModel.Size = value;
                SizeMapped = null;
                OnPropertyChanged("Size");
            }
        }

        public string Title
        {
            get { return TempModel.Title; }
            set
            {
                TempModel.Title = value;
                OnPropertyChanged("Title");
            }
        }

        public string ImageSrc
        {
            get { return TempModel.ImageSrc; }
            set
            {
                TempModel.ImageSrc = value;
                OnPropertyChanged("ImageSrc");
            }
        }

        public string Description
        {
            get  { return string.Format("Brand: {0} Color: {1} Size: {2}", Brand, Color, Size); }
        }

        public string ASIN
        {
            get { return TempModel.ASIN; }
            set
            {
                TempModel.ASIN = value;
                OnPropertyChanged("ASIN");
            }
        }

        public bool ChildParentMatch { get { return ASIN == ParentASIN; } }
        public string ParentASIN
        {
            get { return TempModel.ParentASIN; }
            set
            {
                TempModel.ParentASIN = value;
                OnPropertyChanged("ParentASIN");
            }
        }

        public string SKU {
            get { return TempModel.SKU; }
            set { TempModel.SKU = value;
                ManualChanged = "Yes";
                OnPropertyChanged("SKU");
                SaveChanges();
            }
        }
        public string ManualChanged
        {
            get { return TempModel.ManualChanged; }
            set { TempModel.ManualChanged = value;
                OnPropertyChanged("ManualChanged");
            }
        }

        public bool CalculateSKU()
        {
            if (/*StyleMapped != null &&*/ ColorMapped != null && SizeMapped != null && BrandMapped != null)
            {
                TempModel.SKU = String.Format("{0}_{1}_{2}",BrandMapped.Code,/* StyleMapped.Code,*/ ColorMapped.Code, SizeMapped.Code);
                OnPropertyChanged("SKU");
                return true;
            }
            return false;
        }

        public override void SaveChanges()
        {
            OriginalModel.Color = TempModel.Color;
            OriginalModel.Size = TempModel.Size;
            OriginalModel.Title = TempModel.Title;
            OriginalModel.ASIN = TempModel.ASIN;
            OriginalModel.SKU = TempModel.SKU;
            OriginalModel.Brand = TempModel.Brand;
            OriginalModel.ImageSrc = TempModel.ImageSrc;
            OriginalModel.ManualChanged = TempModel.ManualChanged;
        }
    }
}
