﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FSI.ASINOptimization.Storage.Model;
using FSI.ASINOptimization.UI.Core;
using FSI.ASINOptimization.UI.ViewModel;
using FSI.ASINOptimization.Services.Amazon;
using FSI.ASINOptimization.Services.Local;
using System.Threading;

namespace FSI.ASINOptimization.UI
{
    /// <summary>
    /// Interaction logic for FeedProcessorView.xaml
    /// </summary>
    public partial class FeedProcessorView : UserControl, IASINOptimizationView
    {
        private FeedProcessorViewModel _model;

        public FeedProcessorView(ASINOptimizationContext context = null)
        {
            InitializeComponent();
            Context = context;
            _model = new FeedProcessorViewModel(context);
            _model.SearchResulted += OnSearchResulted;
            _model.searchCanceledEvent += _model_searchCanceledEvent;
            context._lostConnectionEvent += Context__lostConnectionEvent;
            counter.DataContext = _model.CurrentFeed;
            feedList.ItemsSource = _model.FoundFeeds;
            cbSearch.DataContext = _model;
        }

        private void _model_searchCanceledEvent(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                SearchButton.IsEnabled = true;
                StopSearch.Content = "Stop Search";
                StopSearch.IsEnabled = true;
                StopSearch.Visibility = Visibility.Hidden;
            }));
        }

        private void Context__lostConnectionEvent(object sender, string e)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                StatusLabel.Content = e;
            }));
        }

        private void OnSearchResulted(IEnumerable<MarketplaceProduct> result)
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                _model.ProcessSearchResult(result);

                ParentsLabel.Content = String.Format("Parent ASIN : {0}/{1}", AmazonProductService.ProgressDataHelper.CurrentParentAsin, AmazonProductService.ProgressDataHelper.ParentAsinsCount);
                ChildrensLabel.Content = String.Format("Children ASINs : {0}/{1}", AmazonProductService.ProgressDataHelper.CurrentChildren, AmazonProductService.ProgressDataHelper.ChildrensAsinsCount);
                StatusLabel.Content = "Working...";

                ProgressBox.Minimum = 0;
                ProgressBox.Value = AmazonProductService.ProgressDataHelper.CurrentChildren;
                ProgressBox.Maximum = AmazonProductService.ProgressDataHelper.ChildrensAsinsCount;


                if (_model.isCompleatedSearch)
                {
                    ProgressInfoPanel.Visibility = Visibility.Hidden;
                    StopSearch.Visibility = Visibility.Hidden;
                    _model.SetCancelValue(true);
                    SearchButton.IsEnabled = true;
                    CreateFeed.IsEnabled = true;
                }

            }));
        }

        public ASINOptimizationContext Context { get; set; }
        public void SaveCurrentState()
        {

        }

        public void StopBackgroundTasks()
        {
            bool isCompletedSearch;
            lock (_model.SyncRoot)
            {
                isCompletedSearch = _model.isCompleatedSearch;
            }
            if (!isCompletedSearch)
            {
                OnCancel(this, new RoutedEventArgs());
            }
        }
        public event NavigateEventHandler Navigated;

        private async void OnSearch(object sender, EventArgs e)
        {
            StopSearch.Content = "Stop Search";
            StopSearch.IsEnabled = true;

            if (cbSearch.Text == null || cbSearch.Text.Trim() == "")
            {
                MessageBox.Show("Please Enter query first!", "Oops!");
                return;
            }
            if (!_model.CanStartSearch())
            {
                MessageBox.Show("Please Wait, search is canceling...", "Oops!");
                return;
            }
            StatusLabel.Content = "Preparing loading...";

            ParentsLabel.Content = "Parent ASIN : 0/0";
            ChildrensLabel.Content = "Children ASINs : 0/0";
            StatusLabel.Content = "Preparing loading...";
            ProgressBox.Value = 0;

            SearchButton.IsEnabled = false;
            StopSearch.Visibility = Visibility.Visible;
            ProgressInfoPanel.Visibility = Visibility.Visible;
            AmazonProductService.ProgressDataHelper.Clear();
            ProgressBox.Value = 0;
            ProgressBox.Maximum = 100;
            ProgressBox.Minimum = 0;
            CreateFeed.IsEnabled = false;
            //SubmitAllFeed.IsEnabled = false;


            //List<string> parentAsins = AmazonRawProductService.QueryProductsParentASINs(_model.CurrentQuery);
            if (cbSearch.SelectedItem != null)
            {
                SimpleLog.WriteLog("search starting...");
                _model.ProcessSearchQuery((SearchQuery)cbSearch.SelectedItem);
            }
            else if (string.IsNullOrWhiteSpace(_model.CurrentQuery))
            {
                MessageBox.Show("Please, provide search query");
            }
            else
            {
                _model.ProcessSearchQuery(new SearchQuery { QueryString = _model.CurrentQuery, Date = DateTime.Now.ToString() });
                //List<string> parentAsins = AmazonRawProductService.QueryProductsParentASINs(_model.CurrentQuery);
            }

        }

        private void OnCreateFeed(object sender, EventArgs e)
        {
            if (_model.CurrentFeed.TotalCount == 0)
            {
                MessageBox.Show("There are no new asins to create feed.");
                return;
            }

            var model = new FeedNameViewModel("");
            var editWindow = new EditWindow(model, Context);

            var countItems = _model.CurrentFeed.OriginalModel.Items.Count;
            if (Navigated != null && editWindow.ShowDialog() == true)
            {
                foreach(var prodAsin in _model.CurrentFeed.OriginalModel.Items)
                {
                    _model.AddAsinToHistory(prodAsin.ASIN);
                }
                _model.SaveAsinHistory();
                _model.CurrentFeed.Name = model.Name;
                _model.CurrentFeed.Date = DateTime.Now;
                Navigated(this,
                    new NavigateEventArgs
                    {
                        NavigationParameters = new object[] { _model.CurrentFeed },
                        Target = UIModules.SKUGeneratorList,
                        NeedToKeepBackNavigation = false
                    });
            }
        }

        public void OnSelectFeed(object sender, EventArgs e)
        {
            var feed = ((FrameworkElement)sender).DataContext as FeedInfo;
            if (feed != null && feed.LocalFeed != null && Navigated != null)
            {
                var model = new SearchFeedViewModel(feed.LocalFeed, Context);
                Navigated(this, new NavigateEventArgs
                {
                    NavigationParameters = new object[] { model },
                    Target = UIModules.SKUGeneratorList,
                    NeedToKeepBackNavigation = false
                });
            }
        }

        private void OnSubmitFeeds(object sender, EventArgs e)
        {

        }

        private void OnCancel(object sender, RoutedEventArgs e)
        {
           
            //SubmitAllFeed.Visibility = Visibility.Visible;
            CreateFeed.IsEnabled = true;
            StopSearch.Content = "Stopping search...";
            StopSearch.IsEnabled = false;
            ProgressInfoPanel.Visibility = Visibility.Hidden;
            _model.CancelSearch();
            AmazonProductService.ProgressDataHelper.Clear();
            Thread.Sleep(1000);
            if (_model.isCompleatedSearch)
            {
                _model.SearchCanceledEvent();
                ProgressInfoPanel.Visibility = Visibility.Hidden;
                StopSearch.Visibility = Visibility.Hidden;
                _model.SetCancelValue(true);
                SearchButton.IsEnabled = true;
                CreateFeed.IsEnabled = true;
            }
        }

        private void OnDelete(object sender, RoutedEventArgs e)
        {
            var feedInfo = ((FrameworkElement)sender).DataContext as FeedInfo;

                foreach (MarketplaceProduct marketplaceProduct in feedInfo.LocalFeed.Items.ToList())
                {
                    Context.StorageContext.MarketplaceProducts.Remove(marketplaceProduct);
                }
                Context.StorageContext.SearchFeeds.Remove(feedInfo.LocalFeed);
                Context.StorageContext.SaveChanges();

            if (Navigated != null)
            {
                Navigated(this, new NavigateEventArgs { Target = UIModules.FeedProcessor });
            }
        }

    }
}
