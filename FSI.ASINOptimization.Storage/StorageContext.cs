﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FSI.ASINOptimization.Storage.Model;

namespace FSI.ASINOptimization.Storage
{
    public class StorageContext : DbContext
    {
        public StorageContext() : base("local")
        {
            Database.Connection.StateChange += Connection_StateChange;
            Database.SetInitializer(new CreateDatabaseIfNotExists<StorageContext>());
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<StorageContext>());
        }

        private void Connection_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            if(e.CurrentState == System.Data.ConnectionState.Broken)
            {
                Database.Connection.Open();
            }
        }

        public DbSet<AsinModel> AsinHistory { get; set; }
        public DbSet<BrandLookupColor> BrandColors { get; set; }
        public DbSet<BrandLookupSize> BrandSizes { get; set; }
        public DbSet<BrandLookupBrand> Brands { get; set; } 
        public DbSet<SearchQuery> SearchQueries { get; set; } 
        public DbSet<SearchFeed> SearchFeeds { get; set; }

        public DbSet<MarketplaceProduct> MarketplaceProducts { get; set; }
        public DbSet<FeedInfo> SubmittedFeeds { get; set; } 
        public DbSet<SubmittedASIN> ASINs { get; set; } 
         
    }
}
