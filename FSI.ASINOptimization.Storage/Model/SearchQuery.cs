﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FSI.ASINOptimization.Storage.Model
{
    public class SearchQuery : ICloneable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }

        [StringLength(255)]
        public string QueryString { get; set; }

        [StringLength(255)]
        public string Date { get; set; }
        public object Clone()
        {
            return MemberwiseClone();
        }

        public override string ToString()
        {
            return string.Format("{0} | {1}", QueryString, Date);
        }
    }
}
