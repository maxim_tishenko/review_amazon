﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FSI.ASINOptimization.Storage.Model
{
    public class BrandLookupBrand : ICloneable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }
        [StringLength(255)]
        public string Brand { get; set; }
        [StringLength(255)]
        public string Code { get; set; }

        public bool Enabled { get; set; }


        public object Clone()
        {
            return MemberwiseClone();
        }
    }

    public class BrandLookupStyle : ICloneable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity),Key]
        public int Id { get; set; }



        [StringLength(255)]
        public string Code { get; set; }
        public bool Enabled { get; set; }
        public object Clone()
        {
            return MemberwiseClone();
        }
    }

    public class BrandLookupColor : ICloneable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }

        [StringLength(255)]
        public string Style { get; set; }

        [StringLength(255)]
        public string Color { get; set; }

        [StringLength(255)]
        public string Code { get; set; }

        public bool Enabled { get; set; }
        public object Clone()
        {
            return MemberwiseClone();
        }
    }

    public class BrandLookupSize : ICloneable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }

        [StringLength(255)]
        public string Size { get; set; }

        [StringLength(255)]
        public string Code { get; set; }

        public bool Enabled { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
