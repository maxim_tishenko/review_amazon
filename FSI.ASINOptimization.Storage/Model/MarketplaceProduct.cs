﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FSI.ASINOptimization.Storage.Model
{
    public class MarketplaceProduct : ICloneable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }

        public string Brand { get; set; }

        public string Color { get; set; }

        public string Size { get; set; }

        public string SKU { get; set; }

        public string ImageSrc { get; set; }

        public string ASIN { get; set; }

        public string ParentASIN { get; set; }
        public string Title { get; set; }

        public string ManualChanged { get; set; }

        public SearchFeed Feed { get; set; }
        public object Clone()
        {
            return MemberwiseClone();
        }
        public string ToExcelString(string separator)
        {
            return String.Format("{0}{8}{1}{8}{2}{8}{3}{8}{4}{8}{5}{8}{6}{8}{7}", Id, (Brand != null) ? Brand.Replace(separator, "|"):"null", (Color != null) ? Color.Replace(separator, "|"):"null", (Size != null) ? Size.Replace(separator, "|"):"null", (SKU!=null)?SKU.Replace(separator, "|"):"null", (ParentASIN != null) ? ParentASIN.Replace(separator, "|") : "null",(ASIN != null) ? ASIN.Replace(separator, "|"):"null", (Title != null) ? Title.Replace(separator, "|"):"null", separator);
        }

        public static String Headers(string separator)
        {
            return String.Format("Id{0} Brand{0} Color{0} Size{0} SKU{0} ParentASIN{0} ASIN{0} Title",separator);
        }
    }
}
