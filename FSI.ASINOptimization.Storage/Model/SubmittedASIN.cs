﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FSI.ASINOptimization.Storage.Model
{
    public class SubmittedASIN
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }

        public string Value { get; set; }

        public string SKU { get; set; }

        public string FNSKU { get; set; }
    }
}
