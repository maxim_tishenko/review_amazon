﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Storage.Model
{
    public class AsinModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Key { get; set; }
        [StringLength(255)]
        public string Asin { get; set; }
    }
}
