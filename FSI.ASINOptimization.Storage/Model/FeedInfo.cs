﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FSI.ASINOptimization.Storage.Model
{
    public class FeedInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }

        public string FeedSubmissionId { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public DateTime Date { get; set; }
        
        public ICollection<SubmittedASIN> ASINs { get; set; } 

        public SearchFeed LocalFeed { get; set; }

        public FeedInfo()
        {
            ASINs = new List<SubmittedASIN>();
        }
    }
}
