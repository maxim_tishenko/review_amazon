﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FSI.ASINOptimization.Storage.Model
{
    public class SearchFeed : ICloneable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public string Status { get; set; }

        public ICollection<MarketplaceProduct> Items { get; set; }

        public ICollection<SearchQuery> SearchTerms { get; set; }

        [NotMapped]
        public bool IsRemote { get; set; }

        public SearchFeed()
        {
            Date = DateTime.Now;
            SearchTerms = new List<SearchQuery>();
            Items = new List<MarketplaceProduct>();
        }

        public object Clone()
        {
            var clone = (SearchFeed)MemberwiseClone();
            clone.SearchTerms = new List<SearchQuery>(SearchTerms);
            clone.Items = new List<MarketplaceProduct>(Items);
            return clone;
        }
    }
}
