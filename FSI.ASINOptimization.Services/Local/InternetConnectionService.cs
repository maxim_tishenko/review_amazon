﻿using FSI.ASINOptimization.Services.Amazon;
using MarketplaceWebServiceProducts;
using MarketplaceWebServiceProducts.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Local
{
    public class InternetConnectionService
    {
        MarketplaceWebServiceProductsConfig config;
        MarketplaceWebServiceProductsClient MWSClient;

        bool IsCanceled;
        public event EventHandler<string> _updateStatusEvent;
        private int attemptCount;

        public void Cancel()
        {
            IsCanceled = true;
        }
        public void Reset()
        {
            IsCanceled = false;
        }
        private InternetConnectionService()
        {
            config = new MarketplaceWebServiceProductsConfig().WithUserAgent(CredentialsProvider.UserAgent)
             .WithServiceURL("https://mws.amazonservices.com/");

            MWSClient = new MarketplaceWebServiceProductsClient(CredentialsProvider.AWSAccessKeyID, CredentialsProvider.SecretKey, config);
        }

        private static InternetConnectionService instance;

        public static InternetConnectionService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InternetConnectionService();
                }
                return instance;
            }
        }

        public bool GetServiceStatusGreen()
        {
            try
            {
                MarketplaceWebServiceProducts.Model.GetServiceStatusRequest statusRequest = new MarketplaceWebServiceProducts.Model.GetServiceStatusRequest()
                    .WithSellerId(CredentialsProvider.SellerID)
                   .WithMWSAuthToken(CredentialsProvider.MWSAuthToken);

                MarketplaceWebServiceProducts.Model.GetServiceStatusResponse result = MWSClient.GetServiceStatus(statusRequest);
                return result.GetServiceStatusResult.Status == ServiceStatusEnum.GREEN;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool waitingThread(int holdTime)
        {
            
            attemptCount = 1;
            if (_updateStatusEvent != null)
                _updateStatusEvent(this, string.Format("Connection Lost. Waiting for connecting... Attemp {0}", attemptCount));
            while (!GetServiceStatusGreen())
            {
                
                if (IsCanceled)
                {
                    SimpleLog.WriteLog("Cancel waiting for connect requested...");
                    if (_updateStatusEvent != null)
                        _updateStatusEvent(this, string.Format("Canceled...", attemptCount));
                    return false;
                }
                attemptCount++;
                SimpleLog.WriteLog("Waiting..." + attemptCount);
                Thread.Sleep(TimeSpan.FromSeconds(holdTime));
                if (_updateStatusEvent != null)
                    _updateStatusEvent(this, string.Format("Connection Lost. Waiting for connecting... Attemp {0}", attemptCount));
            }
            if (_updateStatusEvent != null)
                _updateStatusEvent(this, string.Format("Connected! Working...", attemptCount));
            return true;
        }
        public bool WaitForConnectAsync(int holdTime = 7)
        {
            CancellationTokenSource ts = new CancellationTokenSource();
            SimpleLog.WriteLog("Start waiting...");
            return waitingThread(holdTime);
            //var waitTask = Task<bool>.Factory.StartNew(() => { return waitingThread(holdTime); }, ts.Token);

            //waitTask.Wait();
        }
    }
}
