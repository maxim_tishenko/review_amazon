﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Local
{
    public class ExportFeedProcessor
    {
        private ExportFeedProcessor()
        {
            Data = null;
        }

        public event EventHandler _completed;
        private List<string> data;
        private static ExportFeedProcessor instance;

        public static ExportFeedProcessor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ExportFeedProcessor();
                }
                return instance;
            }
        }

        public List<string> Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

        public void GenerateExportFeedData(IEnumerable<RemappingDataModel> mappedData)
        {
            string s = Convert.ToString(CultureInfo.CurrentCulture.TextInfo.ListSeparator);
            Data = new List<string>();
            //write Headers
            data.Add(RemappingDataModel.Headers(s));

            foreach (var item in mappedData)
            {
                data.Add(item.ToExcelString(s));
            }
            if (_completed != null)
                _completed(this, new EventArgs());
        }
    }
}
