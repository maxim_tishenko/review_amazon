﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Local
{
    public class BusinessReportListingModel
    {
        public string MatchBrand { get; set; }
        public string Sessions { get; set; }
        public string ASIN { get; set; }
        public string PageViews { get; set; }
        public string BuyBoxPercentage { get; set; }

        public string ToExcelString(string separator)
        {
            return String.Format("{0}{4}{1}{4}{2}{4}{3}{4}", (Sessions != null) ? Sessions : "null", (ASIN != null) ? ASIN : "null", (PageViews != null) ? PageViews : "null", (BuyBoxPercentage != null) ? BuyBoxPercentage : "null", separator);
        }

        public static String Headers(string separator)
        {
            return String.Format("Sessions{0} ASIN{0} PageViews{0} BuyBoxPercentage{0}", separator);
        }
    }

    public class ProgressDataHelper
    {
        static ProgressDataHelper()
        {
            StartPercent = 0;
            CurrentPercent = 0;
            EndPercent = 0;
        }
       
        public static int StartPercent { get; set; }
        public static int CurrentPercent { get; set; }
        public static int EndPercent { get; set; }
       
        public static void Clear()
        {
            StartPercent = 0;
            CurrentPercent = 0;
            EndPercent = 0;
        }
    }

    public class BusinessReportProcessor
    {
        private BusinessReportProcessor()
        {
            data = null;
        }
        public event EventHandler _completed;
        private List<BusinessReportListingModel> data;
        private static BusinessReportProcessor instance;

        public static BusinessReportProcessor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new BusinessReportProcessor();
                }
                return instance;
            }
        }

        public List<BusinessReportListingModel> Data
        {
            get
            {
                return data;
            }
        }

        public List<BusinessReportListingModel> ProcessReportFile(string fileName, ObservableCollection<string> brandFilters)
        {
            try
            {
                data = new List<BusinessReportListingModel>();
                Dictionary<string, int> _headers = new Dictionary<string, int>();
                 
                using (TextFieldParser csvReader = new TextFieldParser(fileName))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    if (csvReader.EndOfData)
                        return null;

                    // get geaders from report
                    var _tempHeaders = csvReader.ReadFields();

                    for (int i = 0; i < _tempHeaders.Length; i++)
                    {
                        _headers.Add(_tempHeaders[i], i);
                    }

                    while (!csvReader.EndOfData)
                    {
                        var curLine = csvReader.ReadFields().ToList();
                        //foreach(var brand in brandFilters)
                        //{
                        //    if (curLine[_headers["Title"]].ToLower().Contains(brand.ToLower()))
                        //    {
                                data.Add(new BusinessReportListingModel()
                                {
                                    //MatchBrand = brand,
                                    ASIN = curLine[_headers["(Child) ASIN"]],
                                    BuyBoxPercentage = curLine[_headers["Buy Box Percentage"]],
                                    PageViews = curLine[_headers["Page Views"]],
                                    Sessions = curLine[_headers["Sessions"]]
                                });
                        //    }
                        //}
                    }
                }

                if (_completed != null)
                    _completed(this, new EventArgs());
                return data;
            }
            catch(Exception ex)
            {
                using(StreamWriter sw = new StreamWriter(Environment.TickCount.ToString() + "ErrorBusiness.txt"))
                {
                    sw.WriteLine("Error!");
                    sw.WriteLine(ex.InnerException.Message);
                    sw.WriteLine(ex.Message);
                    sw.WriteLine(ex.StackTrace);
                }
            }
            return null;
            
        }
    }
}
