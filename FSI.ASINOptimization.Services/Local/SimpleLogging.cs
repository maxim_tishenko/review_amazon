﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Local
{
    public class SimpleLog
    {
        static string filename = "log_" + new Random(Environment.TickCount).Next(100).ToString() + ".txt";
        public static void WriteLog(string line)
        {
            using (StreamWriter sw = new StreamWriter(filename,true))
            {
                sw.WriteLine(line);
            }

        }
    }
}
