﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Local
{
    public class MerchantListingModel
    {
        public string ItemName { get; set; }
        public string Brand { get; set; }
        public string SKU { get; set; }
        public string ASIN { get; set; }
        public string Quantity { get; set; }
        public string ToExcelString(string separator)
        {
            return String.Format("{0}{4}{1}{4}{2}{4}{3}{4}", (Brand != null) ? Brand : "null", (SKU != null) ? SKU : "null", (ASIN != null) ? ASIN : "null", (Quantity != null) ? Quantity : "null", separator);
        }

        public static String Headers(string separator)
        {
            return String.Format("Brand{0} SKU{0} ASIN{0} Quantity{0}", separator);
        }
    }
    public class InventoryReportProcessor
    {
        private InventoryReportProcessor()
        {
            data = null;
        }

        public event EventHandler Complete;
        private List<MerchantListingModel> data;
        private static InventoryReportProcessor instance;

        public static InventoryReportProcessor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new InventoryReportProcessor();
                }
                return instance;
            }
        }

        public List<MerchantListingModel> Data
        {
            get
            {
                return data;
            }
        }

        public List<MerchantListingModel> ProcessReportFile(string fileName, ObservableCollection<string> skuFilters)
        {
            data = new List<MerchantListingModel>();
            Dictionary<string, int> _headers = new Dictionary<string, int>();
                using (StreamReader sr = new StreamReader(fileName))
                {

                    if (sr.EndOfStream)
                        return null;

                    // get geaders from report
                    var _tempHeaders = sr.ReadLine().Split('\t').ToList();

                    for (int i = 0; i < _tempHeaders.Count; i++)
                    {
                        _headers.Add(_tempHeaders[i], i);
                    }
                try
                {
                    while (!sr.EndOfStream)
                    {
                        var curLine = sr.ReadLine().Split('\t').ToList();
                        foreach (var sku in skuFilters)
                        {
                            if (curLine[_headers["seller-sku"]].Contains(sku))
                            {
                                data.Add(new MerchantListingModel()
                                {
                                    ItemName = curLine[_headers["item-name"]],
                                    Brand = curLine[_headers["item-name"]].Split(' ')[0],
                                    ASIN = curLine[_headers["asin1"]],
                                    SKU = curLine[_headers["seller-sku"]],
                                    Quantity = curLine[_headers["quantity"]]
                                });
                            }
                        }

                    }
                }catch(Exception ex)
                {
                    var data = ex.Message;
                }
                }

            if (Complete != null)
            {
                Complete(this, new EventArgs());
            }
            return data;
        }
    }


}
