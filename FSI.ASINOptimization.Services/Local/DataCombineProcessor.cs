﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Local
{
    public class RemappingDataModel
    {
        public string ItemName { get; set; }
        public int Index { get; set; }
        public string Brand { get; set; }
        public string SKU { get; set; }
        public string ASIN { get; set; }
        public string Quantity { get; set; }
        public string Sessions { get; set; }
        public string PageViews { get; set; }
        public string LandedPrice { get; set; }

        public string Info
        {
            get
            {
                return this.ToString();
            }
        }
        public override string ToString()
        {
            return String.Format("ASIN:{0} Sessions:{1} PageViews:{2} LandedPrice:{3}", ASIN, Sessions, PageViews, LandedPrice);
        }
        public string ToExcelString(string separator)
        {
            return String.Format("{0}{7}{1}{7}{2}{7}{3}{7}{4}{7}{5}{7}{6}",
                (ItemName != null) ? ItemName : "null",
                (SKU != null) ? SKU : "null",
                (ASIN != null) ? ASIN : "null",
                (Quantity != null) ? Quantity : "null",
                (Sessions != null) ? Sessions : "null",
                (PageViews != null) ? PageViews : "null", 
                (LandedPrice != null) ? LandedPrice : "null", 
                separator);
        }

        public static String Headers(string separator)
        {
            return String.Format("Title{0} SKU{0} ASIN{0} Quantity{0} Sessions{0} PageViews{0} BuyBoxPerice(LandedPrice){0}", separator);
        }
    }

    public class DataCombineProcessor
    {
        private DataCombineProcessor()
        {
            data = new List<RemappingDataModel>();
        }

        public event EventHandler _completed;
        private List<RemappingDataModel> data;
        private static DataCombineProcessor instance;

        public static DataCombineProcessor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataCombineProcessor();
                }
                return instance;
            }
        }

        public List<RemappingDataModel> Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

        public void CombineReportsData(IEnumerable<MerchantListingModel> inventoryData, IEnumerable<BusinessReportListingModel> businessData, List<LandedPriceItemModel> landedPriceData)
        {
            string s = Convert.ToString(CultureInfo.CurrentCulture.TextInfo.ListSeparator);
            data = new List<RemappingDataModel>();
            // write Headers
            int index = 0;
            foreach (var item in businessData)
            {
                var match = inventoryData.FirstOrDefault(x => x.ASIN == item.ASIN);
                var matchPrice = landedPriceData.FirstOrDefault(x => x.ASIN == item.ASIN);
                if (match != null)
                {
                    data.Add(new RemappingDataModel() {
                        ItemName = match.ItemName,
                        Brand = match.Brand,
                        ASIN = item.ASIN,
                        PageViews = item.PageViews,
                        Quantity = match.Quantity,
                        Sessions = item.Sessions,
                        SKU = match.SKU,
                        Index = index,
                        LandedPrice = matchPrice.LandedPrice
                        //BuyBoxPercentage = "10"
                    }
                    );
                    index++;
                }
            }
            if (_completed != null)
                _completed(this, new EventArgs());
        }
    }
}
