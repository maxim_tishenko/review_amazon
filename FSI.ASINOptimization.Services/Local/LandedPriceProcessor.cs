﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Local
{
    public class LoadedLandedPricePercentEventArgs : EventArgs
    {
        public int ItemsCount { get; set; }
        public int CurrentItem { get; set; }
    }

    public class LandedPriceItemModel
    {
        public string ASIN { get; set; }
        public string LandedPrice { get; set; }
        public string ToExcelString(string separator)
        {
            return String.Format("{0}{3}{1}{3}", (ASIN != null) ? ASIN : "null", (LandedPrice != null) ? LandedPrice : "null", separator);
        }

        public static String Headers(string separator)
        {
            return String.Format("ASIN{0} LandedPrice{0}", separator);
        }
    }
    public class LandedPriceProcessor
    {
        private LandedPriceProcessor()
        {
            data = new List<LandedPriceItemModel>();
        }
        public static event EventHandler<LoadedLandedPricePercentEventArgs> itemLoaded;
        private static int progressCurrentItem = 0;
        private static int progressItemsCount = 0;
        private static bool limitReached = false;

        public static void ResetProgress()
        {
            progressCurrentItem = 0;
            progressItemsCount = 0;
        }
        public static void SetProgressItemsCount(int count)
        {
            progressItemsCount = count;
        }

        public static event EventHandler complete;
        private List<LandedPriceItemModel> data;
        private static LandedPriceProcessor instance;

        public void Complete()
        {
            if (complete != null)
                complete(this, new EventArgs());
        }
        public static LandedPriceProcessor Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LandedPriceProcessor();
                }
                return instance;
            }
        }

        public static bool LimitReached
        {
            get
            {
                return limitReached;
            }

            set
            {
                limitReached = value;
            }
        }

        public List<LandedPriceItemModel> GetItems()
        {
            return data;
        }
        public void Clear()
        {
            data.Clear();
        }

        public void AddLandedPriceItem(string asin,string landedPrice)
        {
            data.Add(
                new LandedPriceItemModel()
                {
                    ASIN = asin,
                    LandedPrice = landedPrice
                });
            progressCurrentItem++;
            if (itemLoaded!=null)
                itemLoaded(this, new LoadedLandedPricePercentEventArgs()
                {
                    CurrentItem = progressCurrentItem,
                    ItemsCount = progressItemsCount
                });
        }
    }
}
