﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using MWSClientCsRuntime;

namespace FSI.ASINOptimization.Services.Amazon
{
    public class CustomMwsAQCall : MWSClientCsRuntime.IMwsCall

    {

        private readonly CustomMwsConnection connection;

        private readonly IDictionary<string, string> parameters = new SortedDictionary<string, string>(StringComparer.Ordinal);

        private readonly IDictionary<string, string> headers = new SortedDictionary<string, string>(StringComparer.Ordinal);

        private readonly CustomMwsConnection.ServiceEndpoint serviceEndPoint;

        private Stream StreamForRequestBody;

        private string DataForRequestBody;

        private Encoding EncodingForRequestBody;

        private readonly string operationName;



        public CustomMwsAQCall(CustomMwsConnection connection, CustomMwsConnection.ServiceEndpoint serviceEndpoint, string operationName)

        {

            this.connection = connection;

            this.serviceEndPoint = serviceEndpoint;

            this.operationName = operationName;

        }



        /// <summary>

        /// Creates a request and invokes it 

        /// </summary>

        /// <returns></returns>

        /// <exception cref="MwsException">Exceptions from invoking the request</exception>

        public string invoke()

        {

            var sellerId = this.GetSellerId();

            this.AddRequiredParameters();

            var queryString = this.GetParametersAsString(this.parameters);



            string responseBody = "";

            HttpStatusCode statusCode;

            try

            {

                string requestBody;

                HttpWebRequest request;

                if (this.StreamForRequestBody != null)

                {

                    request = this.connection.GetHttpClient(this.serviceEndPoint.URI, queryString, this.headers, CalculateContentMD5(this.StreamForRequestBody));

                    this.StreamForRequestBody.Position = 0;

                    using (var streamReader = new StreamReader(this.StreamForRequestBody))

                    {

                        requestBody = streamReader.ReadToEnd();

                        this.WriteToRequestBody(request, this.StreamForRequestBody);

                        this.StreamForRequestBody.Close();

                    }

                }

                else if (!string.IsNullOrEmpty(this.DataForRequestBody) && this.EncodingForRequestBody != null)

                {

                    var bites = this.EncodingForRequestBody.GetBytes(this.DataForRequestBody);

                    request = this.connection.GetHttpClient(this.serviceEndPoint.URI, queryString, this.headers, CalculateContentMD5(bites));

                    requestBody = this.DataForRequestBody;

                    this.WriteToRequestBody(request, bites);

                }

                else

                {

                    request = this.connection.GetHttpClient(this.serviceEndPoint.URI, this.headers);

                    requestBody = queryString;

                    this.WriteToRequestBody(request, queryString);

                }


                string message;

                using (var httpResponse = (HttpWebResponse)request.GetResponse())

                {

                    statusCode = httpResponse.StatusCode;

                    message = httpResponse.StatusDescription;

                    using (var responseStream = httpResponse.GetResponseStream())

                    using (var reader = new StreamReader(responseStream, Encoding.UTF8))

                        responseBody = reader.ReadToEnd();

                }

            }

            catch (WebException we) // Web exception is thrown on unsuccessful responses

            {

                using (var httpErrorResponse = we.Response as HttpWebResponse)

                {

                    statusCode = httpErrorResponse.StatusCode;

                    using (var responseStream = httpErrorResponse.GetResponseStream())

                    using (var reader = new StreamReader(responseStream, Encoding.UTF8))

                    {

                        responseBody = reader.ReadToEnd();


                    }

                }



            }

            catch (Exception e) // Catch other exceptions, attempt to convert to formatted exception, else rethrow wrapped exception 

            {
                string str = e.Message;
            }

            return responseBody;
        }



        private void WriteToRequestBody(HttpWebRequest request, string requestData)

        {

            var bytes = new UTF8Encoding().GetBytes(requestData);

            this.WriteToRequestBody(request, bytes);

        }



        private void WriteToRequestBody(HttpWebRequest request, byte[] requestData)

        {

            request.ContentLength = requestData.Length;

            using (var requestStream = request.GetRequestStream())

                requestStream.Write(requestData, 0, requestData.Length);

        }



        private void WriteToRequestBody(HttpWebRequest request, Stream requestData)

        {

            using (var requestStream = request.GetRequestStream())

            {

                requestData.Position = 0;

                this.CopyStream(requestData, requestStream);

            }

        }

        private static T TryGetResponseHeaderForClass<T>(HttpWebResponse httpResponse, string headerName, Func<string, T> convertFunc) where T : class

        {

            try

            {

                var headerValue = httpResponse.GetResponseHeader(headerName);

                return convertFunc(headerValue);

            }

            catch (Exception)

            {

                return null;

            }

        }



        private static T? TryGetResponseHeaderForStruct<T>(HttpWebResponse httpResponse, string headerName, Func<string, T> convertFunc) where T : struct

        {

            try

            {

                var headerValue = httpResponse.GetResponseHeader(headerName);

                return convertFunc(headerValue);

            }

            catch (Exception)

            {

                return null;

            }

        }



        /// <summary>

        /// Constructs the parameters as string 

        /// </summary>

        /// <param name="parameters"></param>

        /// <returns></returns>

        private string GetParametersAsString(IDictionary<string, string> parameters)

        {

            if (parameters.Count == 0)

                return string.Empty;

            var data = new StringBuilder();

            foreach (var key in parameters.Keys)

            {

                var value = parameters[key];

                if (value != null)

                {

                    data.Append(key);

                    data.Append('=');

                    data.Append(MWSClientCsRuntime.MwsUtil.UrlEncode(value, false));

                    data.Append('&');

                }

            }

            string result = data.ToString();

            return result.Remove(result.Length - 1);

        }



        /// <summary>

        /// Add authentication related and version parameters

        /// </summary>

        private void AddRequiredParameters()

        {

            this.parameters.Add("AWSAccessKeyId", this.connection.AwsAccessKeyId);

            this.parameters.Add("Action", this.operationName);

            this.parameters.Add("Timestamp", MWSClientCsRuntime.MwsUtil.GetFormattedTimestamp());

            var version = this.serviceEndPoint.version ?? this.connection.ServiceVersion;

            if (!string.IsNullOrEmpty(version))

                this.parameters.Add("Version", version);

            string signature = MWSClientCsRuntime.MwsUtil.SignParameters(this.serviceEndPoint.URI, this.connection.SignatureVersion, this.connection.SignatureMethod, this.parameters, this.connection.AwsSecretKeyId);

            this.parameters.Add("Signature", signature);

        }



        private string GetSellerId()

        {

            var sellerId = this.parameters.FirstOrDefault(x => x.Key.Equals("SellerId", StringComparison.InvariantCultureIgnoreCase));

            return sellerId.Value ?? string.Empty;

        }


        /** The parameter prefix */

        private readonly StringBuilder parameterPrefix = new StringBuilder();

        private readonly StringBuilder headerPrefix = new StringBuilder();




        public void WriteRequestBody(Stream bodyStream)

        {

            this.StreamForRequestBody = bodyStream;

        }



        public void WriteRequestBody(string bodyData, Encoding encoding)

        {

            this.DataForRequestBody = bodyData;

            this.EncodingForRequestBody = encoding;

        }



        public void WriteValue(object value)

        {

            throw new NotSupportedException("WriteValue not supported");

        }



        public void BeginObject(string name)

        {

            throw new NotSupportedException("Complex object writing not supported");

        }



        public void EndObject(string name)

        {

            throw new NotSupportedException("Complex object writing not supported");

        }



        public static string CalculateContentMD5(Stream content)

        {

            var provider = new MD5CryptoServiceProvider();

            var hash = provider.ComputeHash(content);

            return Convert.ToBase64String(hash);

        }



        public static string CalculateContentMD5(byte[] content)

        {

            var provider = new MD5CryptoServiceProvider();

            var hash = provider.ComputeHash(content);

            return Convert.ToBase64String(hash);

        }



        private void CopyStream(Stream from, Stream to)

        {

            if (!from.CanRead)

                throw new ArgumentException("from Stream must implement the Read method.");



            if (!to.CanWrite)

                throw new ArgumentException("to Stream must implement the Write method.");



            const int SIZE = 1024 * 1024;

            var buffer = new byte[SIZE];



            var read = 0;

            while ((read = from.Read(buffer, 0, buffer.Length)) > 0)

                to.Write(buffer, 0, read);

        }



        public void Close()

        {

            //nothing to do

        }



        #region IDisposable Members

        public void Dispose()

        {

            this.Dispose(true);

            GC.SuppressFinalize(this);

        }



        protected virtual void Dispose(bool disposing)

        {

            if (this._disposed)

                return;



            if (disposing)

                this.Close();



            this._disposed = true;

        }

        public void close()
        {
            throw new NotImplementedException();
        }

        IMwsReader IMwsCall.invoke()
        {
            throw new NotImplementedException();
        }

        public MwsResponseHeaderMetadata GetResponseMetadataHeader()
        {
            throw new NotImplementedException();
        }

        private void PutValue(IDictionary<string, string> dictionary, object value, StringBuilder prefix)

        {

            if (value == null)

                return;

            if (value is IMwsObject)

            {

                prefix.Append('.');

                (value as IMwsObject).WriteFragmentTo(this);

                return;

            }

            var name = prefix.ToString();

            if (value is DateTime)

            {

                dictionary.Add(name, MwsUtil.GetFormattedTimestamp((DateTime)value));

                return;

            }

            var valueStr = value.ToString();

            if (string.IsNullOrEmpty(valueStr))

                return;

            if (value is bool)

                valueStr = valueStr.ToLower();

            dictionary.Add(name, valueStr);

        }

        public void Write(string name, object value)

        {

            int holdParameterPrefixLen = this.parameterPrefix.Length;

            this.parameterPrefix.Append(name);

            this.PutValue(this.parameters, value, this.parameterPrefix);

            this.parameterPrefix.Length = holdParameterPrefixLen;

        }

        public void WriteAttribute(string name, object value)
        {
            throw new NotImplementedException();
        }

        public void WriteList<T>(string name, ICollection<T> list)
        {
            throw new NotImplementedException();
        }

        public void WriteList<T>(string name, string memberName, ICollection<T> list)
        {
            throw new NotImplementedException();
        }

        public void Write(string namespc, string name, IMwsObject mwsObject)
        {
            throw new NotImplementedException();
        }

        public void WriteAny(ICollection<XmlElement> elements)
        {
            throw new NotImplementedException();
        }

        ~CustomMwsAQCall()

        {

            this.Dispose(false);

        }



        private bool _disposed;

        #endregion IDisposable Members

    }

    public class CustomMwsConnection : ICloneable

    {

        private const string DEFAULT_SERVICE_PATH = "";



        #region Fields and Constructors

        private readonly object lockThis = new object();



        private string applicationName;

        private string applicationVersion;

        private string libraryVersion;

        private string serviceVersion;

        private string userAgent;

        private Dictionary<string, string> headers;



        private string awsAccessKeyId;

        private string awsSecretKeyId;

        private string signatureVersion;

        private string signatureMethod;



        private int connectionTimeout;

        private Uri endpoint;



        private string proxyHost;

        private int proxyPort;

        private string proxyUsername;

        private string proxyPassword;



        private volatile bool frozen;

        private Dictionary<string, ServiceEndpoint> cachedServiceMap;



        public CustomMwsConnection()

        {

            this.cachedServiceMap = new Dictionary<string, ServiceEndpoint>();

            this.headers = new Dictionary<string, string>();



            this.frozen = false;

            this.signatureVersion = "2";

            this.signatureMethod = "HmacSHA256";

            this.connectionTimeout = 50000;

            this.libraryVersion = "1.0.0";

            this.serviceVersion = string.Empty;

        }



        public CustomMwsConnection(Uri endpoint, string applicationName, string applicationVersion, string awsAccessKeyId, string awsSecretKeyId) : this()

        {
            this.endpoint = endpoint;

            this.applicationName = applicationName;

            this.applicationVersion = applicationVersion;

            this.awsAccessKeyId = awsAccessKeyId;

            this.awsSecretKeyId = awsSecretKeyId;

        }

        #endregion



        private void freeze()

        {

            lock (this.lockThis)

            {

                if (this.UserAgent == null)

                    this.SetDefaultUserAgent();

                this.headers = new Dictionary<string, string>(this.headers);



                this.frozen = true;

            }

        }



        internal HttpWebRequest GetHttpClient(Uri uri, string queryParameters, IDictionary<string, string> additionalHeaders, string contentMd5)
        {

            uri = new Uri(uri.AbsoluteUri + "?" + queryParameters);

            additionalHeaders.Add("Content-MD5", contentMd5);

            return this.GetHttpClient(uri, additionalHeaders, "text/xml; charset=iso-8859-1");

        }



        internal HttpWebRequest GetHttpClient(Uri uri, IDictionary<string, string> additionalHeaders, string contentType = "application/x-www-form-urlencoded; charset=utf-8")

        {

            if (this.frozen)

            {

                HttpWebRequest request = WebRequest.Create(uri) as HttpWebRequest;

                if (this.ProxyHost != null && this.ProxyPort != 0)

                {

                    request.Proxy = new WebProxy(this.ProxyHost, this.ProxyPort);

                    request.Proxy.Credentials = new NetworkCredential(this.ProxyUsername, this.ProxyPassword);

                }

                request.UserAgent = this.UserAgent;

                request.Method = "POST";

                request.Timeout = this.ConnectionTimeout;

                request.ContentType = contentType;

                foreach (KeyValuePair<string, string> header in this.headers)

                {

                    request.Headers[header.Key] = header.Value;

                }

                foreach (KeyValuePair<string, string> header in additionalHeaders)

                {

                    request.Headers[header.Key] = header.Value;

                }

                return request;

            }

            else

                throw new InvalidOperationException("Must freeze properties before making HTTP requests");

        }



        /// <summary>

        /// Creates a new MwsCall

        /// </summary>

        /// <param name="operationName"></param>

        /// <returns>A new request</returns>

        public CustomMwsAQCall NewCall(string operationName, string aSIN, string itemCondition)

        {

            if (!this.frozen)

                this.freeze();



            ServiceEndpoint sep = this.GetServiceEndpoint(this.ServicePath);

            var call = new CustomMwsAQCall(this, sep, operationName);
            WriteFragmentTo(call, aSIN, itemCondition);
            return call;

        }


        public void WriteFragmentTo(IMwsWriter writer, string ASIN, string ItemCondition)

        {

            writer.Write("SellerId", CredentialsProvider.SellerID);

            writer.Write("MWSAuthToken", CredentialsProvider.MWSAuthToken);

            writer.Write("MarketplaceId", CredentialsProvider.MarketplaceID);

            writer.Write("ASIN", ASIN);

            writer.Write("ItemCondition", ItemCondition);

        }
        /// <summary>

        /// Creates a MwsCall and sends the request

        /// </summary>

        /// <typeparam name="T"></typeparam>

        /// <param name="type"></param>

        /// <param name="requestData"></param>

        /// <param name="marker"></param>

        /// <returns></returns>


        /// <summary>

        /// Clones the connection and resets the state as if it was never used

        /// </summary>

        /// <returns></returns>

        public Object Clone()

        {

            CustomMwsConnection conn = (CustomMwsConnection)base.MemberwiseClone();

            conn.cachedServiceMap = new Dictionary<string, ServiceEndpoint>();

            conn.frozen = false;

            return conn;

        }



        private void CheckUpdatable()

        {

            if (this.frozen)

                throw new InvalidOperationException("Cannot change MwsConnection properties once connected");

        }



        #region User Agent

        private void SetDefaultUserAgent()

        {

            this.SetUserAgent(

                MWSClientCsRuntime.MwsUtil.EscapeAppName(this.ApplicationName),

                MWSClientCsRuntime.MwsUtil.EscapeAppVersion(this.ApplicationVersion),

                MWSClientCsRuntime.MwsUtil.EscapeAttributeValue("C#"),

                new string[]

                {

                    "CLI", Environment.Version.ToString(),

                    "Platform", Environment.OSVersion.Platform.ToString() + "/" + Environment.OSVersion.Version,

                    "MWSClientVersion", this.LibraryVersion

                });

        }


        public void SetUserAgent(string applicationName, string applicationVersion, string programmingLanguage, params string[] additionalNameValuePairs)

        {

            lock (this.lockThis)

            {

                if (applicationName == null)

                    throw new ArgumentNullException("applicationName", "Value cannot be null.");



                if (applicationVersion == null)

                    throw new ArgumentNullException("applicationVersion", "Value cannot be null.");



                if (programmingLanguage == null)

                    throw new ArgumentNullException("programmingLanguage", "Value cannot be null.");



                if (additionalNameValuePairs.Length % 2 != 0)

                    throw new ArgumentException("additionalNameValuePairs", "Every name must have a corresponding value.");

                StringBuilder sb = new StringBuilder();

                sb.Append(applicationName);

                sb.Append("/");

                sb.Append(applicationVersion);

                sb.Append(" (");

                sb.Append("Language=");

                sb.Append(programmingLanguage);

                int i = 0;

                while (i < additionalNameValuePairs.Length)

                {

                    string name = additionalNameValuePairs[i];

                    string value = additionalNameValuePairs[++i];

                    sb.Append("; ");

                    sb.Append(MWSClientCsRuntime.MwsUtil.EscapeAttributeName(name));

                    sb.Append("=");

                    sb.Append(MWSClientCsRuntime.MwsUtil.EscapeAttributeValue(value));

                    i++;

                }



                sb.Append(")");

                this.userAgent = sb.ToString();

            }

        }



        public string ApplicationName

        {

            get { return this.applicationName; }

            set

            {

                this.CheckUpdatable();

                this.applicationName = value;

            }

        }



        public string ApplicationVersion

        {

            get { return this.applicationVersion; }

            set

            {

                this.CheckUpdatable();

                this.applicationVersion = value;

            }

        }



        /// <summary>

        /// Gets or sets the client library version, defaults to 1.0.0

        /// </summary>

        public string LibraryVersion

        {

            get { return this.libraryVersion; }

            set

            {

                this.CheckUpdatable();

                this.libraryVersion = value;

            }

        }



        public string ServiceVersion

        {

            get { return this.serviceVersion; }

            set

            {

                this.CheckUpdatable();

                this.serviceVersion = value;

            }

        }



        public string UserAgent

        {

            get { return this.userAgent; }

            set

            {

                this.CheckUpdatable();

                this.userAgent = value;

            }

        }

        #endregion



        #region Properties

        public string AwsAccessKeyId

        {

            get { return this.awsAccessKeyId; }

            set

            {

                this.CheckUpdatable();

                this.awsAccessKeyId = value;

            }

        }



        public string AwsSecretKeyId

        {

            get { return this.awsSecretKeyId; }

            set

            {

                this.CheckUpdatable();

                this.awsSecretKeyId = value;

            }

        }



        /// <summary>

        /// Get or set MWS endpoint URI

        /// </summary>

        public Uri Endpoint

        {

            get { return this.endpoint; }

            set

            {

                this.CheckUpdatable();

                this.endpoint = value;

            }

        }



        public ServiceEndpoint GetServiceEndpoint(string servicePath)

        {

            lock (this.cachedServiceMap)

            {

                if (!this.cachedServiceMap.ContainsKey(servicePath))

                {

                    ServiceEndpoint sep = new ServiceEndpoint(this.Endpoint, servicePath);

                    this.cachedServiceMap.Add(servicePath, sep);

                }

                return this.cachedServiceMap[servicePath];

            }

        }



        public string ServicePath { get; private set; }



        /// <summary>

        /// Gets and sets of the URL to base MWS calls on

        /// May include the path to make MWS calls to. Defaults to Sellers/2011-07-01

        /// </summary>

        public string ServiceURL
        {

            get { return new Uri(this.Endpoint, this.ServicePath).ToString(); }

            set

            {

                try

                {

                    var fullUri = new Uri(value);

                    this.Endpoint = new Uri(fullUri.Scheme + "://" + fullUri.Authority);



                    // Strip slashes

                    var path = fullUri.PathAndQuery;

                    if (path != null)

                        path = path.Trim(new[] { '/' });



                    if (String.IsNullOrEmpty(path))

                        this.ServicePath = DEFAULT_SERVICE_PATH;

                    else

                        this.ServicePath = path;

                }

                catch (Exception e)

                {

                    throw MWSClientCsRuntime.MwsUtil.Wrap(e);

                }

            }

        }



        /// <summary>

        /// Get or set signature version - defaults to 2

        /// </summary>

        /// <returns></returns>

        public string SignatureVersion

        {

            get { return this.signatureVersion; }

            set

            {

                this.CheckUpdatable();

                this.signatureVersion = value;

            }

        }



        /// <summary>

        /// Get or set signature method - defaults to HmacSHA256

        /// </summary>

        /// <returns></returns>

        public string SignatureMethod

        {

            get { return this.signatureMethod; }

            set

            {

                this.CheckUpdatable();

                this.signatureMethod = value;

            }

        }



        /// <summary>

        /// Get or set connection timeout, defaults to 50000

        /// </summary>

        /// <returns></returns>

        public int ConnectionTimeout

        {

            get { return this.connectionTimeout; }

            set

            {

                this.CheckUpdatable();

                this.connectionTimeout = value;

            }

        }



        public string ProxyHost

        {

            get { return this.proxyHost; }

            set

            {

                this.CheckUpdatable();

                this.proxyHost = value;

            }

        }



        public int ProxyPort

        {

            get { return this.proxyPort; }

            set

            {

                this.CheckUpdatable();

                this.proxyPort = value;

            }

        }



        public string ProxyUsername

        {

            get { return this.proxyUsername; }

            set

            {

                this.CheckUpdatable();

                this.proxyUsername = value;

            }

        }



        public string ProxyPassword

        {

            get { return this.proxyPassword; }

            set

            {

                this.CheckUpdatable();

                this.proxyPassword = value;

            }

        }



        /// <summary>

        /// Sets the value of a request header to be included on every request

        /// </summary>

        /// <param name="name">the name of the header to set</param>

        /// <param name="value">value to send with header</param>

        public void IncludeRequestHeader(string name, string value)

        {

            this.CheckUpdatable();

            this.headers[name] = value;

        }



        /// <summary>

        /// Gets the currently set value of a request header

        /// </summary>

        /// <param name="name">the name of the header to get</param>

        /// <returns>value of specified header, or null if not defined</returns>

        public string GetRequestHeader(string name)

        {

            if (this.headers.ContainsKey(name))

                return this.headers[name];

            else

                return null;

        }

        #endregion



        #region ServiceEndpoint Definition

        /// <summary>

        /// Immutable service and version URI for an endpoint

        /// </summary>

        public class ServiceEndpoint

        {

            /** The service name. */

            public readonly string service;



            /** The service and version name as service/version. */

            public readonly string servicePath;



            /** The combined uri of the connection, service name, and version. */

            public readonly Uri URI;



            /** The service version. */

            public readonly string version;



            /// <summary>

            /// Creates a new URI 

            /// </summary>

            /// <param name="baseUri"></param>

            /// <param name="servicePath"></param>

            public ServiceEndpoint(Uri baseUri, string servicePath)

            {

                if (servicePath.Length > 0)

                {

                    this.servicePath = servicePath;

                    int j = servicePath.LastIndexOf('/');

                    this.service = servicePath.Substring(0, j);

                    this.version = servicePath.Substring(j + 1);

                }

                this.URI = new Uri(baseUri, "/" + servicePath);

            }

        }

        #endregion

    }

}
