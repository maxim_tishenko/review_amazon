﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Amazon
{
    public enum ReportTypes
    {
        _GET_FLAT_FILE_OPEN_LISTINGS_DATA_=0,
        _GET_MERCHANT_LISTINGS_ALL_DATA_=1,
        _GET_MERCHANT_LISTINGS_DATA_=2,
        _GET_MERCHANT_LISTINGS_INACTIVE_DATA_=3,
        _GET_MERCHANT_LISTINGS_DATA_BACK_COMPAT_=4,
        _GET_MERCHANT_LISTINGS_DATA_LITE_=5,
        _GET_MERCHANT_LISTINGS_DATA_LITER_=6,
        _GET_MERCHANT_CANCELLED_LISTINGS_DATA_=7,
        _GET_CONVERGED_FLAT_FILE_SOLD_LISTINGS_DATA_=8,
        _GET_MERCHANT_LISTINGS_DEFECT_DATA_=9
    }
}
