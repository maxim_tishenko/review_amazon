﻿using MarketplaceWebService;
using MarketplaceWebService.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Amazon
{
    public class AmazonReportService
    {
        MarketplaceWebServiceConfig config;
        MarketplaceWebServiceClient MWSClient;
        private AmazonReportService()
        {
            config = new MarketplaceWebServiceConfig().WithUserAgent(CredentialsProvider.UserAgent)
            .WithServiceURL("https://mws.amazonservices.com/");

            MWSClient = new MarketplaceWebServiceClient(CredentialsProvider.AWSAccessKeyID, CredentialsProvider.SecretKey, config);
        }

        private static AmazonReportService instance;

        public static AmazonReportService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AmazonReportService();
                }
                return instance;
            }
        }

        private static List<string> reportTypesList = new List<string>()
        {
            "_GET_FLAT_FILE_OPEN_LISTINGS_DATA_",
            "_GET_MERCHANT_LISTINGS_ALL_DATA_",
            "_GET_MERCHANT_LISTINGS_DATA_",
            "_GET_MERCHANT_LISTINGS_INACTIVE_DATA_ ",
            "_GET_MERCHANT_LISTINGS_DATA_BACK_COMPAT_",
            "_GET_MERCHANT_LISTINGS_DATA_LITE_",
            "_GET_MERCHANT_LISTINGS_DATA_LITER_",
            "_GET_MERCHANT_CANCELLED_LISTINGS_DATA_",
            "_GET_CONVERGED_FLAT_FILE_SOLD_LISTINGS_DATA_",
            "_GET_MERCHANT_LISTINGS_DEFECT_DATA_"
        };

        public ReportRequestInfo ReportRequestRequest(int reportType)
        {
            var requestReportRequest = new MarketplaceWebService.Model.RequestReportRequest()
                .WithMerchant(CredentialsProvider.SellerID)
                .WithReportType(reportTypesList[reportType])
                .WithMWSAuthToken(CredentialsProvider.MWSAuthToken);

           var data = MWSClient.RequestReport(requestReportRequest).RequestReportResult.ReportRequestInfo;
           return data;
        }
        public List<ReportInfo> ReportsListRequest(string reportRequestId)
        {
            IdList id = new IdList();
            id.Id = new List<string>();
            id.Id.Add(reportRequestId);
            var request = new MarketplaceWebService.Model.GetReportListRequest()
                .WithMerchant(CredentialsProvider.SellerID)
                .WithMWSAuthToken(CredentialsProvider.MWSAuthToken)
                .WithReportRequestIdList(id);

            return MWSClient.GetReportList(request)
                .GetReportListResult
                .ReportInfo;
        }
        public string GetReportRequest(string reportId, int reportType = 999)
        {
            var reportName = ((reportType != 999) ? reportTypesList[reportType] : "") + "_report_" + DateTime.UtcNow.ToShortDateString().Replace('/','_')+"_"+ Environment.TickCount.ToString() + ".csv";
            var stream = new FileStream(reportName, FileMode.Create);
            
            var requestReport = new MarketplaceWebService.Model.GetReportRequest()
               .WithMerchant(CredentialsProvider.SellerID)
               .WithMWSAuthToken(CredentialsProvider.MWSAuthToken)
               .WithReportId(reportId)
               .WithReport(stream);
            var result = MWSClient.GetReport(requestReport);
            stream.Close();
            return stream.Name;
        }
    }
}
