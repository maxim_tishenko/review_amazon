﻿using FSI.ASINOptimization.Services.Amazon.AmazonProductServiceModels.ChildModels;
using FSI.ASINOptimization.Services.Amazon.AmazonProductServiceModels.ParentModels;
using FSI.ASINOptimization.Services.Amazon.AmazonProductServiceModels.BuyBoxPricesModels;
using MarketplaceWebServiceProducts;
using MarketplaceWebServiceProducts.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Net;
using System.Text;
using MWSClientCsRuntime;
using System.Security.Cryptography;
using FSI.ASINOptimization.Services.Local;
using System.Windows;

namespace FSI.ASINOptimization.Services.Amazon
{

    [XmlRoot(ElementName = "Timestamp", Namespace = "https://mws.amazonservices.com")]
    public class Timestamp
    {
        [XmlAttribute(AttributeName = "timestamp")]
        public string timestamp { get; set; }
    }

    [XmlRoot(ElementName = "PingResponse", Namespace = "https://mws.amazonservices.com")]
    public class PingResponse
    {
        [XmlElement(ElementName = "Timestamp", Namespace = "https://mws.amazonservices.com")]
        public Timestamp Timestamp { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }


    public class AmazonProductService
    {
        MarketplaceWebServiceProductsConfig config;
        MarketplaceWebServiceProductsClient MWSClient;

        private int attemptCount;
        private AmazonProductService()
        {
            config = new MarketplaceWebServiceProductsConfig().WithUserAgent(CredentialsProvider.UserAgent)
            .WithServiceURL("https://mws.amazonservices.com/");

            MWSClient = new MarketplaceWebServiceProductsClient(CredentialsProvider.AWSAccessKeyID, CredentialsProvider.SecretKey, config);
        }

        private static AmazonProductService instance;

        public static AmazonProductService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AmazonProductService();
                }
                return instance;
            }
        }

        [XmlRoot(ElementName = "MarketplaceASIN", Namespace = "http://mws.amazonservices.com/schema/Products/2011-10-01")]
        public class MarketplaceASIN
        {
            [XmlElement(ElementName = "MarketplaceId", Namespace = "http://mws.amazonservices.com/schema/Products/2011-10-01")]
            public string MarketplaceId { get; set; }
            [XmlElement(ElementName = "ASIN", Namespace = "http://mws.amazonservices.com/schema/Products/2011-10-01")]
            public string ASIN { get; set; }
        }

        [XmlRoot(ElementName = "Identifiers", Namespace = "http://mws.amazonservices.com/schema/Products/2011-10-01")]
        public class RelationShipsIdentifiers
        {
            [XmlElement(ElementName = "MarketplaceASIN", Namespace = "http://mws.amazonservices.com/schema/Products/2011-10-01")]
            public MarketplaceASIN MarketplaceASIN { get; set; }
            [XmlAttribute(AttributeName = "xmlns")]
            public string Xmlns { get; set; }
        }


        [XmlRoot(ElementName = "Identifiers")]
        public class Identifiers
        {
            [XmlElement(ElementName = "MarketplaceASIN")]
            public MarketplaceASIN MarketplaceASIN { get; set; }
        }

        [XmlRoot(ElementName = "VariationChild")]
        public class VariationChild
        {
            [XmlElement(ElementName = "Identifiers")]
            public Identifiers Identifiers { get; set; }
            [XmlElement(ElementName = "Color")]
            public string Color { get; set; }
            [XmlElement(ElementName = "Size")]
            public string Size { get; set; }
        }


        // Custom MatchingProduct class
        public class MatchingProduct
        {
            public string ASIN { get; set; }
            public string ParentASIN { get; set; }
            public List<string> ChildrenASINs { get; set; }
            public string Brand { get; set; }
            public string Color { get; set; }
            public string Size { get; set; }
            public string ImageURl { get; set; }
            public string Title { get; set; }
        }


        public bool GetServiceStatusGreen()
        {
            try
            {
                MarketplaceWebServiceProducts.Model.GetServiceStatusRequest statusRequest = new GetServiceStatusRequest()
                    .WithSellerId(CredentialsProvider.SellerID)
                   .WithMWSAuthToken(CredentialsProvider.MWSAuthToken);

                MarketplaceWebServiceProducts.Model.GetServiceStatusResponse result = MWSClient.GetServiceStatus(statusRequest);
                return result.GetServiceStatusResult.Status == ServiceStatusEnum.GREEN;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<MatchingProduct> ListMatchingProductsRequest(string query)
        {
            List<MatchingProduct> matchingProductList = new List<MatchingProduct>();

            try
            {


                var requestlistMatchingProducts = new MarketplaceWebServiceProducts.Model.ListMatchingProductsRequest()
                   .WithSellerId(CredentialsProvider.SellerID)
                   .WithMWSAuthToken(CredentialsProvider.MWSAuthToken)
                   .WithMarketplaceId(CredentialsProvider.MarketplaceID)
                   .WithQuery(query);

                SimpleLog.WriteLog("Created Request waiting...");
                MarketplaceWebServiceProducts.Model.ListMatchingProductsResponse result = MWSClient.ListMatchingProducts(requestlistMatchingProducts);
                SimpleLog.WriteLog("Response received ...");
                Dictionary<string, string> attrs = new Dictionary<string, string>();

                foreach (var product in result.ListMatchingProductsResult.Products.Product)
                {
                    if (product.Relationships.Any == null || product.Relationships.Any.Count < 1)
                        continue;

                    XmlSerializer Serializer = new XmlSerializer(typeof(RelationShipsIdentifiers));
                    XmlReader reader = XmlReader.Create(new StringReader(((XmlElement)product.Relationships.Any[0]).InnerXml));
                    RelationShipsIdentifiers parent = (RelationShipsIdentifiers)Serializer.Deserialize(reader);

                    List<XElement> elems = new List<XElement>();


                    XDocument doc = XDocument.Load(new StringReader(product.AttributeSets.ToXML()));
                    foreach (var item in (doc.Elements().ToList()[0].Elements()))
                    {
                        elems.Add(item);
                        foreach (var elem in item.Elements())
                        {
                            if (!attrs.ContainsKey(elem.Name.LocalName))
                            {
                                string value = elem.Name.LocalName == "SmallImage"
                                    ? elem.Elements().First(x => x.Name.LocalName == "URL").Value
                                    : elem.Value;
                                attrs.Add(elem.Name.LocalName, value);
                            }
                        }
                    }

                    MatchingProduct prod = new MatchingProduct()
                    {
                        ASIN = product.Identifiers.MarketplaceASIN.ASIN,
                        ParentASIN = parent.MarketplaceASIN.ASIN,
                        Brand = attrs["Brand"],
                        Color = attrs["Color"],
                        ImageURl = attrs["SmallImage"],
                        Size = attrs["Size"],
                        Title = attrs["Title"]
                    };
                    matchingProductList.Add(prod);

                }

            }
            catch (Exception ex)
            {
                SimpleLog.WriteLog("No Response, Exception throw...");
                SimpleLog.WriteLog(ex.Message);
                if(ex.InnerException != null && ex.InnerException.InnerException!=null)
                if (ex.InnerException.InnerException.Message.Contains("A connection that was expected to be kept alive was closed by the server.") ||
                    ex.InnerException.InnerException.Message.Contains("The remote name could not be resolved") ||
                    ex.InnerException.InnerException.Message.Contains("The underlying connection was closed"))
                {
                    MessageBox.Show(string.Format("Internet connection problem..."));
                    SimpleLog.WriteLog("Enter to WaitForConnectAsync...");
                    if (InternetConnectionService.Instance.WaitForConnectAsync())
                    {
                        SimpleLog.WriteLog("Connected, Resume working...");
                        return ListMatchingProductsRequest(query);
                    }
                    
                }
                SimpleLog.WriteLog("Canceled working...");
                return null;
            }
            return matchingProductList;
        }



        public class ProgressDataHelper
        {
            static ProgressDataHelper()
            {
                ParentAsinsCount = 0;
                CurrentParentAsin = 0;
                ChildrensAsinsCount = 0;
                CurrentChildren = 0;
            }
            public static int ParentAsinsCount { get; set; }
            public static int CurrentParentAsin { get; set; }
            public static int ChildrensAsinsCount { get; set; }
            public static int CurrentChildren { get; set; }
            public static int AttemptCount { get; set; }
            private static string WorkingStatus { get; set; }
            public static void Clear()
            {
                ParentAsinsCount = 0;
                CurrentParentAsin = 0;
                ChildrensAsinsCount = 0;
                CurrentChildren = 0;
            }
        }

        public List<string> GetDistinctParentProduct(IEnumerable<MatchingProduct> values)
        {
            List<string> distinctParentASINS = new List<string>();
            foreach (MatchingProduct item in values)
                if (!distinctParentASINS.Contains(item.ParentASIN))
                    distinctParentASINS.Add(item.ParentASIN);
            return distinctParentASINS;
        }



        public IEnumerable<MatchingProduct> ListMatchingProductsDistinctChildsRequest(string query)
        {
            MatchingProduct childProd = new MatchingProduct()
            {
                ASIN = "Error",
                Brand = "Error",
                ChildrenASINs = new List<string>(),
                Color = "Error",
                ImageURl = "Error",
                ParentASIN = "Error",
                Size = "Error",
                Title = "Error"
            };
            List<MatchingProduct> childProducts = new List<MatchingProduct>();
            Dictionary<string, Dictionary<string, string>> colorImageMap = new Dictionary<string, Dictionary<string, string>>();

            SimpleLog.WriteLog("ListMatchingProductsRequest(query) started...");
            var searchResult = ListMatchingProductsRequest(query);

            SimpleLog.WriteLog("ListMatchingProductsRequest return null...");
            if (searchResult == null)
                yield return childProd;
            SimpleLog.WriteLog("Get distinkt parent asins...");
            List<string> distinctParentASINS = GetDistinctParentProduct(searchResult);

            int tempCounter = 0;
            ProgressDataHelper.ParentAsinsCount = distinctParentASINS.Count;

            
            
            KeyValuePair<string, Dictionary<string, string>> result = new KeyValuePair<string, Dictionary<string, string>>(null, null);
            SimpleLog.WriteLog("Enter to the image scrapper foreach...");
            // get images
            foreach (var asin in distinctParentASINS)
            {
                try
                {

                    result = AmazonImageScrapper.Instance.GetColorImageMapping(asin);
                    SimpleLog.WriteLog("Result imag scrapper received "+ result.Value.Count+ "... GetColorImageMapping" + asin);
                    if (result.Key == null && result.Value == null)
                    {
                        break;
                    }
                    colorImageMap.Add(result.Key, result.Value);
                }
                catch (Exception)
                {
                    colorImageMap.Add(asin, new Dictionary<string, string>());
                }
                if (result.Key == null && result.Value == null)
                {
                    yield return childProd;
                }
                
            }
            ParentProduct prod = new ParentProduct();
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            foreach (var parentAsin in distinctParentASINS)
            {
                ChildProduct data = GetChildMatchingProductRequest(parentAsin);
                if (data == null)
                {
                    yield return childProd;
                }
                ProgressDataHelper.CurrentParentAsin++;
                ProgressDataHelper.ChildrensAsinsCount = data.Relationships.VariationChild.Count;
                foreach (var item in data.Relationships.VariationChild)
                {
                    //ProgressDataHelper.CurrentChildren++;
                    //if (tempCounter > 20)
                    //    continue;
                     
                    if (sw.ElapsedMilliseconds < 500)
                    {
                        Thread.Sleep(500 - Convert.ToInt32(sw.ElapsedMilliseconds));
                    }
                    try
                    {
                        sw.Start();
                        prod = GetParentMatchingProductRequest(item.Identifiers.MarketplaceASIN.ASIN);

                        childProd.ParentASIN = parentAsin;
                        childProd.ASIN = prod.Identifiers.MarketplaceASIN.ASIN;
                        childProd.Brand = prod.AttributeSets.ItemAttributes.Brand;
                        childProd.Color = prod.AttributeSets.ItemAttributes.Color;
                        try
                        {
                            childProd.ImageURl = colorImageMap[parentAsin][prod.AttributeSets.ItemAttributes.Color];
                        }
                        catch
                        {
                            continue;
                        }
                        childProd.Size = prod.AttributeSets.ItemAttributes.Size;
                        childProd.Title = prod.AttributeSets.ItemAttributes.Title;
                        childProd.ParentASIN = prod.Relationships.VariationParent.Identifiers.MarketplaceASIN.ASIN;

                    }
                    catch (Exception ex)
                    {
                        tempCounter--;
                    }
                    tempCounter++;
                    sw.Stop();

                    yield return childProd;
                }
                ProgressDataHelper.CurrentChildren = 0;
                ProgressDataHelper.ChildrensAsinsCount = 0;
                tempCounter = 0;
                //}
            }
            ProgressDataHelper.CurrentParentAsin = 0;
            ProgressDataHelper.ParentAsinsCount = 0;
        }
        /*

1) ListMatchingProduct until there are 40 parent asins.
2) GetMatchingProduct for each parent and extract image and style
3) GetMatchingProduct for each parent children, set parent style and image for it and extract child size and color
4) Build table from this data*/

        public ParentProduct GetParentMatchingProductRequest(string childAsin)
        {
            string childAsin1 = childAsin;
            GetMatchingProductResult result;
            XmlElement doc;
            GetParentMatchingProductResponse document;
            XmlReader reader;
            XmlSerializer Serializer;
            try
            {
                MarketplaceWebServiceProducts.Model.ASINListType singleASIN = new MarketplaceWebServiceProducts.Model.ASINListType();
                singleASIN.ASIN.Add(childAsin);
                var getMatchingProductRequest = new MarketplaceWebServiceProducts.Model.GetMatchingProductRequest()
                    .WithASINList(singleASIN)
                    .WithMarketplaceId(CredentialsProvider.MarketplaceID)
                    .WithMWSAuthToken(CredentialsProvider.MWSAuthToken)
                    .WithSellerId(CredentialsProvider.SellerID);

                result = MWSClient.GetMatchingProduct(getMatchingProductRequest).GetMatchingProductResult[0];
                doc = (XmlElement)result.Product.AttributeSets.Any[0];
                Serializer = new XmlSerializer(typeof(GetParentMatchingProductResponse));
                reader = XmlReader.Create(new StringReader(doc.OwnerDocument.OuterXml));
                document = (GetParentMatchingProductResponse)Serializer.Deserialize(reader);
                return document.GetMatchingProductResult.Product;
            }
            catch (Exception ex)
            {               
                SimpleLog.WriteLog("No Response, Exception throw...");
                SimpleLog.WriteLog(ex.Message);
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                    if (ex.InnerException.InnerException.Message.Contains("A connection that was expected to be kept alive was closed by the server.") ||
                        ex.InnerException.InnerException.Message.Contains("The remote name could not be resolved") ||
                        ex.InnerException.InnerException.Message.Contains("The underlying connection was closed"))
                    {
                        MessageBox.Show(string.Format("Internet connection problem..."));
                        if (InternetConnectionService.Instance.WaitForConnectAsync())
                        return GetParentMatchingProductRequest(childAsin);
                }
                var message = ex.Message + "   -----    " + childAsin1;
                return null;
            }

        }
        public ChildProduct GetChildMatchingProductRequest(string parentAsin)
        {
            GetChildMatchingProductResponse document = null;
            try
            {
                MatchingProduct matchingProduct = new MatchingProduct();
                XmlSerializer serializer = new XmlSerializer(typeof(RelationShipsIdentifiers));
                MarketplaceWebServiceProducts.Model.ASINListType singleASIN = new MarketplaceWebServiceProducts.Model.ASINListType();
                singleASIN.ASIN.Add(parentAsin);
                var getMatchingProductRequest = new MarketplaceWebServiceProducts.Model.GetMatchingProductRequest()
                    .WithASINList(singleASIN)
                    .WithMarketplaceId(CredentialsProvider.MarketplaceID)
                    .WithMWSAuthToken(CredentialsProvider.MWSAuthToken)
                    .WithSellerId(CredentialsProvider.SellerID);


                var result = MWSClient.GetMatchingProduct(getMatchingProductRequest).GetMatchingProductResult[0];
                var doc = (XmlElement)result.Product.AttributeSets.Any[0];

                XmlSerializer Serializer = new XmlSerializer(typeof(GetChildMatchingProductResponse));
                XmlReader reader = XmlReader.Create(new StringReader(doc.OwnerDocument.OuterXml));
                document = (GetChildMatchingProductResponse)Serializer.Deserialize(reader);
                return document.GetMatchingProductResult.Product;
            }
            catch (Exception ex)
            {
                
                SimpleLog.WriteLog("No Response, Exception throw...");
                SimpleLog.WriteLog(ex.Message);
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                    if (ex.InnerException.InnerException.Message.Contains("A connection that was expected to be kept alive was closed by the server.") ||
                        ex.InnerException.InnerException.Message.Contains("The remote name could not be resolved") ||
                        ex.InnerException.InnerException.Message.Contains("The underlying connection was closed"))
                    {
                        MessageBox.Show(string.Format("Internet connection problem..."));
                        if (InternetConnectionService.Instance.WaitForConnectAsync())
                        return GetChildMatchingProductRequest(parentAsin);
                }
                var message = ex.Message + "   -----    " + parentAsin;
                return null;
            }
            
        }

        public static bool quotaReachedMessage = false;
        public LandedPrice GetLowestPricedOffersForASIN(string ASIN, string itemCondition)
        {
            CustomMwsConnection conn = new CustomMwsConnection(new Uri("https://mws.amazonservices.com/Products/2011-10-01"),
                 "MyAmazonTool", "1.0.0", CredentialsProvider.AWSAccessKeyID, CredentialsProvider.SecretKey);
            conn.ServiceURL = "https://mws.amazonservices.com/Products/2011-10-01";
            string data = "";

            data = conn.NewCall("GetLowestPricedOffersForASIN", ASIN, itemCondition).invoke();
            if(data.Contains("You exceeded your quota"))
            {
                if (!quotaReachedMessage)
                {
                    quotaReachedMessage = true;
                }
                return null;
            }
            
            XmlSerializer Serializer1 = null;
            XmlReader reader2 = null;
            FSI.ASINOptimization.Services.Amazon.AmazonProductServiceModels.BuyBoxPricesModels.GetLowestPricedOffersForASINResponse parent2 = null;
            try
            {
                Serializer1 = new XmlSerializer(typeof(FSI.ASINOptimization.Services.Amazon.AmazonProductServiceModels.BuyBoxPricesModels.GetLowestPricedOffersForASINResponse));
                reader2 = XmlReader.Create(new StringReader(data));
                parent2 = (FSI.ASINOptimization.Services.Amazon.AmazonProductServiceModels.BuyBoxPricesModels.GetLowestPricedOffersForASINResponse)Serializer1.Deserialize(reader2);
                if (parent2.GetLowestPricedOffersForASINResult.Offers.Offer.Count == 0)
                    return null;
                if (parent2.GetLowestPricedOffersForASINResult.Summary.BuyBoxPrices == null)
                    return null;
                var result = parent2.GetLowestPricedOffersForASINResult.Summary.BuyBoxPrices.BuyBoxPrice;
                return result
                    .OrderByDescending(x => x.LandedPrice)
                    .FirstOrDefault(x => x.Condition == itemCondition)
                    .LandedPrice;
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                //SimpleLog.WriteLog("GetLowestPricedOffersForASIN, No Response, Exception throw...");
                //if (ex.InnerException != null)
                //{
                    //SimpleLog.WriteLog(ex.InnerException.Message);
                //}
                //SimpleLog.WriteLog(ex.Message);
                //SimpleLog.WriteLog("GetLowestPricedOffersForASIN,exit exception...");
            }
            return null;

        }
        private static string CalculateStringToSignV2(Uri serviceUri,

            IDictionary<string, string> parameters)

        {

            StringBuilder data = new StringBuilder();

            data.Append("POST");

            data.Append("\r\n");

            data.Append(serviceUri.Host.ToLower());

            data.Append("\r\n");

            string uri = serviceUri.LocalPath;

            data.Append(MwsUtil.UrlEncode(uri, true));

            data.Append("\r\n");

            //IDictionary<string, string> sorted = new SortedDictionary<string, string>(parameters, StringComparer.Ordinal);

            IEnumerator<KeyValuePair<string, string>> pairs = parameters.GetEnumerator();

            bool isFirst = true;

            while (pairs.MoveNext())

            {

                if (isFirst)

                    isFirst = false;

                else

                    data.Append("&");

                KeyValuePair<string, string> pair = pairs.Current;

                string key = pair.Key;

                data.Append(MwsUtil.UrlEncode(key, false));

                data.Append("=");

                string value = pair.Value;

                data.Append(MwsUtil.UrlEncode(value, false));

            }

            return data.ToString();

        }

        public static string SignParameters(Uri serviceUri,

            string signatureVersion, string signatureMethod,

            IDictionary<string, string> parameters, string awsSecretKey)

        {

            parameters.Add("SignatureVersion", signatureVersion);

            string algorithm = "HmacSHA1";

            string stringToSign = null;


            algorithm = signatureMethod;

            parameters.Add("SignatureMethod", algorithm);

            stringToSign = CalculateStringToSignV2(serviceUri, parameters);


            return MwsUtil.Sign(stringToSign, awsSecretKey, algorithm);

        }
    }




}
