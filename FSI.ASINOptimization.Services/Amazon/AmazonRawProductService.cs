﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using HtmlAgilityPack;

namespace FSI.ASINOptimization.Services.Amazon
{
    public static class AmazonRawProductService
    {
        private const string SEARCH_URL = "https://www.amazon.com/s/?field-keywords={0}";

        static AmazonRawProductService()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                (send, certificate, chain, sslPolicyErrors) => { return true; };
        }

        public static List<string> QueryProductsParentASINs(string query)
        {
            var parentAsins = new List<string>();
            query = query.Replace(' ', '+');
            string fullUrl = String.Format(SEARCH_URL, query);
            var downloader = new WebClient();
            downloader.Headers.Add(HttpRequestHeader.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            downloader.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");
            downloader.Headers.Add(HttpRequestHeader.Host, "www.amazon.com");
            try
            {
                string result = downloader.DownloadString(new Uri(fullUrl, UriKind.Absolute));

                var doc = new HtmlDocument();
                doc.LoadHtml(result);



                int i = 0;
                HtmlNode currentResult;

                do
                {
                    currentResult = doc.GetElementbyId(string.Format("result_{0}", i++));
                    if (currentResult != null)
                    {
                        parentAsins.Add(currentResult.GetAttributeValue("data-asin", string.Empty));
                    }
                } while (currentResult != null && i < 40);
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Error has occured while getting parent asins from html: {0}", ex.Message);
            }

            return parentAsins;
        }
    }
}
