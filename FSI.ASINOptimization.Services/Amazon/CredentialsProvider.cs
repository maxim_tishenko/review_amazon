﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Amazon
{
    class CredentialsProvider
    {
        public static string SellerID
        {
            ...
        }
        public static string MWSAuthToken
        {
            ...
        }
        public static string AWSAccessKeyID
        {
            ...
        }
        public static string SecretKey
        {
            ...
        }
        public static string MarketplaceID
        {
            ...
        }
        public static string UserAgent
        {
            ...
        }
    }
}
