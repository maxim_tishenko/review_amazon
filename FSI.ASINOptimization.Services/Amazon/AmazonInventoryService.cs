﻿using FBAInventoryServiceMWS;
using FBAInventoryServiceMWS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Amazon
{
    public class AmazonInventoryService
    {
        FBAInventoryServiceMWS.FBAInventoryServiceMWSConfig config;
        FBAInventoryServiceMWS.FBAInventoryServiceMWSClient MWSClient;
        private AmazonInventoryService()
        {
            config = new FBAInventoryServiceMWSConfig().WithUserAgent(CredentialsProvider.UserAgent)
            .WithServiceURL("https://mws.amazonservices.com/");

            MWSClient = new FBAInventoryServiceMWSClient(CredentialsProvider.AWSAccessKeyID, CredentialsProvider.SecretKey,config);
        }

        private static AmazonInventoryService instance;

        public static AmazonInventoryService Instance
        {
            get
            {
                if (instance == null)
                {
                     instance = new AmazonInventoryService();
                }
                return instance;
            }
        }

        public List<InventorySupply> ListInventorySupplyRequest()
        {
            var requestListInventorySupply = new ListInventorySupplyRequest()
               .WithSellerId(CredentialsProvider.SellerID)
               .WithMWSAuthToken(CredentialsProvider.MWSAuthToken)
               .WithQueryStartDateTime(DateTime.Now)
               .WithSellerId(CredentialsProvider.SellerID);
            ListInventorySupplyResponse result = MWSClient.ListInventorySupply(requestListInventorySupply);

            return result.ListInventorySupplyResult.InventorySupplyList.member;
        }


    }
}
