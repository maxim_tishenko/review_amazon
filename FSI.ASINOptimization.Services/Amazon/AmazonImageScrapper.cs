﻿using FSI.ASINOptimization.Services.Local;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace FSI.ASINOptimization.Services.Amazon
{

    public class AmazonImageScrapper
    {
        public const string baseUrl = "https://www.amazon.com/dp/";
        private AmazonImageScrapper()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback +=
                 (send, certificate, chain, sslPolicyErrors) => { return true; };
        }

        private static AmazonImageScrapper instance;

        public static AmazonImageScrapper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AmazonImageScrapper();
                }
                return instance;
            }
        }

        private Dictionary<string, string> ProcessJson(string data)
        {

            string startMarker = "data[\"colorImages\"] = ";
            string endMarker = "data[\"heroImage\"]";
            //data["heroImage"]
            int start = data.IndexOf(startMarker) + startMarker.Length;
            int end = data.IndexOf(endMarker) - 5;
            string result = data.Substring(start, end - start + 1);
            Dictionary<string, string> colorsImages = new Dictionary<string, string>();
            var data12 = (JObject)JsonConvert.DeserializeObject(result);
            foreach (var item in data12)
            {
                var vaues = item.Value[0];
                string main = vaues["large"].ToString();
                colorsImages.Add(item.Key, main);
            }

            return colorsImages;
        }
        public KeyValuePair<string,Dictionary<string, string>> GetColorImageMapping(string parentASIN)
        {
            SimpleLog.WriteLog("Download images for asin start... GetColorImageMapping" + parentASIN);
            Uri fullUrl = AmazonImageScrapper.Instance.getNavigateUri(parentASIN);
            var downloader = new WebClient();
            downloader.Headers.Add(HttpRequestHeader.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            downloader.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");
            downloader.Headers.Add(HttpRequestHeader.Host, "www.amazon.com");
            try
            {
                string result = downloader.DownloadString(fullUrl);
                return new KeyValuePair<string, Dictionary<string, string>> (parentASIN,ProcessJson(result));
            }
            catch (Exception ex)
            {
                SimpleLog.WriteLog("Exception theows... GetColorImageMapping" + parentASIN);
                //Debug.WriteLine("Error has occured while getting parent asins from html: {0}", ex.Message);
                SimpleLog.WriteLog("No Response, Exception throw...");
                SimpleLog.WriteLog(ex.Message);
                if (ex.InnerException != null && ex.InnerException.InnerException != null)
                    if (ex.InnerException.InnerException.Message.Contains("A connection that was expected to be kept alive was closed by the server.") ||
                        ex.InnerException.InnerException.Message.Contains("The remote name could not be resolved") ||
                        ex.InnerException.InnerException.Message.Contains("The underlying connection was closed"))
                    {
                        MessageBox.Show(string.Format("Internet connection problem..."));
                        if (InternetConnectionService.Instance.WaitForConnectAsync())
                        {
                            return GetColorImageMapping(parentASIN);
                            SimpleLog.WriteLog("Resumed... GetColorImageMapping" + parentASIN);
                        }
                    }

                SimpleLog.WriteLog(" Canceled... GetColorImageMapping" + parentASIN);
                return new KeyValuePair<string, Dictionary<string, string>>(null,null);
            }
        }

        public Uri getNavigateUri(string asin)
        {
            return new Uri(baseUrl + asin, UriKind.Absolute);
        }
    }
}
