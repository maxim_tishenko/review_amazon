﻿using MarketplaceWebService;
using MarketplaceWebService.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSI.ASINOptimization.Services.Amazon
{
    public class AmazonFeedService
    {
        MarketplaceWebServiceConfig config;
        MarketplaceWebServiceClient MWSClient;
        private AmazonFeedService()
        {
            config = new MarketplaceWebServiceConfig().WithUserAgent(CredentialsProvider.UserAgent)
            .WithServiceURL("https://mws.amazonservices.com/");

            MWSClient = new MarketplaceWebServiceClient(CredentialsProvider.AWSAccessKeyID, CredentialsProvider.SecretKey, config);
        }

        private static AmazonFeedService instance;

        public static AmazonFeedService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AmazonFeedService();
                }
                return instance;
            }
        }

        private static List<string> feedTypesList = new List<string>()
        {
            "_POST_PRODUCT_DATA_",
            "_POST_INVENTORY_AVAILABILITY_DATA_",
            "_POST_PRODUCT_OVERRIDES_DATA_",
            "_POST_PRODUCT_PRICING_DATA_",
            "_POST_PRODUCT_IMAGE_DATA_",
            "_POST_PRODUCT_RELATIONSHIP_DATA_",
            "_POST_FLAT_FILE_INVLOADER_DATA_",
            "_POST_FLAT_FILE_LISTINGS_DATA_",
            "_POST_FLAT_FILE_BOOKLOADER_DATA_",
            "_POST_FLAT_FILE_CONVERGENCE_LISTINGS_DATA_",
            "_POST_FLAT_FILE_PRICEANDQUANTITYONLY_UPDATE_DATA_",
            "_POST_UIEE_BOOKLOADER_DATA_",
            "_POST_STD_ACES_DATA_",
            "_POST_ORDER_ACKNOWLEDGEMENT_DATA_",
            "_POST_PAYMENT_ADJUSTMENT_DATA_",
            "_POST_ORDER_FULFILLMENT_DATA_",
            "_POST_INVOICE_CONFIRMATION_DATA_",
            "_POST_FLAT_FILE_ORDER_ACKNOWLEDGEMENT_DATA_",
            "_POST_FLAT_FILE_PAYMENT_ADJUSTMENT_DATA_",
            "_POST_FLAT_FILE_FULFILLMENT_DATA_",
            "_POST_FLAT_FILE_INVOICE_CONFIRMATION_DATA_",
            "_POST_FULFILLMENT_ORDER_REQUEST_DATA_",
            "_POST_FULFILLMENT_ORDER_CANCELLATION_REQUEST_DATA_",
            "_POST_FBA_INBOUND_CARTON_CONTENTS_",
            "_POST_FLAT_FILE_FULFILLMENT_ORDER_REQUEST_DATA_",
            "_POST_FLAT_FILE_FULFILLMENT_ORDER_CANCELLATION_REQUEST_DATA_",
            "_POST_FLAT_FILE_FBA_CREATE_INBOUND_PLAN_",
            "_POST_FLAT_FILE_FBA_UPDATE_INBOUND_PLAN_",
            "_POST_FLAT_FILE_FBA_CREATE_REMOVAL_"
        };

        public decimal GetFeedSubmissionCountRequest()
        {
            var getFeedSubmissionCountRequest = new GetFeedSubmissionCountRequest()
                .WithMerchant(CredentialsProvider.SellerID)
               .WithMWSAuthToken(CredentialsProvider.MWSAuthToken);
            return MWSClient.GetFeedSubmissionCount(getFeedSubmissionCountRequest)
                .GetFeedSubmissionCountResult
                .Count;
        }
        public FileStream GetFeedSubmissionResultRequest()
        {
            var reportName = "FeedSubmissionResult_data_" + DateTime.UtcNow.ToShortDateString() + ".csv";
            var stream = new FileStream(reportName, FileMode.Create);
            var getFeedSubmissionResultRequest = new GetFeedSubmissionResultRequest()
                .WithMerchant(CredentialsProvider.SellerID)
               .WithMWSAuthToken(CredentialsProvider.MWSAuthToken)
               .WithFeedSubmissionResult(stream);
            var result = MWSClient.GetFeedSubmissionResult(getFeedSubmissionResultRequest);
            return stream;
        }
        public List<FeedSubmissionInfo> GetFeedSubmissionListRequest()
        {
            var getFeedSubmissionListRequest = new GetFeedSubmissionListRequest()
                .WithMerchant(CredentialsProvider.SellerID)
               .WithMWSAuthToken(CredentialsProvider.MWSAuthToken);
            return MWSClient.GetFeedSubmissionList(getFeedSubmissionListRequest)
                .GetFeedSubmissionListResult
                .FeedSubmissionInfo;
        }
        public FeedSubmissionInfo SubmitFeedRequest(int feedType, Stream content)
        {
            var requestSubmitFeed = new SubmitFeedRequest()
               .WithMWSAuthToken(CredentialsProvider.MWSAuthToken)
               .WithFeedType(feedTypesList[feedType])
               .WithFeedContent(content);
            SubmitFeedResponse result = MWSClient.SubmitFeed(requestSubmitFeed);

            return result.SubmitFeedResult.FeedSubmissionInfo;
        }
    }       
}
